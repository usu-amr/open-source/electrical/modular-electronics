<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.5" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Release" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Heatsink" color="1" fill="7" visible="no" active="no"/>
<layer number="102" name="tMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="103" name="bMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="104" name="tSchirm" color="7" fill="4" visible="no" active="no"/>
<layer number="105" name="bSchirm" color="7" fill="5" visible="no" active="no"/>
<layer number="106" name="shield" color="11" fill="1" visible="no" active="no"/>
<layer number="107" name="ShieldTop" color="12" fill="1" visible="no" active="no"/>
<layer number="108" name="MeasuresWall" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="SYSONDER" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="tStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="bStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="bTPoint" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Änderung" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="Component" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="PCB" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="Layout_Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="lotbs" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="lotls" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="BOHRFILM" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="Mods" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="Document_mirrored" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="References" color="4" fill="1" visible="no" active="no"/>
<layer number="151" name="FaceHelp" color="5" fill="1" visible="no" active="no"/>
<layer number="152" name="FaceFrame" color="6" fill="1" visible="no" active="no"/>
<layer number="153" name="FacePlate" color="2" fill="1" visible="no" active="no"/>
<layer number="154" name="FaceLabel" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="156" name="gesam-Maß" color="1" fill="7" visible="no" active="no"/>
<layer number="157" name="FaceMchng" color="3" fill="1" visible="no" active="no"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="Topologie" color="9" fill="1" visible="no" active="no"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Bauteile" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="bBauteile" color="1" fill="7" visible="no" active="no"/>
<layer number="202" name="value" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="no" color="7" fill="1" visible="no" active="no"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="tPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="bPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="ausheb_u" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="ausheb_o" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="frontpla" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AggieMarineRobotics">
<packages>
<package name="120PIN_EDGE_BOARD">
<smd name="GND1" x="30.4" y="-5.8" dx="7" dy="7" layer="1" rot="R90"/>
<smd name="PWR1" x="21.3" y="-5.8" dx="7" dy="7" layer="1" rot="R90"/>
<smd name="A1" x="36.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A2" x="37.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A3" x="38" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A4" x="38.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="GND2" x="30.4" y="-5.8" dx="7" dy="7" layer="16" rot="R90"/>
<smd name="PWR2" x="21.3" y="-5.8" dx="7" dy="7" layer="16" rot="R90" thermals="no"/>
<smd name="B1" x="36" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B2" x="36.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B3" x="37.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<wire x1="35.55" y1="-4" x2="35" y2="-3.45" width="0" layer="20" curve="90"/>
<wire x1="35" y1="-3.45" x2="34.45" y2="-4" width="0" layer="20" curve="90"/>
<wire x1="34.45" y1="-4" x2="34.45" y2="-10.9" width="0" layer="20"/>
<wire x1="34.45" y1="-10.9" x2="33.95" y2="-11.4" width="0" layer="20"/>
<wire x1="33.95" y1="-11.4" x2="17.85" y2="-11.4" width="0" layer="20"/>
<wire x1="35.55" y1="-4" x2="35.55" y2="-10.9" width="0" layer="20"/>
<wire x1="35.55" y1="-10.9" x2="36.05" y2="-11.4" width="0" layer="20"/>
<wire x1="36.05" y1="-11.4" x2="84.3" y2="-11.4" width="0" layer="20"/>
<wire x1="84.3" y1="-11.4" x2="84.8" y2="-10.9" width="0" layer="20"/>
<wire x1="84.8" y1="-10.9" x2="84.8" y2="0" width="0" layer="20"/>
<wire x1="17.85" y1="-11.4" x2="17.35" y2="-10.9" width="0" layer="20"/>
<wire x1="17.35" y1="-10.9" x2="17.35" y2="0" width="0" layer="20"/>
<smd name="B4" x="38.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A5" x="39.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A6" x="40.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A7" x="41.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A8" x="42" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B5" x="39.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B6" x="40" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B7" x="40.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B8" x="41.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A9" x="42.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A10" x="43.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A11" x="44.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A12" x="45.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B9" x="42.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B10" x="43.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B11" x="44" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B12" x="44.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A13" x="46" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A14" x="46.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A15" x="47.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A16" x="48.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B13" x="45.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B14" x="46.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B15" x="47.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B16" x="48" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A17" x="49.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A18" x="50" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A19" x="50.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A20" x="51.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B17" x="48.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B18" x="49.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B19" x="50.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B20" x="51.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A21" x="52.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A22" x="53.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A23" x="54" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A24" x="54.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B21" x="52" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B22" x="52.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B23" x="53.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B24" x="54.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A25" x="55.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A26" x="56.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A27" x="57.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A28" x="58" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B25" x="55.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B26" x="56" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B27" x="56.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B28" x="57.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A29" x="58.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A30" x="59.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A31" x="60.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A32" x="61.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B29" x="58.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B30" x="59.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B31" x="60" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B32" x="60.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A33" x="62" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A34" x="62.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A35" x="63.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A36" x="64.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B33" x="61.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B34" x="62.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B35" x="63.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B36" x="64" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A37" x="65.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A38" x="66" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A39" x="66.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A40" x="67.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B37" x="64.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B38" x="65.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B39" x="66.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B40" x="67.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A41" x="68.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A42" x="69.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A43" x="70" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A44" x="70.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B41" x="68" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B42" x="68.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B43" x="69.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B44" x="70.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A45" x="71.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A46" x="72.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A47" x="73.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A48" x="74" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B45" x="71.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B46" x="72" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B47" x="72.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B48" x="73.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A49" x="74.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A50" x="75.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A51" x="76.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A52" x="77.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B49" x="74.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B50" x="75.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B51" x="76" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B52" x="76.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A53" x="78" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A54" x="78.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A55" x="79.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A56" x="80.4" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B53" x="77.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B54" x="78.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B55" x="79.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B56" x="80" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="A57" x="81.2" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A58" x="82" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A59" x="82.8" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="A60" x="83.6" y="-8.85" dx="2.5" dy="0.5" layer="1" rot="R90"/>
<smd name="B57" x="80.8" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B58" x="81.6" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B59" x="82.4" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<smd name="B60" x="83.2" y="-8.85" dx="2.5" dy="0.5" layer="16" rot="R90"/>
<wire x1="17.35" y1="0" x2="0" y2="0" width="0" layer="20"/>
<wire x1="84.8" y1="0" x2="100" y2="0" width="0" layer="20"/>
</package>
</packages>
<symbols>
<symbol name="120PIN_EDGE_CONN">
<wire x1="-6.286" y1="-158" x2="6.286" y2="-158" width="0.254" layer="94"/>
<wire x1="6.286" y1="-158" x2="6.54" y2="-157.75" width="0.254" layer="94"/>
<wire x1="6.54" y1="-157.75" x2="6.54" y2="30.226" width="0.254" layer="94"/>
<wire x1="6.54" y1="30.226" x2="6.286" y2="30.48" width="0.254" layer="94"/>
<wire x1="6.286" y1="30.48" x2="-6.286" y2="30.48" width="0.254" layer="94"/>
<wire x1="-6.286" y1="30.48" x2="-6.54" y2="30.226" width="0.254" layer="94"/>
<wire x1="-6.54" y1="30.226" x2="-6.54" y2="-35.686" width="0.254" layer="94"/>
<wire x1="-6.54" y1="-35.686" x2="-6.286" y2="-158" width="0.254" layer="94"/>
<pin name="P1" x="-9" y="29" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P2" x="-9" y="26" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G1" x="9" y="29" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G2" x="9" y="26" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="A1" x="-9" y="21" visible="pin" length="short" swaplevel="1"/>
<pin name="B2" x="9" y="18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B1" x="9" y="21" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B3" x="9" y="15" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B4" x="9" y="12" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B5" x="9" y="9" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B6" x="9" y="6" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B7" x="9" y="3" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B8" x="9" y="0" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B9" x="9" y="-3" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B10" x="9" y="-6" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B11" x="9" y="-9" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B12" x="9" y="-12" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B13" x="9" y="-15" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B14" x="9" y="-18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B15" x="9" y="-21" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B16" x="9" y="-24" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B17" x="9" y="-27" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B18" x="9" y="-30" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B19" x="9" y="-33" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B20" x="9" y="-36" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B21" x="9" y="-39" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B22" x="9" y="-42" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B23" x="9" y="-45" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B24" x="9" y="-48" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B25" x="9" y="-51" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B26" x="9" y="-54" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B27" x="9" y="-57" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B28" x="9" y="-60" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B29" x="9" y="-63" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B30" x="9" y="-66" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B31" x="9" y="-69" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B32" x="9" y="-72" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B33" x="9" y="-75" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B34" x="9" y="-78" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B35" x="9" y="-81" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B36" x="9" y="-84" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B37" x="9" y="-87" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B38" x="9" y="-90" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B39" x="9" y="-93" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B40" x="9" y="-96" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B41" x="9" y="-99" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B42" x="9" y="-102" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B43" x="9" y="-105" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B44" x="9" y="-108" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B45" x="9" y="-111" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B46" x="9" y="-114" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B47" x="9" y="-117" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B48" x="9" y="-120" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B49" x="9" y="-123" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B50" x="9" y="-126" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B51" x="9" y="-129" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B52" x="9" y="-132" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B53" x="9" y="-135" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B54" x="9" y="-138" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B55" x="9" y="-141" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B56" x="9" y="-144" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B57" x="9" y="-147" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B58" x="9" y="-150" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B59" x="9" y="-153" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B60" x="9" y="-156" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="A2" x="-9" y="18" visible="pin" length="short" swaplevel="1"/>
<pin name="A3" x="-9" y="15" visible="pin" length="short" swaplevel="1"/>
<pin name="A4" x="-9" y="12" visible="pin" length="short" swaplevel="1"/>
<pin name="A5" x="-9" y="9" visible="pin" length="short" swaplevel="1"/>
<pin name="A6" x="-9" y="6" visible="pin" length="short" swaplevel="1"/>
<pin name="A7" x="-9" y="3" visible="pin" length="short" swaplevel="1"/>
<pin name="A8" x="-9" y="0" visible="pin" length="short" swaplevel="1"/>
<pin name="A9" x="-9" y="-3" visible="pin" length="short" swaplevel="1"/>
<pin name="A10" x="-9" y="-6" visible="pin" length="short" swaplevel="1"/>
<pin name="A11" x="-9" y="-9" visible="pin" length="short" swaplevel="1"/>
<pin name="A12" x="-9" y="-12" visible="pin" length="short" swaplevel="1"/>
<pin name="A13" x="-9" y="-15" visible="pin" length="short" swaplevel="1"/>
<pin name="A14" x="-9" y="-18" visible="pin" length="short" swaplevel="1"/>
<pin name="A15" x="-9" y="-21" visible="pin" length="short" swaplevel="1"/>
<pin name="A16" x="-9" y="-24" visible="pin" length="short" swaplevel="1"/>
<pin name="A17" x="-9" y="-27" visible="pin" length="short" swaplevel="1"/>
<pin name="A18" x="-9" y="-30" visible="pin" length="short" swaplevel="1"/>
<pin name="A19" x="-9" y="-33" visible="pin" length="short" swaplevel="1"/>
<pin name="A20" x="-9" y="-36" visible="pin" length="short" swaplevel="1"/>
<pin name="A21" x="-9" y="-39" visible="pin" length="short" swaplevel="1"/>
<pin name="A22" x="-9" y="-42" visible="pin" length="short" swaplevel="1"/>
<pin name="A23" x="-9" y="-45" visible="pin" length="short" swaplevel="1"/>
<pin name="A24" x="-9" y="-48" visible="pin" length="short" swaplevel="1"/>
<pin name="A25" x="-9" y="-51" visible="pin" length="short" swaplevel="1"/>
<pin name="A26" x="-9" y="-54" visible="pin" length="short" swaplevel="1"/>
<pin name="A27" x="-9" y="-57" visible="pin" length="short" swaplevel="1"/>
<pin name="A28" x="-9" y="-60" visible="pin" length="short" swaplevel="1"/>
<pin name="A29" x="-9" y="-63" visible="pin" length="short" swaplevel="1"/>
<pin name="A30" x="-9" y="-66" visible="pin" length="short" swaplevel="1"/>
<pin name="A31" x="-9" y="-69" visible="pin" length="short" swaplevel="1"/>
<pin name="A32" x="-9" y="-72" visible="pin" length="short" swaplevel="1"/>
<pin name="A33" x="-9" y="-75" visible="pin" length="short" swaplevel="1"/>
<pin name="A34" x="-9" y="-78" visible="pin" length="short" swaplevel="1"/>
<pin name="A35" x="-9" y="-81" visible="pin" length="short" swaplevel="1"/>
<pin name="A36" x="-9" y="-84" visible="pin" length="short" swaplevel="1"/>
<pin name="A37" x="-9" y="-87" visible="pin" length="short" swaplevel="1"/>
<pin name="A38" x="-9" y="-90" visible="pin" length="short" swaplevel="1"/>
<pin name="A39" x="-9" y="-93" visible="pin" length="short" swaplevel="1"/>
<pin name="A40" x="-9" y="-96" visible="pin" length="short" swaplevel="1"/>
<pin name="A41" x="-9" y="-99" visible="pin" length="short" swaplevel="1"/>
<pin name="A42" x="-9" y="-102" visible="pin" length="short" swaplevel="1"/>
<pin name="A43" x="-9" y="-105" visible="pin" length="short" swaplevel="1"/>
<pin name="A44" x="-9" y="-108" visible="pin" length="short" swaplevel="1"/>
<pin name="A45" x="-9" y="-111" visible="pin" length="short" swaplevel="1"/>
<pin name="A46" x="-9" y="-114" visible="pin" length="short" swaplevel="1"/>
<pin name="A47" x="-9" y="-117" visible="pin" length="short" swaplevel="1"/>
<pin name="A48" x="-9" y="-120" visible="pin" length="short" swaplevel="1"/>
<pin name="A49" x="-9" y="-123" visible="pin" length="short" swaplevel="1"/>
<pin name="A50" x="-9" y="-126" visible="pin" length="short" swaplevel="1"/>
<pin name="A51" x="-9" y="-129" visible="pin" length="short" swaplevel="1"/>
<pin name="A52" x="-9" y="-132" visible="pin" length="short" swaplevel="1"/>
<pin name="A53" x="-9" y="-135" visible="pin" length="short" swaplevel="1"/>
<pin name="A54" x="-9" y="-138" visible="pin" length="short" swaplevel="1"/>
<pin name="A55" x="-9" y="-141" visible="pin" length="short" swaplevel="1"/>
<pin name="A56" x="-9" y="-144" visible="pin" length="short" swaplevel="1"/>
<pin name="A57" x="-9" y="-147" visible="pin" length="short" swaplevel="1"/>
<pin name="A58" x="-9" y="-150" visible="pin" length="short" swaplevel="1"/>
<pin name="A59" x="-9" y="-153" visible="pin" length="short" swaplevel="1"/>
<pin name="A60" x="-9" y="-156" visible="pin" length="short" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EDGE_CONN_LAYOUT" prefix="EDGE">
<gates>
<gate name="G$1" symbol="120PIN_EDGE_CONN" x="0" y="2.54" swaplevel="1"/>
</gates>
<devices>
<device name="" package="120PIN_EDGE_BOARD">
<connects>
<connect gate="G$1" pin="A1" pad="A1"/>
<connect gate="G$1" pin="A10" pad="A10"/>
<connect gate="G$1" pin="A11" pad="A11"/>
<connect gate="G$1" pin="A12" pad="A12"/>
<connect gate="G$1" pin="A13" pad="A13"/>
<connect gate="G$1" pin="A14" pad="A14"/>
<connect gate="G$1" pin="A15" pad="A15"/>
<connect gate="G$1" pin="A16" pad="A16"/>
<connect gate="G$1" pin="A17" pad="A17"/>
<connect gate="G$1" pin="A18" pad="A18"/>
<connect gate="G$1" pin="A19" pad="A19"/>
<connect gate="G$1" pin="A2" pad="A2"/>
<connect gate="G$1" pin="A20" pad="A20"/>
<connect gate="G$1" pin="A21" pad="A21"/>
<connect gate="G$1" pin="A22" pad="A22"/>
<connect gate="G$1" pin="A23" pad="A23"/>
<connect gate="G$1" pin="A24" pad="A24"/>
<connect gate="G$1" pin="A25" pad="A25"/>
<connect gate="G$1" pin="A26" pad="A26"/>
<connect gate="G$1" pin="A27" pad="A27"/>
<connect gate="G$1" pin="A28" pad="A28"/>
<connect gate="G$1" pin="A29" pad="A29"/>
<connect gate="G$1" pin="A3" pad="A3"/>
<connect gate="G$1" pin="A30" pad="A30"/>
<connect gate="G$1" pin="A31" pad="A31"/>
<connect gate="G$1" pin="A32" pad="A32"/>
<connect gate="G$1" pin="A33" pad="A33"/>
<connect gate="G$1" pin="A34" pad="A34"/>
<connect gate="G$1" pin="A35" pad="A35"/>
<connect gate="G$1" pin="A36" pad="A36"/>
<connect gate="G$1" pin="A37" pad="A37"/>
<connect gate="G$1" pin="A38" pad="A38"/>
<connect gate="G$1" pin="A39" pad="A39"/>
<connect gate="G$1" pin="A4" pad="A4"/>
<connect gate="G$1" pin="A40" pad="A40"/>
<connect gate="G$1" pin="A41" pad="A41"/>
<connect gate="G$1" pin="A42" pad="A42"/>
<connect gate="G$1" pin="A43" pad="A43"/>
<connect gate="G$1" pin="A44" pad="A44"/>
<connect gate="G$1" pin="A45" pad="A45"/>
<connect gate="G$1" pin="A46" pad="A46"/>
<connect gate="G$1" pin="A47" pad="A47"/>
<connect gate="G$1" pin="A48" pad="A48"/>
<connect gate="G$1" pin="A49" pad="A49"/>
<connect gate="G$1" pin="A5" pad="A5"/>
<connect gate="G$1" pin="A50" pad="A50"/>
<connect gate="G$1" pin="A51" pad="A51"/>
<connect gate="G$1" pin="A52" pad="A52"/>
<connect gate="G$1" pin="A53" pad="A53"/>
<connect gate="G$1" pin="A54" pad="A54"/>
<connect gate="G$1" pin="A55" pad="A55"/>
<connect gate="G$1" pin="A56" pad="A56"/>
<connect gate="G$1" pin="A57" pad="A57"/>
<connect gate="G$1" pin="A58" pad="A58"/>
<connect gate="G$1" pin="A59" pad="A59"/>
<connect gate="G$1" pin="A6" pad="A6"/>
<connect gate="G$1" pin="A60" pad="A60"/>
<connect gate="G$1" pin="A7" pad="A7"/>
<connect gate="G$1" pin="A8" pad="A8"/>
<connect gate="G$1" pin="A9" pad="A9"/>
<connect gate="G$1" pin="B1" pad="B1"/>
<connect gate="G$1" pin="B10" pad="B10"/>
<connect gate="G$1" pin="B11" pad="B11"/>
<connect gate="G$1" pin="B12" pad="B12"/>
<connect gate="G$1" pin="B13" pad="B13"/>
<connect gate="G$1" pin="B14" pad="B14"/>
<connect gate="G$1" pin="B15" pad="B15"/>
<connect gate="G$1" pin="B16" pad="B16"/>
<connect gate="G$1" pin="B17" pad="B17"/>
<connect gate="G$1" pin="B18" pad="B18"/>
<connect gate="G$1" pin="B19" pad="B19"/>
<connect gate="G$1" pin="B2" pad="B2"/>
<connect gate="G$1" pin="B20" pad="B20"/>
<connect gate="G$1" pin="B21" pad="B21"/>
<connect gate="G$1" pin="B22" pad="B22"/>
<connect gate="G$1" pin="B23" pad="B23"/>
<connect gate="G$1" pin="B24" pad="B24"/>
<connect gate="G$1" pin="B25" pad="B25"/>
<connect gate="G$1" pin="B26" pad="B26"/>
<connect gate="G$1" pin="B27" pad="B27"/>
<connect gate="G$1" pin="B28" pad="B28"/>
<connect gate="G$1" pin="B29" pad="B29"/>
<connect gate="G$1" pin="B3" pad="B3"/>
<connect gate="G$1" pin="B30" pad="B30"/>
<connect gate="G$1" pin="B31" pad="B31"/>
<connect gate="G$1" pin="B32" pad="B32"/>
<connect gate="G$1" pin="B33" pad="B33"/>
<connect gate="G$1" pin="B34" pad="B34"/>
<connect gate="G$1" pin="B35" pad="B35"/>
<connect gate="G$1" pin="B36" pad="B36"/>
<connect gate="G$1" pin="B37" pad="B37"/>
<connect gate="G$1" pin="B38" pad="B38"/>
<connect gate="G$1" pin="B39" pad="B39"/>
<connect gate="G$1" pin="B4" pad="B4"/>
<connect gate="G$1" pin="B40" pad="B40"/>
<connect gate="G$1" pin="B41" pad="B41"/>
<connect gate="G$1" pin="B42" pad="B42"/>
<connect gate="G$1" pin="B43" pad="B43"/>
<connect gate="G$1" pin="B44" pad="B44"/>
<connect gate="G$1" pin="B45" pad="B45"/>
<connect gate="G$1" pin="B46" pad="B46"/>
<connect gate="G$1" pin="B47" pad="B47"/>
<connect gate="G$1" pin="B48" pad="B48"/>
<connect gate="G$1" pin="B49" pad="B49"/>
<connect gate="G$1" pin="B5" pad="B5"/>
<connect gate="G$1" pin="B50" pad="B50"/>
<connect gate="G$1" pin="B51" pad="B51"/>
<connect gate="G$1" pin="B52" pad="B52"/>
<connect gate="G$1" pin="B53" pad="B53"/>
<connect gate="G$1" pin="B54" pad="B54"/>
<connect gate="G$1" pin="B55" pad="B55"/>
<connect gate="G$1" pin="B56" pad="B56"/>
<connect gate="G$1" pin="B57" pad="B57"/>
<connect gate="G$1" pin="B58" pad="B58"/>
<connect gate="G$1" pin="B59" pad="B59"/>
<connect gate="G$1" pin="B6" pad="B6"/>
<connect gate="G$1" pin="B60" pad="B60"/>
<connect gate="G$1" pin="B7" pad="B7"/>
<connect gate="G$1" pin="B8" pad="B8"/>
<connect gate="G$1" pin="B9" pad="B9"/>
<connect gate="G$1" pin="G1" pad="GND1"/>
<connect gate="G$1" pin="G2" pad="GND2"/>
<connect gate="G$1" pin="P1" pad="PWR1"/>
<connect gate="G$1" pin="P2" pad="PWR2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA04-1" urn="urn:adsk.eagle:footprint:8285/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="0.635" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="5.334" y="-0.635" size="1.27" layer="21" ratio="10">4</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
</package>
<package name="MA06-1" urn="urn:adsk.eagle:footprint:8287/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.62" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.985" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="5.715" y="1.651" size="1.27" layer="21" ratio="10">6</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
<package name="MA03-1" urn="urn:adsk.eagle:footprint:8281/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-3.81" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA04-1" urn="urn:adsk.eagle:package:8337/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA04-1"/>
</packageinstances>
</package3d>
<package3d name="MA06-1" urn="urn:adsk.eagle:package:8340/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA06-1"/>
</packageinstances>
</package3d>
<package3d name="MA03-1" urn="urn:adsk.eagle:package:8339/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA03-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA04-1" urn="urn:adsk.eagle:symbol:8284/1" library_version="1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA06-1" urn="urn:adsk.eagle:symbol:8286/1" library_version="1">
<wire x1="3.81" y1="-10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-1.27" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA03-1" urn="urn:adsk.eagle:symbol:8280/1" library_version="1">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA04-1" urn="urn:adsk.eagle:component:8375/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA04-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA04-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8337/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA06-1" urn="urn:adsk.eagle:component:8378/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA06-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8340/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA03-1" urn="urn:adsk.eagle:component:8376/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8339/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="jumper" urn="urn:adsk.eagle:library:252">
<description>&lt;b&gt;Jumpers&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="JP1" urn="urn:adsk.eagle:footprint:15398/1" library_version="1">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<wire x1="-1.016" y1="0" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="0" x2="-1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="0" x2="1.27" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.254" x2="1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-2.54" x2="1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="1.27" y1="2.286" x2="1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="2.286" x2="1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.54" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.286" x2="-1.016" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.286" x2="-1.27" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.254" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="-1.27" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-2.54" x2="1.016" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="0" y="-1.27" drill="0.9144" shape="long"/>
<pad name="2" x="0" y="1.27" drill="0.9144" shape="long"/>
<text x="-1.651" y="-2.54" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.921" y="-2.54" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="0.9652" x2="0.3048" y2="1.5748" layer="51"/>
<rectangle x1="-0.3048" y1="-1.5748" x2="0.3048" y2="-0.9652" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="JP1" urn="urn:adsk.eagle:package:15455/1" type="box" library_version="1">
<description>JUMPER</description>
<packageinstances>
<packageinstance name="JP1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="JP2E" urn="urn:adsk.eagle:symbol:15391/1" library_version="1">
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.27" width="0.4064" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0" x2="3.175" y2="0" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="0.635" width="0.4064" layer="94"/>
<wire x1="3.175" y1="0.635" x2="-0.635" y2="0.635" width="0.4064" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.4064" layer="94"/>
<text x="-1.27" y="0" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="1" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="2.54" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JP1E" urn="urn:adsk.eagle:component:15487/1" prefix="JP" uservalue="yes" library_version="1">
<description>&lt;b&gt;JUMPER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="JP2E" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="JP1">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:15455/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="EDGE2" library="AggieMarineRobotics" deviceset="EDGE_CONN_LAYOUT" device=""/>
<part name="SV1" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV2" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV3" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV4" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV5" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA04-1" device="" package3d_urn="urn:adsk.eagle:package:8337/1"/>
<part name="SV6" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV7" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1"/>
<part name="SV8" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-1" device="" package3d_urn="urn:adsk.eagle:package:8339/1"/>
<part name="SV9" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-1" device="" package3d_urn="urn:adsk.eagle:package:8339/1"/>
<part name="JP1" library="jumper" library_urn="urn:adsk.eagle:library:252" deviceset="JP1E" device="" package3d_urn="urn:adsk.eagle:package:15455/1"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="EDGE2" gate="G$1" x="83" y="172"/>
<instance part="SV1" gate="1" x="4.9" y="224.5"/>
<instance part="SV2" gate="1" x="4.9" y="201.64"/>
<instance part="SV3" gate="1" x="4.9" y="178.78"/>
<instance part="SV4" gate="1" x="4.9" y="155.92"/>
<instance part="SV5" gate="1" x="5.9" y="21.36"/>
<instance part="SV6" gate="1" x="4.9" y="109.5"/>
<instance part="SV7" gate="1" x="4.9" y="132.5"/>
<instance part="SV8" gate="G$1" x="7.62" y="45.72"/>
<instance part="SV9" gate="G$1" x="7.62" y="66.04"/>
<instance part="JP1" gate="A" x="9" y="85" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="PWM1" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A13"/>
<wire x1="74" y1="157" x2="70" y2="157" width="0.1524" layer="91"/>
<label x="70" y="157" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="6"/>
<wire x1="12.52" y1="229.58" x2="17.6" y2="229.58" width="0.1524" layer="91"/>
<label x="17.6" y="229.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM2" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A12"/>
<wire x1="74" y1="160" x2="70" y2="160" width="0.1524" layer="91"/>
<label x="70" y="160" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="3"/>
<wire x1="12.52" y1="221.96" x2="17.6" y2="221.96" width="0.1524" layer="91"/>
<label x="17.6" y="221.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM3" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A11"/>
<wire x1="74" y1="163" x2="70" y2="163" width="0.1524" layer="91"/>
<label x="70" y="163" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="6"/>
<wire x1="12.52" y1="206.72" x2="17.6" y2="206.72" width="0.1524" layer="91"/>
<label x="17.6" y="206.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM4" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A10"/>
<wire x1="74" y1="166" x2="70" y2="166" width="0.1524" layer="91"/>
<label x="70" y="166" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="3"/>
<wire x1="12.52" y1="199.1" x2="17.6" y2="199.1" width="0.1524" layer="91"/>
<label x="17.6" y="199.1" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM5" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A9"/>
<wire x1="74" y1="169" x2="70" y2="169" width="0.1524" layer="91"/>
<label x="70" y="169" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV3" gate="1" pin="6"/>
<wire x1="12.52" y1="183.86" x2="17.6" y2="183.86" width="0.1524" layer="91"/>
<label x="17.6" y="183.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM6" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A8"/>
<wire x1="74" y1="172" x2="70" y2="172" width="0.1524" layer="91"/>
<label x="70" y="172" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV3" gate="1" pin="3"/>
<wire x1="12.52" y1="176.24" x2="17.6" y2="176.24" width="0.1524" layer="91"/>
<label x="17.6" y="176.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM7" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A7"/>
<wire x1="74" y1="175" x2="70" y2="175" width="0.1524" layer="91"/>
<label x="70" y="175" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV4" gate="1" pin="6"/>
<wire x1="12.52" y1="161" x2="17.6" y2="161" width="0.1524" layer="91"/>
<label x="17.6" y="161" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM8" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A6"/>
<wire x1="74" y1="178" x2="70" y2="178" width="0.1524" layer="91"/>
<label x="70" y="178" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV4" gate="1" pin="3"/>
<wire x1="12.52" y1="153.38" x2="17.6" y2="153.38" width="0.1524" layer="91"/>
<label x="17.6" y="153.38" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="IMU_SDA" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A24"/>
<wire x1="74" y1="124" x2="70" y2="124" width="0.1524" layer="91"/>
<label x="70" y="124" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="13.52" y1="21.36" x2="18.6" y2="21.36" width="0.1524" layer="91"/>
<label x="18.6" y="21.36" size="1.778" layer="95" xref="yes"/>
<pinref part="SV5" gate="1" pin="3"/>
</segment>
</net>
<net name="IMU_SCL" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A25"/>
<wire x1="74" y1="121" x2="70" y2="121" width="0.1524" layer="91"/>
<label x="70" y="121" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="10.98" y1="18.82" x2="18.6" y2="18.82" width="0.1524" layer="91"/>
<label x="18.6" y="18.82" size="1.778" layer="95" xref="yes"/>
<pinref part="SV5" gate="1" pin="2"/>
<wire x1="13.52" y1="18.82" x2="10.98" y2="18.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A1"/>
<wire x1="74" y1="193" x2="70" y2="193" width="0.1524" layer="91"/>
<label x="70" y="193" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="13.52" y1="16.28" x2="18.6" y2="16.28" width="0.1524" layer="91"/>
<label x="18.6" y="16.28" size="1.778" layer="95" xref="yes"/>
<pinref part="SV5" gate="1" pin="1"/>
</segment>
</net>
<net name="PWM9" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A5"/>
<wire x1="74" y1="181" x2="70" y2="181" width="0.1524" layer="91"/>
<label x="70" y="181" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV7" gate="1" pin="6"/>
<wire x1="12.52" y1="137.58" x2="17.6" y2="137.58" width="0.1524" layer="91"/>
<label x="17.6" y="137.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM10" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A4"/>
<wire x1="74" y1="184" x2="70" y2="184" width="0.1524" layer="91"/>
<label x="70" y="184" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV7" gate="1" pin="3"/>
<wire x1="12.52" y1="129.96" x2="17.6" y2="129.96" width="0.1524" layer="91"/>
<label x="17.6" y="129.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM11" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A3"/>
<wire x1="74" y1="187" x2="70" y2="187" width="0.1524" layer="91"/>
<label x="70" y="187" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV6" gate="1" pin="6"/>
<wire x1="12.52" y1="114.58" x2="17.6" y2="114.58" width="0.1524" layer="91"/>
<label x="17.6" y="114.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PWM12" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A2"/>
<wire x1="74" y1="190" x2="70" y2="190" width="0.1524" layer="91"/>
<label x="70" y="190" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV6" gate="1" pin="3"/>
<wire x1="12.52" y1="106.96" x2="17.6" y2="106.96" width="0.1524" layer="91"/>
<label x="17.6" y="106.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="WATER_SENS" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A59"/>
<wire x1="74" y1="19" x2="71" y2="19" width="0.1524" layer="91"/>
<label x="71" y="19" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV9" gate="G$1" pin="1"/>
<wire x1="15.24" y1="63.5" x2="20.32" y2="63.5" width="0.1524" layer="91"/>
<label x="20.32" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PRESS_SENS" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A60"/>
<wire x1="74" y1="16" x2="71" y2="16" width="0.1524" layer="91"/>
<label x="71" y="16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV8" gate="G$1" pin="1"/>
<wire x1="15.24" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<label x="20.32" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="PGND" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="B1"/>
<wire x1="92" y1="193" x2="97" y2="193" width="0.1524" layer="91"/>
<label x="97" y="201" size="1.778" layer="95" xref="yes"/>
<pinref part="EDGE2" gate="G$1" pin="G1"/>
<wire x1="92" y1="201" x2="97" y2="201" width="0.1524" layer="91"/>
<wire x1="97" y1="201" x2="97" y2="193" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SV5" gate="1" pin="4"/>
<wire x1="13.52" y1="23.9" x2="13.52" y2="24" width="0.1524" layer="91"/>
<wire x1="13.52" y1="24" x2="18" y2="24" width="0.1524" layer="91"/>
<label x="18" y="24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DGND" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="B2"/>
<wire x1="92" y1="190" x2="97" y2="190" width="0.1524" layer="91"/>
<wire x1="97" y1="190" x2="97" y2="187" width="0.1524" layer="91"/>
<pinref part="EDGE2" gate="G$1" pin="B13"/>
<wire x1="97" y1="187" x2="97" y2="184" width="0.1524" layer="91"/>
<wire x1="97" y1="184" x2="97" y2="181" width="0.1524" layer="91"/>
<wire x1="97" y1="181" x2="97" y2="178" width="0.1524" layer="91"/>
<wire x1="97" y1="178" x2="97" y2="175" width="0.1524" layer="91"/>
<wire x1="97" y1="175" x2="97" y2="172" width="0.1524" layer="91"/>
<wire x1="97" y1="172" x2="97" y2="169" width="0.1524" layer="91"/>
<wire x1="97" y1="169" x2="97" y2="166" width="0.1524" layer="91"/>
<wire x1="97" y1="166" x2="97" y2="163" width="0.1524" layer="91"/>
<wire x1="97" y1="163" x2="97" y2="160" width="0.1524" layer="91"/>
<wire x1="97" y1="160" x2="97" y2="157" width="0.1524" layer="91"/>
<wire x1="97" y1="157" x2="92" y2="157" width="0.1524" layer="91"/>
<pinref part="EDGE2" gate="G$1" pin="B12"/>
<wire x1="92" y1="160" x2="97" y2="160" width="0.1524" layer="91"/>
<junction x="97" y="160"/>
<pinref part="EDGE2" gate="G$1" pin="B11"/>
<wire x1="92" y1="163" x2="97" y2="163" width="0.1524" layer="91"/>
<junction x="97" y="163"/>
<pinref part="EDGE2" gate="G$1" pin="B10"/>
<wire x1="92" y1="166" x2="97" y2="166" width="0.1524" layer="91"/>
<junction x="97" y="166"/>
<pinref part="EDGE2" gate="G$1" pin="B9"/>
<wire x1="92" y1="169" x2="97" y2="169" width="0.1524" layer="91"/>
<junction x="97" y="169"/>
<pinref part="EDGE2" gate="G$1" pin="B8"/>
<wire x1="92" y1="172" x2="97" y2="172" width="0.1524" layer="91"/>
<junction x="97" y="172"/>
<pinref part="EDGE2" gate="G$1" pin="B7"/>
<wire x1="92" y1="175" x2="97" y2="175" width="0.1524" layer="91"/>
<junction x="97" y="175"/>
<pinref part="EDGE2" gate="G$1" pin="B6"/>
<wire x1="92" y1="178" x2="97" y2="178" width="0.1524" layer="91"/>
<junction x="97" y="178"/>
<pinref part="EDGE2" gate="G$1" pin="B5"/>
<wire x1="92" y1="181" x2="97" y2="181" width="0.1524" layer="91"/>
<junction x="97" y="181"/>
<pinref part="EDGE2" gate="G$1" pin="B4"/>
<wire x1="92" y1="184" x2="97" y2="184" width="0.1524" layer="91"/>
<junction x="97" y="184"/>
<pinref part="EDGE2" gate="G$1" pin="B3"/>
<wire x1="92" y1="187" x2="97" y2="187" width="0.1524" layer="91"/>
<junction x="97" y="187"/>
<pinref part="EDGE2" gate="G$1" pin="B24"/>
<wire x1="97" y1="157" x2="97" y2="124" width="0.1524" layer="91"/>
<wire x1="97" y1="124" x2="92" y2="124" width="0.1524" layer="91"/>
<junction x="97" y="157"/>
<pinref part="EDGE2" gate="G$1" pin="B25"/>
<wire x1="92" y1="121" x2="97" y2="121" width="0.1524" layer="91"/>
<wire x1="97" y1="121" x2="97" y2="124" width="0.1524" layer="91"/>
<junction x="97" y="124"/>
<label x="97" y="190" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV6" gate="1" pin="1"/>
<wire x1="12.52" y1="101.88" x2="15.06" y2="101.88" width="0.1524" layer="91"/>
<pinref part="SV6" gate="1" pin="4"/>
<wire x1="12.52" y1="109.5" x2="15.06" y2="109.5" width="0.1524" layer="91"/>
<wire x1="15.06" y1="109.5" x2="15.06" y2="101.88" width="0.1524" layer="91"/>
<label x="15" y="102" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV7" gate="1" pin="1"/>
<wire x1="12.52" y1="124.88" x2="15.06" y2="124.88" width="0.1524" layer="91"/>
<pinref part="SV7" gate="1" pin="4"/>
<wire x1="12.52" y1="132.5" x2="15.06" y2="132.5" width="0.1524" layer="91"/>
<wire x1="15.06" y1="132.5" x2="15.06" y2="124.88" width="0.1524" layer="91"/>
<label x="15" y="125" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV4" gate="1" pin="1"/>
<wire x1="12.52" y1="148.3" x2="15.06" y2="148.3" width="0.1524" layer="91"/>
<pinref part="SV4" gate="1" pin="4"/>
<wire x1="12.52" y1="155.92" x2="15.06" y2="155.92" width="0.1524" layer="91"/>
<wire x1="15.06" y1="155.92" x2="15.06" y2="148.3" width="0.1524" layer="91"/>
<label x="15" y="148" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV3" gate="1" pin="1"/>
<wire x1="12.52" y1="171.16" x2="15.06" y2="171.16" width="0.1524" layer="91"/>
<pinref part="SV3" gate="1" pin="4"/>
<wire x1="12.52" y1="178.78" x2="15.06" y2="178.78" width="0.1524" layer="91"/>
<wire x1="15.06" y1="178.78" x2="15.06" y2="171.16" width="0.1524" layer="91"/>
<label x="15" y="171" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="4"/>
<wire x1="12.52" y1="201.64" x2="15.06" y2="201.64" width="0.1524" layer="91"/>
<wire x1="15.06" y1="201.64" x2="15.06" y2="194.02" width="0.1524" layer="91"/>
<pinref part="SV2" gate="1" pin="1"/>
<wire x1="12.52" y1="194.02" x2="15.06" y2="194.02" width="0.1524" layer="91"/>
<label x="15" y="194" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="1" pin="1"/>
<wire x1="12.52" y1="216.88" x2="15.06" y2="216.88" width="0.1524" layer="91"/>
<pinref part="SV1" gate="1" pin="4"/>
<wire x1="12.52" y1="224.5" x2="15.06" y2="224.5" width="0.1524" layer="91"/>
<wire x1="15.06" y1="224.5" x2="15.06" y2="216.88" width="0.1524" layer="91"/>
<label x="15" y="217" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="AGND" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="B60"/>
<wire x1="92" y1="16" x2="95" y2="16" width="0.1524" layer="91"/>
<pinref part="EDGE2" gate="G$1" pin="B59"/>
<wire x1="95" y1="16" x2="95" y2="19" width="0.1524" layer="91"/>
<wire x1="95" y1="19" x2="92" y2="19" width="0.1524" layer="91"/>
<label x="95" y="19" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV8" gate="G$1" pin="2"/>
<wire x1="15.24" y1="45.72" x2="20.32" y2="45.72" width="0.1524" layer="91"/>
<label x="20.32" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV9" gate="G$1" pin="2"/>
<wire x1="15.24" y1="66.04" x2="20.32" y2="66.04" width="0.1524" layer="91"/>
<label x="20.32" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="P1"/>
<wire x1="74" y1="201" x2="70" y2="201" width="0.1524" layer="91"/>
<label x="70" y="201" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV8" gate="G$1" pin="3"/>
<wire x1="15.24" y1="48.26" x2="20.32" y2="48.26" width="0.1524" layer="91"/>
<label x="20.32" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="SV9" gate="G$1" pin="3"/>
<wire x1="15.24" y1="68.58" x2="20.32" y2="68.58" width="0.1524" layer="91"/>
<label x="20.32" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="MAG_KILL_SW" class="0">
<segment>
<pinref part="EDGE2" gate="G$1" pin="A32"/>
<wire x1="74" y1="100" x2="70" y2="100" width="0.1524" layer="91"/>
<label x="70" y="100" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<wire x1="11.54" y1="87.54" x2="16" y2="87.54" width="0.1524" layer="91"/>
<label x="16" y="87.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="SW_GND" class="0">
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<wire x1="11.54" y1="85" x2="16" y2="85" width="0.1524" layer="91"/>
<label x="16" y="85" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="EDGE2" gate="G$1" pin="B32"/>
<wire x1="92" y1="100" x2="97" y2="100" width="0.1524" layer="91"/>
<label x="96.5" y="100" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
