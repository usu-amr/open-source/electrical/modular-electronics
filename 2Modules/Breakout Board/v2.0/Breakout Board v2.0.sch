<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Release" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Heatsink" color="1" fill="7" visible="no" active="no"/>
<layer number="102" name="tMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="103" name="bMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="104" name="tSchirm" color="7" fill="4" visible="no" active="no"/>
<layer number="105" name="bSchirm" color="7" fill="5" visible="no" active="no"/>
<layer number="106" name="shield" color="11" fill="1" visible="no" active="no"/>
<layer number="107" name="ShieldTop" color="12" fill="1" visible="no" active="no"/>
<layer number="108" name="MeasuresWall" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="SYSONDER" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="tStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="bStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="bTPoint" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Änderung" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="Component" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="PCB" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="Layout_Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="lotbs" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="lotls" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="BOHRFILM" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="Mods" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="Document_mirrored" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="References" color="4" fill="1" visible="no" active="no"/>
<layer number="151" name="FaceHelp" color="5" fill="1" visible="no" active="no"/>
<layer number="152" name="FaceFrame" color="6" fill="1" visible="no" active="no"/>
<layer number="153" name="FacePlate" color="2" fill="1" visible="no" active="no"/>
<layer number="154" name="FaceLabel" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="156" name="gesam-Maß" color="1" fill="7" visible="no" active="no"/>
<layer number="157" name="FaceMchng" color="3" fill="1" visible="no" active="no"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="Topologie" color="9" fill="1" visible="no" active="no"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Bauteile" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="bBauteile" color="1" fill="7" visible="no" active="no"/>
<layer number="202" name="value" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="no" color="7" fill="1" visible="no" active="no"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="tPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="bPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="ausheb_u" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="ausheb_o" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="frontpla" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AggieMarineRobotics">
<packages>
<package name="EDGEBRD_AMPHENOL_164PCI">
<smd name="B12" x="1.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="B11" x="-1.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<wire x1="-12.15" y1="10" x2="-12.15" y2="0.95" width="0.1" layer="20"/>
<wire x1="-12.15" y1="0.95" x2="-11.2" y2="0" width="0.1" layer="20"/>
<wire x1="-11.2" y1="0" x2="-1.9" y2="0" width="0.1" layer="20"/>
<wire x1="-1.9" y1="0" x2="-0.95" y2="0.95" width="0.1" layer="20"/>
<wire x1="-0.95" y1="0.95" x2="-0.95" y2="7.45" width="0.1" layer="20"/>
<wire x1="-0.95" y1="7.45" x2="0" y2="8.4" width="0.1" layer="20" curve="-90"/>
<wire x1="0" y1="8.4" x2="0.95" y2="7.45" width="0.1" layer="20" curve="-90"/>
<wire x1="0.95" y1="7.45" x2="0.95" y2="0.95" width="0.1" layer="20"/>
<wire x1="0.95" y1="0.95" x2="1.9" y2="0" width="0.1" layer="20"/>
<wire x1="1.9" y1="0" x2="71.2" y2="0" width="0.1" layer="20"/>
<wire x1="71.2" y1="0" x2="72.15" y2="0.95" width="0.1" layer="20"/>
<wire x1="72.15" y1="0.95" x2="72.15" y2="10" width="0.1" layer="20"/>
<wire x1="-12" y1="1" x2="-1" y2="1" width="0.1" layer="49"/>
<wire x1="1" y1="1" x2="72" y2="1" width="0.1" layer="49"/>
<smd name="A11" x="-1.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="A12" x="1.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B1" x="-11.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A1" x="-11.5" y="4" dx="3.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B2" x="-10.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A2" x="-10.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B3" x="-9.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A3" x="-9.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B4" x="-8.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A4" x="-8.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B5" x="-7.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A5" x="-7.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B6" x="-6.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A6" x="-6.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B7" x="-5.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A7" x="-5.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B8" x="-4.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A8" x="-4.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B9" x="-3.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A9" x="-3.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B10" x="-2.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A10" x="-2.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B13" x="2.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A13" x="2.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B14" x="3.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A14" x="3.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B15" x="4.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A15" x="4.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B16" x="5.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A16" x="5.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B17" x="6.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A17" x="6.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B18" x="7.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A18" x="7.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B19" x="8.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A19" x="8.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B20" x="9.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A20" x="9.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B21" x="10.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A21" x="10.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B22" x="11.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A22" x="11.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B23" x="12.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A23" x="12.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B24" x="13.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A24" x="13.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B25" x="14.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A25" x="14.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B26" x="15.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A26" x="15.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B27" x="16.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A27" x="16.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B28" x="17.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A28" x="17.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B29" x="18.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A29" x="18.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B30" x="19.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A30" x="19.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B31" x="20.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A31" x="20.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B32" x="21.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A32" x="21.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B33" x="22.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A33" x="22.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B34" x="23.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A34" x="23.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B35" x="24.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A35" x="24.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B36" x="25.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A36" x="25.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B37" x="26.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A37" x="26.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B38" x="27.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A38" x="27.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B39" x="28.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A39" x="28.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B40" x="29.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A40" x="29.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B41" x="30.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A41" x="30.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B42" x="31.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A42" x="31.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B43" x="32.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A43" x="32.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B44" x="33.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A44" x="33.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B45" x="34.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A45" x="34.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B46" x="35.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A46" x="35.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B47" x="36.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A47" x="36.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B48" x="37.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A48" x="37.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B49" x="38.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A49" x="38.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B50" x="39.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A50" x="39.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B51" x="40.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A51" x="40.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B52" x="41.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A52" x="41.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B53" x="42.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A53" x="42.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B54" x="43.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A54" x="43.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B55" x="44.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A55" x="44.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B56" x="45.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A56" x="45.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B57" x="46.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A57" x="46.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B58" x="47.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A58" x="47.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B59" x="48.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A59" x="48.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B60" x="49.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A60" x="49.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B61" x="50.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A61" x="50.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B62" x="51.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A62" x="51.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B63" x="52.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A63" x="52.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B64" x="53.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A64" x="53.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B65" x="54.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A65" x="54.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B66" x="55.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A66" x="55.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B67" x="56.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A67" x="56.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B68" x="57.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A68" x="57.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B69" x="58.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A69" x="58.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B70" x="59.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A70" x="59.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B71" x="60.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A71" x="60.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B72" x="61.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A72" x="61.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B73" x="62.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A73" x="62.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B74" x="63.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A74" x="63.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B75" x="64.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A75" x="64.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B76" x="65.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A76" x="65.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B77" x="66.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A77" x="66.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B78" x="67.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A78" x="67.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B79" x="68.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A79" x="68.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B80" x="69.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A80" x="69.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B81" x="70.5" y="4" dx="3.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A81" x="70.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
<smd name="B82" x="71.5" y="3.5" dx="4.2" dy="0.7" layer="1" rot="R90"/>
<smd name="A82" x="71.5" y="3.5" dx="4.2" dy="0.7" layer="16" rot="R90"/>
</package>
</packages>
<symbols>
<symbol name="164PIN_EDGE">
<pin name="P1" x="-10.16" y="27.94" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P2" x="-10.16" y="25.4" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G1" x="10.16" y="27.94" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G2" x="10.16" y="25.4" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="A12" x="10.16" y="-2.54" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B12" x="-10.16" y="-2.54" visible="pin" length="short" swaplevel="1"/>
<pin name="P3" x="-10.16" y="22.86" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P4" x="-10.16" y="20.32" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G3" x="10.16" y="22.86" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G4" x="10.16" y="20.32" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P5" x="-10.16" y="17.78" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P6" x="-10.16" y="15.24" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G5" x="10.16" y="17.78" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G6" x="10.16" y="15.24" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P7" x="-10.16" y="12.7" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P8" x="-10.16" y="10.16" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G7" x="10.16" y="12.7" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G8" x="10.16" y="10.16" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P9" x="-10.16" y="7.62" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G9" x="10.16" y="7.62" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P10" x="-10.16" y="5.08" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P11" x="-10.16" y="2.54" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G10" x="10.16" y="5.08" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G11" x="10.16" y="2.54" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="A13" x="10.16" y="-5.08" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B13" x="-10.16" y="-5.08" visible="pin" length="short" swaplevel="1"/>
<pin name="A14" x="10.16" y="-7.62" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B14" x="-10.16" y="-7.62" visible="pin" length="short" swaplevel="1"/>
<pin name="A15" x="10.16" y="-10.16" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B15" x="-10.16" y="-10.16" visible="pin" length="short" swaplevel="1"/>
<pin name="A16" x="10.16" y="-12.7" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B16" x="-10.16" y="-12.7" visible="pin" length="short" swaplevel="1"/>
<pin name="A17" x="10.16" y="-15.24" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B17" x="-10.16" y="-15.24" visible="pin" length="short" swaplevel="1"/>
<pin name="A18" x="10.16" y="-17.78" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B18" x="-10.16" y="-17.78" visible="pin" length="short" swaplevel="1"/>
<pin name="A19" x="10.16" y="-20.32" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B19" x="-10.16" y="-20.32" visible="pin" length="short" swaplevel="1"/>
<pin name="A20" x="10.16" y="-22.86" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B20" x="-10.16" y="-22.86" visible="pin" length="short" swaplevel="1"/>
<pin name="A21" x="10.16" y="-25.4" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B21" x="-10.16" y="-25.4" visible="pin" length="short" swaplevel="1"/>
<pin name="A22" x="10.16" y="-27.94" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B22" x="-10.16" y="-27.94" visible="pin" length="short" swaplevel="1"/>
<pin name="A23" x="10.16" y="-30.48" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B23" x="-10.16" y="-30.48" visible="pin" length="short" swaplevel="1"/>
<pin name="A24" x="10.16" y="-33.02" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B24" x="-10.16" y="-33.02" visible="pin" length="short" swaplevel="1"/>
<pin name="A25" x="10.16" y="-35.56" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B25" x="-10.16" y="-35.56" visible="pin" length="short" swaplevel="1"/>
<pin name="A26" x="10.16" y="-38.1" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B26" x="-10.16" y="-38.1" visible="pin" length="short" swaplevel="1"/>
<pin name="A27" x="10.16" y="-40.64" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B27" x="-10.16" y="-40.64" visible="pin" length="short" swaplevel="1"/>
<pin name="A28" x="10.16" y="-43.18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B28" x="-10.16" y="-43.18" visible="pin" length="short" swaplevel="1"/>
<pin name="A29" x="10.16" y="-45.72" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B29" x="-10.16" y="-45.72" visible="pin" length="short" swaplevel="1"/>
<pin name="A30" x="10.16" y="-48.26" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B30" x="-10.16" y="-48.26" visible="pin" length="short" swaplevel="1"/>
<pin name="A31" x="10.16" y="-50.8" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B31" x="-10.16" y="-50.8" visible="pin" length="short" swaplevel="1"/>
<pin name="A32" x="10.16" y="-53.34" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B32" x="-10.16" y="-53.34" visible="pin" length="short" swaplevel="1"/>
<pin name="A33" x="10.16" y="-55.88" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B33" x="-10.16" y="-55.88" visible="pin" length="short" swaplevel="1"/>
<pin name="A34" x="10.16" y="-58.42" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B34" x="-10.16" y="-58.42" visible="pin" length="short" swaplevel="1"/>
<pin name="A35" x="10.16" y="-60.96" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B35" x="-10.16" y="-60.96" visible="pin" length="short" swaplevel="1"/>
<pin name="A36" x="10.16" y="-63.5" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B36" x="-10.16" y="-63.5" visible="pin" length="short" swaplevel="1"/>
<pin name="A37" x="10.16" y="-66.04" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B37" x="-10.16" y="-66.04" visible="pin" length="short" swaplevel="1"/>
<pin name="A38" x="10.16" y="-68.58" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B38" x="-10.16" y="-68.58" visible="pin" length="short" swaplevel="1"/>
<pin name="A39" x="10.16" y="-71.12" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B39" x="-10.16" y="-71.12" visible="pin" length="short" swaplevel="1"/>
<pin name="A40" x="10.16" y="-73.66" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B40" x="-10.16" y="-73.66" visible="pin" length="short" swaplevel="1"/>
<pin name="A41" x="10.16" y="-76.2" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B41" x="-10.16" y="-76.2" visible="pin" length="short" swaplevel="1"/>
<pin name="A42" x="10.16" y="-78.74" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B42" x="-10.16" y="-78.74" visible="pin" length="short" swaplevel="1"/>
<pin name="A43" x="10.16" y="-81.28" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B43" x="-10.16" y="-81.28" visible="pin" length="short" swaplevel="1"/>
<pin name="A44" x="10.16" y="-83.82" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B44" x="-10.16" y="-83.82" visible="pin" length="short" swaplevel="1"/>
<pin name="A45" x="10.16" y="-86.36" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B45" x="-10.16" y="-86.36" visible="pin" length="short" swaplevel="1"/>
<pin name="A46" x="10.16" y="-88.9" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B46" x="-10.16" y="-88.9" visible="pin" length="short" swaplevel="1"/>
<pin name="A47" x="10.16" y="-91.44" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B47" x="-10.16" y="-91.44" visible="pin" length="short" swaplevel="1"/>
<pin name="A48" x="10.16" y="-93.98" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B48" x="-10.16" y="-93.98" visible="pin" length="short" swaplevel="1"/>
<pin name="A49" x="10.16" y="-96.52" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B49" x="-10.16" y="-96.52" visible="pin" length="short" swaplevel="1"/>
<pin name="A50" x="10.16" y="-99.06" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B50" x="-10.16" y="-99.06" visible="pin" length="short" swaplevel="1"/>
<pin name="A51" x="10.16" y="-101.6" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B51" x="-10.16" y="-101.6" visible="pin" length="short" swaplevel="1"/>
<pin name="A52" x="10.16" y="-104.14" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B52" x="-10.16" y="-104.14" visible="pin" length="short" swaplevel="1"/>
<pin name="A53" x="10.16" y="-106.68" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B53" x="-10.16" y="-106.68" visible="pin" length="short" swaplevel="1"/>
<pin name="A54" x="10.16" y="-109.22" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B54" x="-10.16" y="-109.22" visible="pin" length="short" swaplevel="1"/>
<pin name="A55" x="10.16" y="-111.76" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B55" x="-10.16" y="-111.76" visible="pin" length="short" swaplevel="1"/>
<pin name="A56" x="10.16" y="-114.3" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B56" x="-10.16" y="-114.3" visible="pin" length="short" swaplevel="1"/>
<pin name="A57" x="10.16" y="-116.84" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B57" x="-10.16" y="-116.84" visible="pin" length="short" swaplevel="1"/>
<pin name="A58" x="10.16" y="-119.38" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B58" x="-10.16" y="-119.38" visible="pin" length="short" swaplevel="1"/>
<pin name="A59" x="10.16" y="-121.92" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B59" x="-10.16" y="-121.92" visible="pin" length="short" swaplevel="1"/>
<pin name="A60" x="10.16" y="-124.46" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B60" x="-10.16" y="-124.46" visible="pin" length="short" swaplevel="1"/>
<pin name="A61" x="10.16" y="-127" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B61" x="-10.16" y="-127" visible="pin" length="short" swaplevel="1"/>
<pin name="A62" x="10.16" y="-129.54" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B62" x="-10.16" y="-129.54" visible="pin" length="short" swaplevel="1"/>
<pin name="A63" x="10.16" y="-132.08" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B63" x="-10.16" y="-132.08" visible="pin" length="short" swaplevel="1"/>
<pin name="A64" x="10.16" y="-134.62" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B64" x="-10.16" y="-134.62" visible="pin" length="short" swaplevel="1"/>
<pin name="A65" x="10.16" y="-137.16" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B65" x="-10.16" y="-137.16" visible="pin" length="short" swaplevel="1"/>
<pin name="A66" x="10.16" y="-139.7" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B66" x="-10.16" y="-139.7" visible="pin" length="short" swaplevel="1"/>
<pin name="A67" x="10.16" y="-142.24" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B67" x="-10.16" y="-142.24" visible="pin" length="short" swaplevel="1"/>
<pin name="A68" x="10.16" y="-144.78" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B68" x="-10.16" y="-144.78" visible="pin" length="short" swaplevel="1"/>
<pin name="A69" x="10.16" y="-147.32" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B69" x="-10.16" y="-147.32" visible="pin" length="short" swaplevel="1"/>
<pin name="A70" x="10.16" y="-149.86" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B70" x="-10.16" y="-149.86" visible="pin" length="short" swaplevel="1"/>
<pin name="A71" x="10.16" y="-152.4" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B71" x="-10.16" y="-152.4" visible="pin" length="short" swaplevel="1"/>
<pin name="A72" x="10.16" y="-154.94" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B72" x="-10.16" y="-154.94" visible="pin" length="short" swaplevel="1"/>
<pin name="A73" x="10.16" y="-157.48" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B73" x="-10.16" y="-157.48" visible="pin" length="short" swaplevel="1"/>
<pin name="A74" x="10.16" y="-160.02" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B74" x="-10.16" y="-160.02" visible="pin" length="short" swaplevel="1"/>
<pin name="A75" x="10.16" y="-162.56" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B75" x="-10.16" y="-162.56" visible="pin" length="short" swaplevel="1"/>
<pin name="A76" x="10.16" y="-165.1" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B76" x="-10.16" y="-165.1" visible="pin" length="short" swaplevel="1"/>
<pin name="A77" x="10.16" y="-167.64" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B77" x="-10.16" y="-167.64" visible="pin" length="short" swaplevel="1"/>
<pin name="A78" x="10.16" y="-170.18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B78" x="-10.16" y="-170.18" visible="pin" length="short" swaplevel="1"/>
<pin name="A79" x="10.16" y="-172.72" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B79" x="-10.16" y="-172.72" visible="pin" length="short" swaplevel="1"/>
<pin name="A80" x="10.16" y="-175.26" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B80" x="-10.16" y="-175.26" visible="pin" length="short" swaplevel="1"/>
<pin name="A81" x="10.16" y="-177.8" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B81" x="-10.16" y="-177.8" visible="pin" length="short" swaplevel="1"/>
<pin name="A82" x="10.16" y="-180.34" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B82" x="-10.16" y="-180.34" visible="pin" length="short" swaplevel="1"/>
<wire x1="-7.62" y1="30.48" x2="7.62" y2="30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="-182.88" width="0.254" layer="94"/>
<wire x1="7.62" y1="-182.88" x2="-7.62" y2="-182.88" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-182.88" x2="-7.62" y2="30.48" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="EDGEBRD_AMPHENOL_164PCI" prefix="EDCONN">
<gates>
<gate name="G$1" symbol="164PIN_EDGE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EDGEBRD_AMPHENOL_164PCI">
<connects>
<connect gate="G$1" pin="A12" pad="A12"/>
<connect gate="G$1" pin="A13" pad="A13"/>
<connect gate="G$1" pin="A14" pad="A14"/>
<connect gate="G$1" pin="A15" pad="A15"/>
<connect gate="G$1" pin="A16" pad="A16"/>
<connect gate="G$1" pin="A17" pad="A17"/>
<connect gate="G$1" pin="A18" pad="A18"/>
<connect gate="G$1" pin="A19" pad="A19"/>
<connect gate="G$1" pin="A20" pad="A20"/>
<connect gate="G$1" pin="A21" pad="A21"/>
<connect gate="G$1" pin="A22" pad="A22"/>
<connect gate="G$1" pin="A23" pad="A23"/>
<connect gate="G$1" pin="A24" pad="A24"/>
<connect gate="G$1" pin="A25" pad="A25"/>
<connect gate="G$1" pin="A26" pad="A26"/>
<connect gate="G$1" pin="A27" pad="A27"/>
<connect gate="G$1" pin="A28" pad="A28"/>
<connect gate="G$1" pin="A29" pad="A29"/>
<connect gate="G$1" pin="A30" pad="A30"/>
<connect gate="G$1" pin="A31" pad="A31"/>
<connect gate="G$1" pin="A32" pad="A32"/>
<connect gate="G$1" pin="A33" pad="A33"/>
<connect gate="G$1" pin="A34" pad="A34"/>
<connect gate="G$1" pin="A35" pad="A35"/>
<connect gate="G$1" pin="A36" pad="A36"/>
<connect gate="G$1" pin="A37" pad="A37"/>
<connect gate="G$1" pin="A38" pad="A38"/>
<connect gate="G$1" pin="A39" pad="A39"/>
<connect gate="G$1" pin="A40" pad="A40"/>
<connect gate="G$1" pin="A41" pad="A41"/>
<connect gate="G$1" pin="A42" pad="A42"/>
<connect gate="G$1" pin="A43" pad="A43"/>
<connect gate="G$1" pin="A44" pad="A44"/>
<connect gate="G$1" pin="A45" pad="A45"/>
<connect gate="G$1" pin="A46" pad="A46"/>
<connect gate="G$1" pin="A47" pad="A47"/>
<connect gate="G$1" pin="A48" pad="A48"/>
<connect gate="G$1" pin="A49" pad="A49"/>
<connect gate="G$1" pin="A50" pad="A50"/>
<connect gate="G$1" pin="A51" pad="A51"/>
<connect gate="G$1" pin="A52" pad="A52"/>
<connect gate="G$1" pin="A53" pad="A53"/>
<connect gate="G$1" pin="A54" pad="A54"/>
<connect gate="G$1" pin="A55" pad="A55"/>
<connect gate="G$1" pin="A56" pad="A56"/>
<connect gate="G$1" pin="A57" pad="A57"/>
<connect gate="G$1" pin="A58" pad="A58"/>
<connect gate="G$1" pin="A59" pad="A59"/>
<connect gate="G$1" pin="A60" pad="A60"/>
<connect gate="G$1" pin="A61" pad="A61"/>
<connect gate="G$1" pin="A62" pad="A62"/>
<connect gate="G$1" pin="A63" pad="A63"/>
<connect gate="G$1" pin="A64" pad="A64"/>
<connect gate="G$1" pin="A65" pad="A65"/>
<connect gate="G$1" pin="A66" pad="A66"/>
<connect gate="G$1" pin="A67" pad="A67"/>
<connect gate="G$1" pin="A68" pad="A68"/>
<connect gate="G$1" pin="A69" pad="A69"/>
<connect gate="G$1" pin="A70" pad="A70"/>
<connect gate="G$1" pin="A71" pad="A71"/>
<connect gate="G$1" pin="A72" pad="A72"/>
<connect gate="G$1" pin="A73" pad="A73"/>
<connect gate="G$1" pin="A74" pad="A74"/>
<connect gate="G$1" pin="A75" pad="A75"/>
<connect gate="G$1" pin="A76" pad="A76"/>
<connect gate="G$1" pin="A77" pad="A77"/>
<connect gate="G$1" pin="A78" pad="A78"/>
<connect gate="G$1" pin="A79" pad="A79"/>
<connect gate="G$1" pin="A80" pad="A80"/>
<connect gate="G$1" pin="A81" pad="A81"/>
<connect gate="G$1" pin="A82" pad="A82"/>
<connect gate="G$1" pin="B12" pad="B12"/>
<connect gate="G$1" pin="B13" pad="B13"/>
<connect gate="G$1" pin="B14" pad="B14"/>
<connect gate="G$1" pin="B15" pad="B15"/>
<connect gate="G$1" pin="B16" pad="B16"/>
<connect gate="G$1" pin="B17" pad="B17"/>
<connect gate="G$1" pin="B18" pad="B18"/>
<connect gate="G$1" pin="B19" pad="B19"/>
<connect gate="G$1" pin="B20" pad="B20"/>
<connect gate="G$1" pin="B21" pad="B21"/>
<connect gate="G$1" pin="B22" pad="B22"/>
<connect gate="G$1" pin="B23" pad="B23"/>
<connect gate="G$1" pin="B24" pad="B24"/>
<connect gate="G$1" pin="B25" pad="B25"/>
<connect gate="G$1" pin="B26" pad="B26"/>
<connect gate="G$1" pin="B27" pad="B27"/>
<connect gate="G$1" pin="B28" pad="B28"/>
<connect gate="G$1" pin="B29" pad="B29"/>
<connect gate="G$1" pin="B30" pad="B30"/>
<connect gate="G$1" pin="B31" pad="B31"/>
<connect gate="G$1" pin="B32" pad="B32"/>
<connect gate="G$1" pin="B33" pad="B33"/>
<connect gate="G$1" pin="B34" pad="B34"/>
<connect gate="G$1" pin="B35" pad="B35"/>
<connect gate="G$1" pin="B36" pad="B36"/>
<connect gate="G$1" pin="B37" pad="B37"/>
<connect gate="G$1" pin="B38" pad="B38"/>
<connect gate="G$1" pin="B39" pad="B39"/>
<connect gate="G$1" pin="B40" pad="B40"/>
<connect gate="G$1" pin="B41" pad="B41"/>
<connect gate="G$1" pin="B42" pad="B42"/>
<connect gate="G$1" pin="B43" pad="B43"/>
<connect gate="G$1" pin="B44" pad="B44"/>
<connect gate="G$1" pin="B45" pad="B45"/>
<connect gate="G$1" pin="B46" pad="B46"/>
<connect gate="G$1" pin="B47" pad="B47"/>
<connect gate="G$1" pin="B48" pad="B48"/>
<connect gate="G$1" pin="B49" pad="B49"/>
<connect gate="G$1" pin="B50" pad="B50"/>
<connect gate="G$1" pin="B51" pad="B51"/>
<connect gate="G$1" pin="B52" pad="B52"/>
<connect gate="G$1" pin="B53" pad="B53"/>
<connect gate="G$1" pin="B54" pad="B54"/>
<connect gate="G$1" pin="B55" pad="B55"/>
<connect gate="G$1" pin="B56" pad="B56"/>
<connect gate="G$1" pin="B57" pad="B57"/>
<connect gate="G$1" pin="B58" pad="B58"/>
<connect gate="G$1" pin="B59" pad="B59"/>
<connect gate="G$1" pin="B60" pad="B60"/>
<connect gate="G$1" pin="B61" pad="B61"/>
<connect gate="G$1" pin="B62" pad="B62"/>
<connect gate="G$1" pin="B63" pad="B63"/>
<connect gate="G$1" pin="B64" pad="B64"/>
<connect gate="G$1" pin="B65" pad="B65"/>
<connect gate="G$1" pin="B66" pad="B66"/>
<connect gate="G$1" pin="B67" pad="B67"/>
<connect gate="G$1" pin="B68" pad="B68"/>
<connect gate="G$1" pin="B69" pad="B69"/>
<connect gate="G$1" pin="B70" pad="B70"/>
<connect gate="G$1" pin="B71" pad="B71"/>
<connect gate="G$1" pin="B72" pad="B72"/>
<connect gate="G$1" pin="B73" pad="B73"/>
<connect gate="G$1" pin="B74" pad="B74"/>
<connect gate="G$1" pin="B75" pad="B75"/>
<connect gate="G$1" pin="B76" pad="B76"/>
<connect gate="G$1" pin="B77" pad="B77"/>
<connect gate="G$1" pin="B78" pad="B78"/>
<connect gate="G$1" pin="B79" pad="B79"/>
<connect gate="G$1" pin="B80" pad="B80"/>
<connect gate="G$1" pin="B81" pad="B81"/>
<connect gate="G$1" pin="B82" pad="B82"/>
<connect gate="G$1" pin="G1" pad="A1"/>
<connect gate="G$1" pin="G10" pad="A10"/>
<connect gate="G$1" pin="G11" pad="A11"/>
<connect gate="G$1" pin="G2" pad="A2"/>
<connect gate="G$1" pin="G3" pad="A3"/>
<connect gate="G$1" pin="G4" pad="A4"/>
<connect gate="G$1" pin="G5" pad="A5"/>
<connect gate="G$1" pin="G6" pad="A6"/>
<connect gate="G$1" pin="G7" pad="A7"/>
<connect gate="G$1" pin="G8" pad="A8"/>
<connect gate="G$1" pin="G9" pad="A9"/>
<connect gate="G$1" pin="P1" pad="B1"/>
<connect gate="G$1" pin="P10" pad="B10"/>
<connect gate="G$1" pin="P11" pad="B11"/>
<connect gate="G$1" pin="P2" pad="B2"/>
<connect gate="G$1" pin="P3" pad="B3"/>
<connect gate="G$1" pin="P4" pad="B4"/>
<connect gate="G$1" pin="P5" pad="B5"/>
<connect gate="G$1" pin="P6" pad="B6"/>
<connect gate="G$1" pin="P7" pad="B7"/>
<connect gate="G$1" pin="P8" pad="B8"/>
<connect gate="G$1" pin="P9" pad="B9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-PowerSymbols" urn="urn:adsk.eagle:library:530">
<description>&lt;h3&gt;SparkFun Power Symbols&lt;/h3&gt;
This library contains power, ground, and voltage-supply symbols.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
</packages>
<symbols>
<symbol name="DGND" urn="urn:adsk.eagle:symbol:39415/1" library_version="1">
<description>&lt;h3&gt;Digital Ground Supply&lt;/h3&gt;</description>
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<text x="0" y="-0.254" size="1.778" layer="96" align="top-center">&gt;VALUE</text>
</symbol>
<symbol name="3.3V" urn="urn:adsk.eagle:symbol:39411/1" library_version="1">
<description>&lt;h3&gt;3.3V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="3.3V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="12V" urn="urn:adsk.eagle:symbol:39413/1" library_version="1">
<description>&lt;h3&gt;12V Voltage Supply&lt;/h3&gt;</description>
<wire x1="0.762" y1="1.27" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-0.762" y2="1.27" width="0.254" layer="94"/>
<pin name="12V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<text x="0" y="2.794" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:39439/1" prefix="GND" library_version="1">
<description>&lt;h3&gt;Ground Supply Symbol&lt;/h3&gt;
&lt;p&gt;Generic signal ground supply symbol.&lt;/p&gt;</description>
<gates>
<gate name="1" symbol="DGND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3.3V" urn="urn:adsk.eagle:component:39435/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;3.3V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 3.3V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="3.3V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="12V" urn="urn:adsk.eagle:component:39437/1" prefix="SUPPLY" library_version="1">
<description>&lt;h3&gt;12V Supply Symbol&lt;/h3&gt;
&lt;p&gt;Power supply symbol for a specifically-stated 12V source.&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AMR_Connectors">
<packages>
<package name="02JST-53048-THRU">
<pad name="2" x="0.625" y="0" drill="0.5"/>
<pad name="1" x="-0.625" y="0" drill="0.5"/>
<wire x1="-2.125" y1="1.05" x2="-2.125" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-2.125" y1="-4.45" x2="2.125" y2="-4.45" width="0.127" layer="21"/>
<wire x1="2.125" y1="-4.45" x2="2.125" y2="1.05" width="0.127" layer="21"/>
<wire x1="2.125" y1="1.05" x2="-2.125" y2="1.05" width="0.127" layer="21"/>
<text x="2.5" y="0" size="0.6096" layer="21">&gt;NAME</text>
<text x="-1.875" y="1.25" size="0.6096" layer="21">&gt;VALUE</text>
</package>
<package name="04JST-53048-THRU">
<pad name="2" x="-0.625" y="0" drill="0.5"/>
<pad name="3" x="0.625" y="0" drill="0.5"/>
<pad name="1" x="-1.875" y="0" drill="0.5"/>
<pad name="4" x="1.875" y="0" drill="0.5"/>
<wire x1="-3.375" y1="1.05" x2="-3.375" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-3.375" y1="-4.45" x2="3.375" y2="-4.45" width="0.127" layer="21"/>
<wire x1="3.375" y1="-4.45" x2="3.375" y2="1.05" width="0.127" layer="21"/>
<wire x1="3.375" y1="1.05" x2="-3.375" y2="1.05" width="0.127" layer="21"/>
<text x="3.75" y="0" size="0.6096" layer="21">&gt;NAME</text>
<text x="-3.125" y="1.25" size="0.6096" layer="21">&gt;VALUE</text>
</package>
<package name="08JST-53048-THRU">
<pad name="2" x="-3.125" y="0" drill="0.5"/>
<pad name="3" x="-1.875" y="0" drill="0.5"/>
<pad name="1" x="-4.375" y="0" drill="0.5"/>
<pad name="4" x="-0.625" y="0" drill="0.5"/>
<wire x1="-5.875" y1="1.05" x2="-5.875" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-5.875" y1="-4.45" x2="5.875" y2="-4.45" width="0.127" layer="21"/>
<wire x1="5.875" y1="-4.45" x2="5.875" y2="1.05" width="0.127" layer="21"/>
<wire x1="5.875" y1="1.05" x2="-5.875" y2="1.05" width="0.127" layer="21"/>
<text x="6.25" y="0" size="0.6096" layer="21">&gt;NAME</text>
<text x="-5.625" y="1.25" size="0.6096" layer="21">&gt;VALUE</text>
<pad name="5" x="0.605" y="0" drill="0.5"/>
<pad name="6" x="1.855" y="0" drill="0.5"/>
<pad name="7" x="3.165" y="0" drill="0.5"/>
<pad name="8" x="4.375" y="0" drill="0.5"/>
</package>
<package name="03JST-53048-THRU">
<pad name="2" x="0" y="0" drill="0.5"/>
<pad name="3" x="1.25" y="0" drill="0.5"/>
<pad name="1" x="-1.25" y="0" drill="0.5"/>
<wire x1="-2.75" y1="1.05" x2="-2.75" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-2.75" y1="-4.45" x2="2.75" y2="-4.45" width="0.127" layer="21"/>
<wire x1="2.75" y1="-4.45" x2="2.75" y2="1.05" width="0.127" layer="21"/>
<wire x1="2.75" y1="1.05" x2="-2.75" y2="1.05" width="0.127" layer="21"/>
<text x="3.125" y="0" size="0.6096" layer="21">&gt;NAME</text>
<text x="-2.5" y="1.25" size="0.6096" layer="21">&gt;VALUE</text>
</package>
<package name="06JST-53048-THRU">
<pad name="2" x="-1.875" y="0" drill="0.5"/>
<pad name="3" x="-0.625" y="0" drill="0.5"/>
<pad name="1" x="-3.125" y="0" drill="0.5"/>
<pad name="4" x="0.625" y="0" drill="0.5"/>
<wire x1="-4.625" y1="1.05" x2="-4.625" y2="-4.45" width="0.127" layer="21"/>
<wire x1="-4.625" y1="-4.45" x2="4.625" y2="-4.45" width="0.127" layer="21"/>
<wire x1="4.625" y1="-4.45" x2="4.625" y2="1.05" width="0.127" layer="21"/>
<wire x1="4.625" y1="1.05" x2="-4.625" y2="1.05" width="0.127" layer="21"/>
<text x="5" y="0" size="0.6096" layer="21">&gt;NAME</text>
<text x="-4.375" y="1.25" size="0.6096" layer="21">&gt;VALUE</text>
<pad name="6" x="3.125" y="0" drill="0.5"/>
<pad name="5" x="1.875" y="0" drill="0.5"/>
</package>
</packages>
<symbols>
<symbol name="JST02">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" length="middle" swaplevel="1" rot="R180"/>
<text x="-2.54" y="5.08" size="1.9304" layer="94">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.9304" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="JST04">
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<text x="-2.54" y="7.62" size="1.9304" layer="94">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.9304" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="JST08">
<wire x1="-2.54" y1="7.62" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="2.54" y1="-15.24" x2="-2.54" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<text x="-2.54" y="7.62" size="1.9304" layer="94">&gt;NAME</text>
<text x="-2.54" y="-17.78" size="1.9304" layer="94">&gt;VALUE</text>
<pin name="5" x="7.62" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<pin name="7" x="7.62" y="-10.16" length="middle" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-12.7" length="middle" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="JST03">
<wire x1="-2.54" y1="5.08" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="-2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<pin name="1" x="7.62" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<text x="-2.54" y="5.08" size="1.9304" layer="94">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.9304" layer="94">&gt;VALUE</text>
</symbol>
<symbol name="06-RJ11">
<wire x1="0" y1="7.62" x2="0" y2="-10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<pin name="1" x="10.16" y="5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="2" x="10.16" y="2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="3" x="10.16" y="0" length="middle" swaplevel="1" rot="R180"/>
<pin name="4" x="10.16" y="-2.54" length="middle" swaplevel="1" rot="R180"/>
<pin name="5" x="10.16" y="-5.08" length="middle" swaplevel="1" rot="R180"/>
<pin name="6" x="10.16" y="-7.62" length="middle" swaplevel="1" rot="R180"/>
<text x="0" y="7.62" size="1.27" layer="94">&gt;NAME</text>
<text x="0" y="-12.7" size="1.27" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="04JST-53048-THRU" prefix="JST">
<gates>
<gate name="G$1" symbol="JST04" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="04JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="08-53048-THRU" prefix="JST" uservalue="yes">
<gates>
<gate name="G$1" symbol="JST08" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="08JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="03JST-53048-THRU" prefix="JST">
<gates>
<gate name="G$1" symbol="JST03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="03JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="03-53048-THRU" prefix="JST" uservalue="yes">
<gates>
<gate name="G$1" symbol="JST03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="03JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="04-53048-THRU" prefix="JST" uservalue="yes">
<gates>
<gate name="G$1" symbol="JST04" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="04JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="02-53048-THRU" prefix="JST" uservalue="yes">
<gates>
<gate name="G$1" symbol="JST02" x="0" y="0"/>
</gates>
<devices>
<device name="" package="02JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="06-53048-THRU" prefix="JST" uservalue="yes">
<gates>
<gate name="G$1" symbol="06-RJ11" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="06JST-53048-THRU">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb" urn="urn:adsk.eagle:library:162">
<description>&lt;b&gt;Pin Headers&lt;/b&gt;&lt;p&gt;
Naming:&lt;p&gt;
MA = male&lt;p&gt;
# contacts - # rows&lt;p&gt;
W = angled&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MA03-1" urn="urn:adsk.eagle:footprint:8281/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-3.81" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="MA06-1" urn="urn:adsk.eagle:footprint:8287/1" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.62" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.985" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="5.715" y="1.651" size="1.27" layer="21" ratio="10">6</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="MA03-1" urn="urn:adsk.eagle:package:8339/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA03-1"/>
</packageinstances>
</package3d>
<package3d name="MA06-1" urn="urn:adsk.eagle:package:8340/1" type="box" library_version="1">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="MA06-1"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MA03-1" urn="urn:adsk.eagle:symbol:8280/1" library_version="1">
<wire x1="3.81" y1="-5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<text x="-1.27" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="MA06-1" urn="urn:adsk.eagle:symbol:8286/1" library_version="1">
<wire x1="3.81" y1="-10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-1.27" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA03-1" urn="urn:adsk.eagle:component:8376/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA03-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA03-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8339/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA06-1" urn="urn:adsk.eagle:component:8378/1" prefix="SV" uservalue="yes" library_version="1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA06-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:8340/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-Connectors" urn="urn:adsk.eagle:library:513">
<description>&lt;h3&gt;SparkFun Connectors&lt;/h3&gt;
This library contains electrically-functional connectors. 
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="2X5" urn="urn:adsk.eagle:footprint:37614/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.175" x2="-0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.905" y2="3.81" width="0.1524" layer="21"/>
<wire x1="1.905" y1="3.81" x2="3.175" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.175" y1="3.81" x2="3.81" y2="3.175" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="4.445" y2="3.81" width="0.1524" layer="21"/>
<wire x1="4.445" y1="3.81" x2="5.715" y2="3.81" width="0.1524" layer="21"/>
<wire x1="5.715" y1="3.81" x2="6.35" y2="3.175" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.985" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.81" x2="8.255" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.255" y1="3.81" x2="8.89" y2="3.175" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.175" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="3.175" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="3.175" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="9.525" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-1.27" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="3.175" x2="9.525" y2="3.81" width="0.1524" layer="21"/>
<wire x1="9.525" y1="3.81" x2="10.795" y2="3.81" width="0.1524" layer="21"/>
<wire x1="10.795" y1="3.81" x2="11.43" y2="3.175" width="0.1524" layer="21"/>
<wire x1="11.43" y1="3.175" x2="11.43" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="10.795" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.651" x2="0.635" y2="-1.651" width="0.2032" layer="22"/>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<text x="-1.27" y="3.937" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RA" urn="urn:adsk.eagle:footprint:37733/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Male Headers&lt;/h3&gt;
tDocu shows pin location. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.8" y1="6.3" x2="5.3" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="6.3" x2="5.3" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="-6.3" x2="2.8" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.8" y1="-6.3" x2="2.8" y2="6.3" width="0.2032" layer="21"/>
<wire x1="5.3" y1="0" x2="11.3" y2="0" width="0.127" layer="51"/>
<wire x1="5.3" y1="-2.54" x2="11.3" y2="-2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="-5.08" x2="11.3" y2="-5.08" width="0.127" layer="51"/>
<wire x1="5.3" y1="2.54" x2="11.3" y2="2.54" width="0.127" layer="51"/>
<wire x1="5.3" y1="5.08" x2="11.3" y2="5.08" width="0.127" layer="51"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="13.8" y1="6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="6.3" x2="13.8" y2="6.3" width="0.127" layer="51"/>
<wire x1="5.3" y1="-6.3" x2="13.8" y2="-6.3" width="0.127" layer="51"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="2.54" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="2.54" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-RAF" urn="urn:adsk.eagle:footprint:37734/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Right Angle Female Header&lt;/h3&gt;
Silk outline of pins
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="21"/>
<wire x1="2.7" y1="6.3" x2="11.2" y2="6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="6.3" x2="11.2" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="11.2" y1="-6.3" x2="2.7" y2="-6.3" width="0.2032" layer="21"/>
<wire x1="2.7" y1="-6.3" x2="2.7" y2="6.3" width="0.2032" layer="21"/>
<wire x1="8.2" y1="7" x2="8.2" y2="-6.9" width="0.127" layer="51"/>
<wire x1="-2.54" y1="5.715" x2="-2.54" y2="4.445" width="0.2032" layer="22"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="3.175" y="6.477" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="3.175" y="-7.112" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED" urn="urn:adsk.eagle:footprint:37615/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.318" y="10.414" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED_LOCK" urn="urn:adsk.eagle:footprint:37616/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header Locking Footprint&lt;/h3&gt;
Holes are offset 0.005" from center, to hold pins in place during soldering. 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<pad name="1" x="-1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="2" x="1.397" y="5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="3" x="-1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="4" x="1.397" y="2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="5" x="-1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="6" x="1.397" y="0" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="7" x="-1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="8" x="1.397" y="-2.54" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="9" x="-1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<pad name="10" x="1.397" y="-5.08" drill="1.016" diameter="1.8796" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51" rot="R270"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51" rot="R270"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51" rot="R270"/>
<text x="-4.191" y="10.541" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.318" y="-11.049" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED_SMD" urn="urn:adsk.eagle:footprint:37617/1" library_version="1">
<description>&lt;h3&gt;Surface Mount - 2x5 Shrouded Header&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-5.315" y1="5.715" x2="-5.315" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="21"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="21"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-5.353" y1="5.715" x2="-5.353" y2="4.445" width="0.2032" layer="22"/>
<smd name="1" x="-2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="2" x="2.794" y="5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="3" x="-2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="4" x="2.794" y="2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="5" x="-2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="6" x="2.794" y="0" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="7" x="-2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="8" x="2.794" y="-2.54" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="9" x="-2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<smd name="10" x="2.794" y="-5.08" dx="4.15" dy="1" layer="1" roundness="50"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_NOSILK" urn="urn:adsk.eagle:footprint:37735/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 No Silk Outline&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="1.016" diameter="1.8796"/>
<pad name="2" x="0" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.8796"/>
<pad name="4" x="2.54" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="5" x="5.08" y="0" drill="1.016" diameter="1.8796"/>
<pad name="6" x="5.08" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="7" x="7.62" y="0" drill="1.016" diameter="1.8796"/>
<pad name="8" x="7.62" y="2.54" drill="1.016" diameter="1.8796"/>
<pad name="9" x="10.16" y="0" drill="1.016" diameter="1.8796"/>
<pad name="10" x="10.16" y="2.54" drill="1.016" diameter="1.8796"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="-0.254" y1="2.286" x2="0.254" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="2.286" x2="2.794" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="2.286" x2="10.414" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="9.906" y1="-0.254" x2="10.414" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51" rot="R90"/>
<rectangle x1="7.366" y1="2.286" x2="7.874" y2="2.794" layer="51" rot="R90"/>
<rectangle x1="4.826" y1="2.286" x2="5.334" y2="2.794" layer="51" rot="R90"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="51"/>
<text x="-0.889" y="3.81" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-2.159" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5_PTH_SILK_.05" urn="urn:adsk.eagle:footprint:37736/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5&lt;/h3&gt;
Holes are 0.05". 
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<pad name="1" x="0" y="0" drill="0.4318" rot="R90"/>
<pad name="2" x="0" y="1.27" drill="0.4318" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="0.4318" rot="R90"/>
<pad name="4" x="1.27" y="1.27" drill="0.4318" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="0.4318" rot="R90"/>
<pad name="6" x="2.54" y="1.27" drill="0.4318" rot="R90"/>
<pad name="7" x="3.81" y="0" drill="0.4318" rot="R90"/>
<pad name="8" x="3.81" y="1.27" drill="0.4318" rot="R90"/>
<pad name="9" x="5.08" y="0" drill="0.4318" rot="R90"/>
<pad name="10" x="5.08" y="1.27" drill="0.4318" rot="R90"/>
<wire x1="-0.635" y1="0.635" x2="-0.762" y2="0.762" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.762" x2="-0.762" y2="1.778" width="0.127" layer="21"/>
<wire x1="-0.762" y1="1.778" x2="-0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="-0.508" y1="2.032" x2="0.508" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.508" y1="2.032" x2="0.635" y2="1.905" width="0.127" layer="21"/>
<wire x1="0.635" y1="1.905" x2="0.762" y2="2.032" width="0.127" layer="21"/>
<wire x1="0.762" y1="2.032" x2="1.778" y2="2.032" width="0.127" layer="21"/>
<wire x1="1.778" y1="2.032" x2="1.905" y2="1.905" width="0.127" layer="21"/>
<wire x1="1.905" y1="1.905" x2="2.032" y2="2.032" width="0.127" layer="21"/>
<wire x1="2.032" y1="2.032" x2="3.048" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.048" y1="2.032" x2="3.175" y2="1.905" width="0.127" layer="21"/>
<wire x1="3.175" y1="1.905" x2="3.302" y2="2.032" width="0.127" layer="21"/>
<wire x1="3.302" y1="2.032" x2="4.318" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.318" y1="2.032" x2="4.445" y2="1.905" width="0.127" layer="21"/>
<wire x1="4.445" y1="1.905" x2="4.572" y2="2.032" width="0.127" layer="21"/>
<wire x1="4.572" y1="2.032" x2="5.588" y2="2.032" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.032" x2="5.842" y2="1.778" width="0.127" layer="21"/>
<wire x1="5.842" y1="1.778" x2="5.842" y2="0.762" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.762" x2="5.715" y2="0.635" width="0.127" layer="21"/>
<wire x1="5.715" y1="0.635" x2="5.842" y2="0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="0.508" x2="5.842" y2="-0.508" width="0.127" layer="21"/>
<wire x1="5.842" y1="-0.508" x2="5.588" y2="-0.762" width="0.127" layer="21"/>
<wire x1="5.588" y1="-0.762" x2="4.572" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.572" y1="-0.762" x2="4.445" y2="-0.635" width="0.127" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="4.318" y2="-0.762" width="0.127" layer="21"/>
<wire x1="4.318" y1="-0.762" x2="3.302" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.175" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.635" x2="3.048" y2="-0.762" width="0.127" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="2.032" y2="-0.762" width="0.127" layer="21"/>
<wire x1="2.032" y1="-0.762" x2="1.905" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.905" y1="-0.635" x2="1.778" y2="-0.762" width="0.127" layer="21"/>
<wire x1="1.778" y1="-0.762" x2="0.762" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.762" y1="-0.762" x2="0.635" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.635" x2="0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.762" x2="-0.508" y2="-0.762" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.762" x2="-0.762" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="-0.508" x2="-0.762" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.762" y1="0.508" x2="-0.635" y2="0.635" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="22"/>
<text x="-0.762" y="2.286" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-0.762" y="-1.778" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
<package name="2X5-SHROUDED-NS" urn="urn:adsk.eagle:footprint:37737/1" library_version="1">
<description>&lt;h3&gt;Plated Through Hole - 2x5 Shrouded Header No Silk&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:10&lt;/li&gt;
&lt;li&gt;Pin pitch:0.1"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;&lt;a href=”https://www.sparkfun.com/datasheets/Prototyping/Shrouded-10pin.pdf”&gt;Datasheet referenced for footprint&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_05x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-2.775" y1="5.715" x2="-2.775" y2="4.445" width="0.2032" layer="21"/>
<wire x1="4.5" y1="10.1" x2="4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-10.1" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="-2.2" x2="-4.5" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-4.5" y2="10.1" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="10.1" x2="4.4" y2="10.1" width="0.2032" layer="51"/>
<wire x1="4.5" y1="-10.1" x2="-4.5" y2="-10.1" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="3.4" y2="9" width="0.2032" layer="51"/>
<wire x1="3.4" y1="9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="3.4" y2="-9" width="0.2032" layer="51"/>
<wire x1="-4.5" y1="2.2" x2="-3" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.2" x2="-3" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.2" x2="-4.5" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="9" x2="-3.4" y2="2.2" width="0.2032" layer="51"/>
<wire x1="-3.4" y1="-9" x2="-3.4" y2="-2.2" width="0.2032" layer="51"/>
<wire x1="-2.813" y1="5.715" x2="-2.813" y2="4.445" width="0.2032" layer="22"/>
<pad name="1" x="-1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="2" x="1.27" y="5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="3" x="-1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="4" x="1.27" y="2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="5" x="-1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="6" x="1.27" y="0" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="7" x="-1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="8" x="1.27" y="-2.54" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="9" x="-1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<pad name="10" x="1.27" y="-5.08" drill="1.016" diameter="1.8796" shape="octagon" rot="R270"/>
<rectangle x1="-1.524" y1="4.826" x2="-1.016" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="4.826" x2="1.524" y2="5.334" layer="51"/>
<rectangle x1="1.016" y1="2.286" x2="1.524" y2="2.794" layer="51"/>
<rectangle x1="-1.524" y1="2.286" x2="-1.016" y2="2.794" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-5.334" x2="1.524" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-5.334" x2="-1.016" y2="-4.826" layer="51"/>
<rectangle x1="-1.524" y1="-2.794" x2="-1.016" y2="-2.286" layer="51"/>
<rectangle x1="1.016" y1="-2.794" x2="1.524" y2="-2.286" layer="51"/>
<text x="-4.445" y="10.287" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-4.445" y="-10.922" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="2X5" urn="urn:adsk.eagle:package:38012/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5
Specifications:
Pin count:10
Pin pitch:0.1"

Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5"/>
</packageinstances>
</package3d>
<package3d name="2X5-RA" urn="urn:adsk.eagle:package:38115/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 Right Angle Male Headers
tDocu shows pin location. 
Specifications:
Pin count:10
Pin pitch:0.1"

Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-RA"/>
</packageinstances>
</package3d>
<package3d name="2X5-RAF" urn="urn:adsk.eagle:package:38113/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 Right Angle Female Header
Silk outline of pins
Specifications:
Pin count:10
Pin pitch:0.1"

Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-RAF"/>
</packageinstances>
</package3d>
<package3d name="2X5-SHROUDED" urn="urn:adsk.eagle:package:38000/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 Shrouded Header
Specifications:
Pin count:10
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-SHROUDED"/>
</packageinstances>
</package3d>
<package3d name="2X5-SHROUDED_LOCK" urn="urn:adsk.eagle:package:38007/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 Shrouded Header Locking Footprint
Holes are offset 0.005" from center, to hold pins in place during soldering. 
Specifications:
Pin count:10
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-SHROUDED_LOCK"/>
</packageinstances>
</package3d>
<package3d name="2X5-SHROUDED_SMD" urn="urn:adsk.eagle:package:38003/1" type="box" library_version="1">
<description>Surface Mount - 2x5 Shrouded Header
Specifications:
Pin count:10
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-SHROUDED_SMD"/>
</packageinstances>
</package3d>
<package3d name="2X5_NOSILK" urn="urn:adsk.eagle:package:38114/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 No Silk Outline
Specifications:
Pin count:10
Pin pitch:0.1"

Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5_NOSILK"/>
</packageinstances>
</package3d>
<package3d name="2X5_PTH_SILK_.05" urn="urn:adsk.eagle:package:38116/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5
Holes are 0.05". 
Specifications:
Pin count:10
Pin pitch:0.1"

Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5_PTH_SILK_.05"/>
</packageinstances>
</package3d>
<package3d name="2X5-SHROUDED-NS" urn="urn:adsk.eagle:package:38119/1" type="box" library_version="1">
<description>Plated Through Hole - 2x5 Shrouded Header No Silk
Specifications:
Pin count:10
Pin pitch:0.1"

Datasheet referenced for footprint
Example device(s):
CONN_05x2
</description>
<packageinstances>
<packageinstance name="2X5-SHROUDED-NS"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="CONN_05X2" urn="urn:adsk.eagle:symbol:37732/1" library_version="1">
<description>&lt;h3&gt;10 Pin Connection&lt;/h3&gt;
5x2 pin layout</description>
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="-2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="-2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="-2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="5.08" x2="-2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="-2.54" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-9.906" size="1.778" layer="96" font="vector">&gt;VALUE</text>
<text x="-3.81" y="8.128" size="1.778" layer="95" font="vector">&gt;NAME</text>
<pin name="10" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="5" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="3" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="1" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_05X2" urn="urn:adsk.eagle:component:38328/1" prefix="J" uservalue="yes" library_version="1">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;h3&gt;For AVR SPI programming port, see special device with nice symbol: "AVR_SPI_PROG_5x2.dev"&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/778"&gt; 2x5 AVR ICSP Male Header&lt;/a&gt; (PRT-00778)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8506"&gt; 2x5 Pin Shrouded Header&lt;/a&gt; (PRT-08506)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 5x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/8535"&gt; 2x5 Pin IDC Ribbon Cable&lt;/a&gt; (PRT-08535)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="CONN_05X2" x="0" y="0"/>
</gates>
<devices>
<device name="PTH" package="2X5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38012/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08499" constant="no"/>
<attribute name="SF_ID" value="PRT-0778" constant="no"/>
</technology>
</technologies>
</device>
<device name="RA" package="2X5-RA">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38115/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RAF" package="2X5-RAF">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38113/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD" package="2X5-SHROUDED">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38000/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_LOCK" package="2X5-SHROUDED_LOCK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38007/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
<device name="SHD_SMD" package="2X5-SHROUDED_SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38003/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-09508" constant="no"/>
</technology>
</technologies>
</device>
<device name="NO_SILK" package="2X5_NOSILK">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38114/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0.05_IN_PTH_SILK" package="2X5_PTH_SILK_.05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38116/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SHD-NS" package="2X5-SHROUDED-NS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:38119/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-08671" constant="no"/>
<attribute name="SF_ID" value="PRT-08506" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="EDCONN2" library="AggieMarineRobotics" deviceset="EDGEBRD_AMPHENOL_164PCI" device=""/>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND9" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND10" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="GND14" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="JST10" library="AMR_Connectors" deviceset="04JST-53048-THRU" device="" value="MOTOR"/>
<part name="JST4" library="AMR_Connectors" deviceset="04JST-53048-THRU" device="" value="JET_SERIAL"/>
<part name="JST6" library="AMR_Connectors" deviceset="08-53048-THRU" device="" value="IMU"/>
<part name="JST8" library="AMR_Connectors" deviceset="03JST-53048-THRU" device="" value="PRESS"/>
<part name="JST7" library="AMR_Connectors" deviceset="03JST-53048-THRU" device="" value="TEMP"/>
<part name="GND1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="GND7" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="JST2" library="AMR_Connectors" deviceset="03-53048-THRU" device="" value="JET_KILLSW"/>
<part name="JST3" library="AMR_Connectors" deviceset="04-53048-THRU" device="" value="PWR_CUTOFF"/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="JST5" library="AMR_Connectors" deviceset="02-53048-THRU" device="" value="JET_UART2"/>
<part name="GND6" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="SUPPLY1" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SUPPLY2" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="SV1" library="SparkFun-Connectors" library_urn="urn:adsk.eagle:library:513" deviceset="CONN_05X2" device="PTH" package3d_urn="urn:adsk.eagle:package:38012/1" value="IMU_AUX-DIG_AUX"/>
<part name="SV2" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA06-1" device="" package3d_urn="urn:adsk.eagle:package:8340/1" value="ANALOG"/>
<part name="JST9" library="AMR_Connectors" deviceset="06-53048-THRU" device="" value="PROG"/>
<part name="GND11" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="JST1" library="AMR_Connectors" deviceset="02-53048-THRU" device="" value="JET_PWR"/>
<part name="SV3" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-1" device="" package3d_urn="urn:adsk.eagle:package:8339/1" value="5V"/>
<part name="SV4" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-1" device="" package3d_urn="urn:adsk.eagle:package:8339/1" value="3.3V"/>
<part name="SV5" library="con-lstb" library_urn="urn:adsk.eagle:library:162" deviceset="MA03-1" device="" package3d_urn="urn:adsk.eagle:package:8339/1" value="GND"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="SUPPLY3" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="3.3V" device=""/>
<part name="GND12" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="GND" device=""/>
<part name="JST11" library="AMR_Connectors" deviceset="03JST-53048-THRU" device="" value="IMU_AUX"/>
<part name="SUPPLY4" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="12V" device="" value="24V"/>
<part name="SUPPLY5" library="SparkFun-PowerSymbols" library_urn="urn:adsk.eagle:library:530" deviceset="12V" device="" value="24V"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="EDCONN2" gate="G$1" x="17.78" y="185.42"/>
<instance part="P+1" gate="1" x="-17.78" y="10.16"/>
<instance part="P+2" gate="1" x="5.08" y="218.44"/>
<instance part="GND2" gate="1" x="114.3" y="167.64"/>
<instance part="GND3" gate="1" x="101.6" y="144.78"/>
<instance part="GND9" gate="1" x="152.4" y="182.88"/>
<instance part="P+5" gate="1" x="152.4" y="190.5"/>
<instance part="GND10" gate="1" x="149.86" y="162.56"/>
<instance part="P+6" gate="1" x="149.86" y="170.18"/>
<instance part="GND14" gate="1" x="99.06" y="76.2"/>
<instance part="JST10" gate="G$1" x="233.68" y="190.5" rot="R180"/>
<instance part="JST4" gate="G$1" x="129.54" y="132.08" rot="R180"/>
<instance part="JST6" gate="G$1" x="129.54" y="83.82" rot="R180"/>
<instance part="JST8" gate="G$1" x="182.88" y="167.64" rot="R180"/>
<instance part="JST7" gate="G$1" x="182.88" y="187.96" rot="R180"/>
<instance part="GND1" gate="1" x="33.02" y="162.56"/>
<instance part="GND4" gate="1" x="33.02" y="121.92"/>
<instance part="GND5" gate="1" x="33.02" y="7.62"/>
<instance part="GND7" gate="1" x="33.02" y="175.26"/>
<instance part="JST2" gate="G$1" x="129.54" y="172.72" rot="R180"/>
<instance part="JST3" gate="G$1" x="129.54" y="152.4" rot="R180"/>
<instance part="P+3" gate="1" x="101.6" y="152.4"/>
<instance part="JST5" gate="G$1" x="129.54" y="114.3" rot="R180"/>
<instance part="GND6" gate="1" x="33.02" y="210.82"/>
<instance part="SUPPLY1" gate="G$1" x="2.54" y="203.2"/>
<instance part="SUPPLY2" gate="G$1" x="99.06" y="81.28"/>
<instance part="SV1" gate="G$1" x="182.88" y="127" rot="R180"/>
<instance part="SV2" gate="1" x="182.88" y="104.14" rot="R180"/>
<instance part="JST9" gate="G$1" x="185.42" y="144.78" rot="R180"/>
<instance part="GND11" gate="1" x="149.86" y="137.16"/>
<instance part="JST1" gate="G$1" x="129.54" y="190.5" rot="R180"/>
<instance part="SV3" gate="G$1" x="233.68" y="157.48" rot="R180"/>
<instance part="SV4" gate="G$1" x="233.68" y="137.16" rot="R180"/>
<instance part="SV5" gate="G$1" x="233.68" y="114.3" rot="R180"/>
<instance part="P+7" gate="1" x="223.52" y="162.56"/>
<instance part="SUPPLY3" gate="G$1" x="223.52" y="139.7"/>
<instance part="GND12" gate="1" x="223.52" y="109.22"/>
<instance part="JST11" gate="G$1" x="182.88" y="83.82" rot="R180"/>
<instance part="SUPPLY4" gate="G$1" x="-2.54" y="200.66"/>
<instance part="SUPPLY5" gate="G$1" x="149.86" y="142.24"/>
</instances>
<busses>
</busses>
<nets>
<net name="MISO" class="0">
<segment>
<label x="119.38" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="119.38" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<pinref part="JST6" gate="G$1" pin="7"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B13"/>
<wire x1="7.62" y1="180.34" x2="5.08" y2="180.34" width="0.1524" layer="91"/>
<label x="5.08" y="180.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<label x="119.38" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="119.38" y1="96.52" x2="121.92" y2="96.52" width="0.1524" layer="91"/>
<pinref part="JST6" gate="G$1" pin="8"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B12"/>
<wire x1="7.62" y1="182.88" x2="5.08" y2="182.88" width="0.1524" layer="91"/>
<label x="5.08" y="182.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SS_IMU" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B17"/>
<wire x1="7.62" y1="170.18" x2="5.08" y2="170.18" width="0.1524" layer="91"/>
<label x="5.08" y="170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST6" gate="G$1" pin="5"/>
<wire x1="121.92" y1="88.9" x2="119.38" y2="88.9" width="0.1524" layer="91"/>
<label x="119.38" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="KILL_SW" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B24"/>
<wire x1="7.62" y1="152.4" x2="5.08" y2="152.4" width="0.1524" layer="91"/>
<label x="5.08" y="152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST3" gate="G$1" pin="4"/>
<wire x1="121.92" y1="154.94" x2="119.38" y2="154.94" width="0.1524" layer="91"/>
<label x="119.38" y="154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="KILL_CONT" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B25"/>
<wire x1="7.62" y1="149.86" x2="5.08" y2="149.86" width="0.1524" layer="91"/>
<label x="5.08" y="149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="121.92" y1="152.4" x2="119.38" y2="152.4" width="0.1524" layer="91"/>
<label x="119.38" y="152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST3" gate="G$1" pin="3"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="121.92" y1="170.18" x2="114.3" y2="170.18" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="JST2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="121.92" y1="147.32" x2="101.6" y2="147.32" width="0.1524" layer="91"/>
<pinref part="JST3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="175.26" y1="185.42" x2="152.4" y2="185.42" width="0.1524" layer="91"/>
<pinref part="JST7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="175.26" y1="165.1" x2="149.86" y2="165.1" width="0.1524" layer="91"/>
<pinref part="JST8" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="121.92" y1="78.74" x2="99.06" y2="78.74" width="0.1524" layer="91"/>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="JST6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="EDCONN2" gate="G$1" pin="A35"/>
<wire x1="27.94" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A22"/>
<pinref part="EDCONN2" gate="G$1" pin="A23"/>
<wire x1="27.94" y1="157.48" x2="27.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A24"/>
<wire x1="27.94" y1="154.94" x2="27.94" y2="152.4" width="0.1524" layer="91"/>
<junction x="27.94" y="154.94"/>
<pinref part="EDCONN2" gate="G$1" pin="A25"/>
<wire x1="27.94" y1="152.4" x2="27.94" y2="149.86" width="0.1524" layer="91"/>
<junction x="27.94" y="152.4"/>
<pinref part="EDCONN2" gate="G$1" pin="A26"/>
<wire x1="27.94" y1="149.86" x2="27.94" y2="147.32" width="0.1524" layer="91"/>
<junction x="27.94" y="149.86"/>
<pinref part="EDCONN2" gate="G$1" pin="A27"/>
<wire x1="27.94" y1="147.32" x2="27.94" y2="144.78" width="0.1524" layer="91"/>
<junction x="27.94" y="147.32"/>
<pinref part="EDCONN2" gate="G$1" pin="A28"/>
<wire x1="27.94" y1="144.78" x2="27.94" y2="142.24" width="0.1524" layer="91"/>
<junction x="27.94" y="144.78"/>
<pinref part="EDCONN2" gate="G$1" pin="A29"/>
<wire x1="27.94" y1="142.24" x2="27.94" y2="139.7" width="0.1524" layer="91"/>
<junction x="27.94" y="142.24"/>
<pinref part="EDCONN2" gate="G$1" pin="A30"/>
<wire x1="27.94" y1="139.7" x2="27.94" y2="137.16" width="0.1524" layer="91"/>
<junction x="27.94" y="139.7"/>
<pinref part="EDCONN2" gate="G$1" pin="A31"/>
<wire x1="27.94" y1="137.16" x2="27.94" y2="134.62" width="0.1524" layer="91"/>
<junction x="27.94" y="137.16"/>
<pinref part="EDCONN2" gate="G$1" pin="A32"/>
<wire x1="27.94" y1="134.62" x2="27.94" y2="132.08" width="0.1524" layer="91"/>
<junction x="27.94" y="134.62"/>
<pinref part="EDCONN2" gate="G$1" pin="A33"/>
<wire x1="27.94" y1="132.08" x2="27.94" y2="129.54" width="0.1524" layer="91"/>
<junction x="27.94" y="132.08"/>
<pinref part="EDCONN2" gate="G$1" pin="A34"/>
<wire x1="27.94" y1="129.54" x2="27.94" y2="127" width="0.1524" layer="91"/>
<junction x="27.94" y="129.54"/>
<wire x1="27.94" y1="127" x2="27.94" y2="124.46" width="0.1524" layer="91"/>
<junction x="27.94" y="127"/>
<junction x="27.94" y="124.46"/>
<pinref part="EDCONN2" gate="G$1" pin="A36"/>
<wire x1="27.94" y1="124.46" x2="27.94" y2="121.92" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A37"/>
<wire x1="27.94" y1="121.92" x2="27.94" y2="119.38" width="0.1524" layer="91"/>
<junction x="27.94" y="121.92"/>
<pinref part="EDCONN2" gate="G$1" pin="A38"/>
<wire x1="27.94" y1="119.38" x2="27.94" y2="116.84" width="0.1524" layer="91"/>
<junction x="27.94" y="119.38"/>
<pinref part="EDCONN2" gate="G$1" pin="A39"/>
<wire x1="27.94" y1="116.84" x2="27.94" y2="114.3" width="0.1524" layer="91"/>
<junction x="27.94" y="116.84"/>
<pinref part="EDCONN2" gate="G$1" pin="A40"/>
<wire x1="27.94" y1="114.3" x2="27.94" y2="111.76" width="0.1524" layer="91"/>
<junction x="27.94" y="114.3"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="A80"/>
<wire x1="27.94" y1="10.16" x2="33.02" y2="10.16" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A79"/>
<wire x1="27.94" y1="10.16" x2="27.94" y2="12.7" width="0.1524" layer="91"/>
<junction x="27.94" y="10.16"/>
<pinref part="EDCONN2" gate="G$1" pin="A78"/>
<wire x1="27.94" y1="12.7" x2="27.94" y2="15.24" width="0.1524" layer="91"/>
<junction x="27.94" y="12.7"/>
<pinref part="EDCONN2" gate="G$1" pin="A77"/>
<wire x1="27.94" y1="15.24" x2="27.94" y2="17.78" width="0.1524" layer="91"/>
<junction x="27.94" y="15.24"/>
<pinref part="EDCONN2" gate="G$1" pin="A76"/>
<wire x1="27.94" y1="17.78" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<junction x="27.94" y="17.78"/>
<pinref part="EDCONN2" gate="G$1" pin="A75"/>
<wire x1="27.94" y1="20.32" x2="27.94" y2="22.86" width="0.1524" layer="91"/>
<junction x="27.94" y="20.32"/>
<pinref part="EDCONN2" gate="G$1" pin="A74"/>
<wire x1="27.94" y1="22.86" x2="27.94" y2="25.4" width="0.1524" layer="91"/>
<junction x="27.94" y="22.86"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="A17"/>
<wire x1="27.94" y1="170.18" x2="27.94" y2="167.64" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A18"/>
<junction x="27.94" y="167.64"/>
<wire x1="27.94" y1="167.64" x2="27.94" y2="165.1" width="0.1524" layer="91"/>
<pinref part="EDCONN2" gate="G$1" pin="A19"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="27.94" y1="165.1" x2="33.02" y2="165.1" width="0.1524" layer="91"/>
<junction x="27.94" y="165.1"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="A14"/>
<pinref part="EDCONN2" gate="G$1" pin="A13"/>
<pinref part="EDCONN2" gate="G$1" pin="A12"/>
<wire x1="27.94" y1="182.88" x2="27.94" y2="180.34" width="0.1524" layer="91"/>
<junction x="27.94" y="180.34"/>
<wire x1="27.94" y1="180.34" x2="27.94" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="27.94" y1="177.8" x2="33.02" y2="177.8" width="0.1524" layer="91"/>
<junction x="27.94" y="177.8"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="G4"/>
<pinref part="EDCONN2" gate="G$1" pin="G3"/>
<pinref part="EDCONN2" gate="G$1" pin="G2"/>
<pinref part="EDCONN2" gate="G$1" pin="G1"/>
<wire x1="27.94" y1="213.36" x2="27.94" y2="210.82" width="0.1524" layer="91"/>
<junction x="27.94" y="210.82"/>
<wire x1="27.94" y1="210.82" x2="27.94" y2="208.28" width="0.1524" layer="91"/>
<wire x1="27.94" y1="208.28" x2="27.94" y2="205.74" width="0.1524" layer="91"/>
<junction x="27.94" y="208.28"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="33.02" y1="213.36" x2="27.94" y2="213.36" width="0.1524" layer="91"/>
<junction x="27.94" y="213.36"/>
<pinref part="EDCONN2" gate="G$1" pin="G6"/>
<pinref part="EDCONN2" gate="G$1" pin="G5"/>
<wire x1="27.94" y1="203.2" x2="27.94" y2="200.66" width="0.1524" layer="91"/>
<wire x1="27.94" y1="205.74" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<junction x="27.94" y="205.74"/>
<junction x="27.94" y="203.2"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="1"/>
<wire x1="175.26" y1="139.7" x2="149.86" y2="139.7" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="SV5" gate="G$1" pin="1"/>
<pinref part="SV5" gate="G$1" pin="2"/>
<wire x1="226.06" y1="116.84" x2="226.06" y2="114.3" width="0.1524" layer="91"/>
<pinref part="SV5" gate="G$1" pin="3"/>
<wire x1="226.06" y1="114.3" x2="226.06" y2="111.76" width="0.1524" layer="91"/>
<junction x="226.06" y="114.3"/>
<wire x1="226.06" y1="111.76" x2="223.52" y2="111.76" width="0.1524" layer="91"/>
<junction x="226.06" y="111.76"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B82"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="7.62" y1="5.08" x2="-17.78" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="5.08" x2="-17.78" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="P1"/>
<pinref part="EDCONN2" gate="G$1" pin="P3"/>
<pinref part="EDCONN2" gate="G$1" pin="P2"/>
<wire x1="7.62" y1="208.28" x2="7.62" y2="210.82" width="0.1524" layer="91"/>
<wire x1="7.62" y1="210.82" x2="7.62" y2="213.36" width="0.1524" layer="91"/>
<junction x="7.62" y="210.82"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="7.62" y1="213.36" x2="5.08" y2="213.36" width="0.1524" layer="91"/>
<wire x1="5.08" y1="213.36" x2="5.08" y2="215.9" width="0.1524" layer="91"/>
<junction x="7.62" y="213.36"/>
<pinref part="EDCONN2" gate="G$1" pin="P4"/>
<wire x1="7.62" y1="205.74" x2="7.62" y2="208.28" width="0.1524" layer="91"/>
<junction x="7.62" y="208.28"/>
</segment>
<segment>
<pinref part="JST3" gate="G$1" pin="2"/>
<wire x1="121.92" y1="149.86" x2="101.6" y2="149.86" width="0.1524" layer="91"/>
<pinref part="P+3" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="175.26" y1="187.96" x2="152.4" y2="187.96" width="0.1524" layer="91"/>
<pinref part="JST7" gate="G$1" pin="2"/>
<pinref part="P+5" gate="1" pin="+5V"/>
</segment>
<segment>
<wire x1="175.26" y1="167.64" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<pinref part="JST8" gate="G$1" pin="2"/>
<pinref part="P+6" gate="1" pin="+5V"/>
</segment>
<segment>
<pinref part="SV3" gate="G$1" pin="1"/>
<wire x1="226.06" y1="160.02" x2="223.52" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SV3" gate="G$1" pin="2"/>
<wire x1="226.06" y1="157.48" x2="226.06" y2="160.02" width="0.1524" layer="91"/>
<junction x="226.06" y="160.02"/>
<pinref part="SV3" gate="G$1" pin="3"/>
<wire x1="226.06" y1="154.94" x2="226.06" y2="157.48" width="0.1524" layer="91"/>
<junction x="226.06" y="157.48"/>
<pinref part="P+7" gate="1" pin="+5V"/>
</segment>
</net>
<net name="JET_KILL" class="0">
<segment>
<wire x1="121.92" y1="172.72" x2="119.38" y2="172.72" width="0.1524" layer="91"/>
<label x="119.38" y="172.72" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST2" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B26"/>
<wire x1="7.62" y1="147.32" x2="5.08" y2="147.32" width="0.1524" layer="91"/>
<label x="5.08" y="147.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="TEMP_SENS" class="0">
<segment>
<wire x1="175.26" y1="190.5" x2="172.72" y2="190.5" width="0.1524" layer="91"/>
<label x="172.72" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST7" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B29"/>
<wire x1="7.62" y1="139.7" x2="5.08" y2="139.7" width="0.1524" layer="91"/>
<label x="5.08" y="139.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PRESS_SENS" class="0">
<segment>
<wire x1="175.26" y1="170.18" x2="172.72" y2="170.18" width="0.1524" layer="91"/>
<label x="172.72" y="170.18" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST8" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B80"/>
<wire x1="7.62" y1="10.16" x2="5.08" y2="10.16" width="0.1524" layer="91"/>
<label x="5.08" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANLG6" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B74"/>
<wire x1="7.62" y1="25.4" x2="5.08" y2="25.4" width="0.1524" layer="91"/>
<label x="5.08" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="6"/>
<wire x1="175.26" y1="99.06" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<label x="172.72" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANLG5" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B75"/>
<wire x1="7.62" y1="22.86" x2="5.08" y2="22.86" width="0.1524" layer="91"/>
<label x="5.08" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="5"/>
<wire x1="175.26" y1="101.6" x2="172.72" y2="101.6" width="0.1524" layer="91"/>
<label x="172.72" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="JET_TX" class="0">
<segment>
<wire x1="121.92" y1="134.62" x2="119.38" y2="134.62" width="0.1524" layer="91"/>
<label x="119.38" y="134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST4" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B27"/>
<wire x1="7.62" y1="144.78" x2="5.08" y2="144.78" width="0.1524" layer="91"/>
<label x="5.08" y="144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="JET_RX" class="0">
<segment>
<wire x1="121.92" y1="132.08" x2="119.38" y2="132.08" width="0.1524" layer="91"/>
<label x="119.38" y="132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST4" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B28"/>
<wire x1="7.62" y1="142.24" x2="5.08" y2="142.24" width="0.1524" layer="91"/>
<label x="5.08" y="142.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B14"/>
<wire x1="7.62" y1="177.8" x2="5.08" y2="177.8" width="0.1524" layer="91"/>
<label x="5.08" y="177.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<label x="119.38" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="119.38" y1="91.44" x2="121.92" y2="91.44" width="0.1524" layer="91"/>
<pinref part="JST6" gate="G$1" pin="6"/>
</segment>
</net>
<net name="ANLG4" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B76"/>
<wire x1="7.62" y1="20.32" x2="5.08" y2="20.32" width="0.1524" layer="91"/>
<label x="5.08" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="4"/>
<wire x1="175.26" y1="104.14" x2="172.72" y2="104.14" width="0.1524" layer="91"/>
<label x="172.72" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOTOR_TLM" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B23"/>
<wire x1="7.62" y1="154.94" x2="5.08" y2="154.94" width="0.1524" layer="91"/>
<label x="5.08" y="154.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="190.5" x2="223.52" y2="190.5" width="0.1524" layer="91"/>
<label x="223.52" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="MOTOR_SIG" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B22"/>
<wire x1="7.62" y1="157.48" x2="5.08" y2="157.48" width="0.1524" layer="91"/>
<label x="5.08" y="157.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="193.04" x2="223.52" y2="193.04" width="0.1524" layer="91"/>
<label x="223.52" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST10" gate="G$1" pin="4"/>
</segment>
</net>
<net name="IMU_RST" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B18"/>
<wire x1="7.62" y1="167.64" x2="5.08" y2="167.64" width="0.1524" layer="91"/>
<label x="5.08" y="167.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST6" gate="G$1" pin="4"/>
<wire x1="121.92" y1="86.36" x2="119.38" y2="86.36" width="0.1524" layer="91"/>
<label x="119.38" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IMU_WAKE" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B19"/>
<wire x1="7.62" y1="165.1" x2="5.08" y2="165.1" width="0.1524" layer="91"/>
<label x="5.08" y="165.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST6" gate="G$1" pin="3"/>
<wire x1="121.92" y1="83.82" x2="119.38" y2="83.82" width="0.1524" layer="91"/>
<label x="119.38" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PROG_SLCT" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B37"/>
<wire x1="7.62" y1="119.38" x2="5.08" y2="119.38" width="0.1524" layer="91"/>
<label x="5.08" y="119.38" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="6"/>
<wire x1="175.26" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<label x="172.72" y="152.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PROG_IND_R" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B38"/>
<wire x1="7.62" y1="116.84" x2="5.08" y2="116.84" width="0.1524" layer="91"/>
<label x="5.08" y="116.84" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="5"/>
<wire x1="175.26" y1="149.86" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
<label x="172.72" y="149.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PROG_IND_G" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B39"/>
<wire x1="7.62" y1="114.3" x2="5.08" y2="114.3" width="0.1524" layer="91"/>
<label x="5.08" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="4"/>
<wire x1="175.26" y1="147.32" x2="172.72" y2="147.32" width="0.1524" layer="91"/>
<label x="172.72" y="147.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PROG_IND_B" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B40"/>
<wire x1="7.62" y1="111.76" x2="5.08" y2="111.76" width="0.1524" layer="91"/>
<label x="5.08" y="111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="3"/>
<wire x1="175.26" y1="144.78" x2="172.72" y2="144.78" width="0.1524" layer="91"/>
<label x="172.72" y="144.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANLG1" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B79"/>
<wire x1="7.62" y1="12.7" x2="5.08" y2="12.7" width="0.1524" layer="91"/>
<label x="5.08" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="1"/>
<wire x1="175.26" y1="111.76" x2="172.72" y2="111.76" width="0.1524" layer="91"/>
<label x="172.72" y="111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANLG2" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B78"/>
<wire x1="7.62" y1="15.24" x2="5.08" y2="15.24" width="0.1524" layer="91"/>
<label x="5.08" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="2"/>
<wire x1="175.26" y1="109.22" x2="172.72" y2="109.22" width="0.1524" layer="91"/>
<label x="172.72" y="109.22" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ANLG3" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B77"/>
<wire x1="7.62" y1="17.78" x2="5.08" y2="17.78" width="0.1524" layer="91"/>
<label x="5.08" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV2" gate="1" pin="3"/>
<wire x1="175.26" y1="106.68" x2="172.72" y2="106.68" width="0.1524" layer="91"/>
<label x="172.72" y="106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="JET_PWR" class="0">
<segment>
<wire x1="121.92" y1="175.26" x2="119.38" y2="175.26" width="0.1524" layer="91"/>
<label x="119.38" y="175.26" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="JST2" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="JST1" gate="G$1" pin="1"/>
<wire x1="121.92" y1="187.96" x2="119.38" y2="187.96" width="0.1524" layer="91"/>
<label x="119.38" y="187.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="JET_RX2" class="0">
<segment>
<pinref part="JST4" gate="G$1" pin="2"/>
<wire x1="121.92" y1="129.54" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<label x="119.38" y="129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST5" gate="G$1" pin="2"/>
<wire x1="121.92" y1="114.3" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<label x="119.38" y="114.3" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="JET_TX2" class="0">
<segment>
<pinref part="JST4" gate="G$1" pin="1"/>
<wire x1="121.92" y1="127" x2="119.38" y2="127" width="0.1524" layer="91"/>
<label x="119.38" y="127" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST5" gate="G$1" pin="1"/>
<wire x1="121.92" y1="111.76" x2="119.38" y2="111.76" width="0.1524" layer="91"/>
<label x="119.38" y="111.76" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOT_BAT_PWR" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="P8"/>
<wire x1="7.62" y1="195.58" x2="5.08" y2="195.58" width="0.1524" layer="91"/>
<label x="5.08" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="187.96" x2="223.52" y2="187.96" width="0.1524" layer="91"/>
<pinref part="JST10" gate="G$1" pin="2"/>
<label x="223.52" y="187.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="MOT_BAT_GND" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="G8"/>
<wire x1="27.94" y1="195.58" x2="30.48" y2="195.58" width="0.1524" layer="91"/>
<label x="30.48" y="195.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="226.06" y1="185.42" x2="223.52" y2="185.42" width="0.1524" layer="91"/>
<pinref part="JST10" gate="G$1" pin="1"/>
<label x="223.52" y="185.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="P5"/>
<wire x1="7.62" y1="203.2" x2="2.54" y2="203.2" width="0.1524" layer="91"/>
<pinref part="SUPPLY1" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="JST6" gate="G$1" pin="2"/>
<wire x1="121.92" y1="81.28" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
<pinref part="SUPPLY2" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<pinref part="SV4" gate="G$1" pin="3"/>
<pinref part="SV4" gate="G$1" pin="2"/>
<wire x1="226.06" y1="134.62" x2="226.06" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SV4" gate="G$1" pin="1"/>
<wire x1="226.06" y1="137.16" x2="226.06" y2="139.7" width="0.1524" layer="91"/>
<junction x="226.06" y="137.16"/>
<wire x1="226.06" y1="139.7" x2="223.52" y2="139.7" width="0.1524" layer="91"/>
<junction x="226.06" y="139.7"/>
<pinref part="SUPPLY3" gate="G$1" pin="3.3V"/>
</segment>
</net>
<net name="DIG1" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B30"/>
<wire x1="7.62" y1="137.16" x2="5.08" y2="137.16" width="0.1524" layer="91"/>
<label x="5.08" y="137.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="1"/>
<wire x1="190.5" y1="121.92" x2="193.04" y2="121.92" width="0.1524" layer="91"/>
<label x="193.04" y="121.92" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DIG2" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B31"/>
<wire x1="7.62" y1="134.62" x2="5.08" y2="134.62" width="0.1524" layer="91"/>
<label x="5.08" y="134.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="3"/>
<wire x1="190.5" y1="124.46" x2="193.04" y2="124.46" width="0.1524" layer="91"/>
<label x="193.04" y="124.46" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DIG3" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B32"/>
<wire x1="7.62" y1="132.08" x2="5.08" y2="132.08" width="0.1524" layer="91"/>
<label x="5.08" y="132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="5"/>
<wire x1="190.5" y1="127" x2="193.04" y2="127" width="0.1524" layer="91"/>
<label x="193.04" y="127" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DIG4" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B33"/>
<wire x1="7.62" y1="129.54" x2="5.08" y2="129.54" width="0.1524" layer="91"/>
<label x="5.08" y="129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="7"/>
<wire x1="190.5" y1="129.54" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<label x="193.04" y="129.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DIG5" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B34"/>
<wire x1="7.62" y1="127" x2="5.08" y2="127" width="0.1524" layer="91"/>
<label x="5.08" y="127" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="9"/>
<wire x1="190.5" y1="132.08" x2="193.04" y2="132.08" width="0.1524" layer="91"/>
<label x="193.04" y="132.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DIG6" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B35"/>
<wire x1="7.62" y1="124.46" x2="5.08" y2="124.46" width="0.1524" layer="91"/>
<label x="5.08" y="124.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="10"/>
<wire x1="175.26" y1="132.08" x2="172.72" y2="132.08" width="0.1524" layer="91"/>
<label x="172.72" y="132.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DIG7" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="B36"/>
<wire x1="7.62" y1="121.92" x2="5.08" y2="121.92" width="0.1524" layer="91"/>
<label x="5.08" y="121.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="SV1" gate="G$1" pin="8"/>
<wire x1="175.26" y1="129.54" x2="172.72" y2="129.54" width="0.1524" layer="91"/>
<label x="172.72" y="129.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IMU_BOOT" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="2"/>
<wire x1="175.26" y1="121.92" x2="172.72" y2="121.92" width="0.1524" layer="91"/>
<label x="172.72" y="121.92" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST11" gate="G$1" pin="3"/>
<wire x1="175.26" y1="86.36" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<label x="172.72" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IMU_INTRPT" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="6"/>
<wire x1="175.26" y1="127" x2="172.72" y2="127" width="0.1524" layer="91"/>
<label x="172.72" y="127" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST11" gate="G$1" pin="1"/>
<wire x1="175.26" y1="81.28" x2="172.72" y2="81.28" width="0.1524" layer="91"/>
<label x="172.72" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="IMU_RESET" class="0">
<segment>
<pinref part="SV1" gate="G$1" pin="4"/>
<wire x1="175.26" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<label x="172.72" y="124.46" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="JST11" gate="G$1" pin="2"/>
<wire x1="175.26" y1="83.82" x2="172.72" y2="83.82" width="0.1524" layer="91"/>
<label x="172.72" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="24V" class="0">
<segment>
<pinref part="EDCONN2" gate="G$1" pin="P6"/>
<wire x1="7.62" y1="200.66" x2="-2.54" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SUPPLY4" gate="G$1" pin="12V"/>
</segment>
<segment>
<pinref part="JST9" gate="G$1" pin="2"/>
<wire x1="175.26" y1="142.24" x2="149.86" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SUPPLY5" gate="G$1" pin="12V"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="101,1,121.92,190.5,JST1,2,,,,"/>
<approved hash="104,1,7.62,213.36,EDCONN2,P1,+5V,,,"/>
<approved hash="104,1,7.62,210.82,EDCONN2,P2,+5V,,,"/>
<approved hash="104,1,27.94,213.36,EDCONN2,G1,GND,,,"/>
<approved hash="104,1,27.94,210.82,EDCONN2,G2,GND,,,"/>
<approved hash="104,1,7.62,208.28,EDCONN2,P3,+5V,,,"/>
<approved hash="104,1,7.62,205.74,EDCONN2,P4,+5V,,,"/>
<approved hash="104,1,27.94,208.28,EDCONN2,G3,GND,,,"/>
<approved hash="104,1,27.94,205.74,EDCONN2,G4,GND,,,"/>
<approved hash="104,1,7.62,203.2,EDCONN2,P5,MOT_BAT_PWR,,,"/>
<approved hash="104,1,7.62,200.66,EDCONN2,P6,MOT_BAT_PWR,,,"/>
<approved hash="104,1,27.94,203.2,EDCONN2,G5,MOT_BAT_GND,,,"/>
<approved hash="104,1,27.94,200.66,EDCONN2,G6,MOT_BAT_GND,,,"/>
<approved hash="104,1,7.62,198.12,EDCONN2,P7,3.3V,,,"/>
<approved hash="104,1,7.62,195.58,EDCONN2,P8,3.3V,,,"/>
<approved hash="104,1,27.94,198.12,EDCONN2,G7,GND,,,"/>
<approved hash="104,1,27.94,195.58,EDCONN2,G8,GND,,,"/>
<approved hash="204,1,7.62,193.04,EDCONN2,P9,,,,"/>
<approved hash="204,1,27.94,193.04,EDCONN2,G9,,,,"/>
<approved hash="204,1,7.62,190.5,EDCONN2,P10,,,,"/>
<approved hash="204,1,7.62,187.96,EDCONN2,P11,,,,"/>
<approved hash="204,1,27.94,190.5,EDCONN2,G10,,,,"/>
<approved hash="204,1,27.94,187.96,EDCONN2,G11,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
