<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.3">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Release" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Heatsink" color="1" fill="7" visible="no" active="no"/>
<layer number="102" name="tMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="103" name="bMarkings" color="1" fill="7" visible="no" active="no"/>
<layer number="104" name="tSchirm" color="7" fill="4" visible="no" active="no"/>
<layer number="105" name="bSchirm" color="7" fill="5" visible="no" active="no"/>
<layer number="106" name="shield" color="11" fill="1" visible="no" active="no"/>
<layer number="107" name="ShieldTop" color="12" fill="1" visible="no" active="no"/>
<layer number="108" name="MeasuresWall" color="7" fill="1" visible="no" active="no"/>
<layer number="109" name="SYSONDER" color="7" fill="1" visible="no" active="no"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="tStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="112" name="bStopDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="bTPoint" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Änderung" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="Component" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="PCB" color="7" fill="1" visible="yes" active="yes"/>
<layer number="119" name="Layout_Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="lotbs" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="lotls" color="7" fill="1" visible="no" active="no"/>
<layer number="122" name="bTestdril" color="7" fill="1" visible="no" active="no"/>
<layer number="123" name="BOHRFILM" color="7" fill="1" visible="no" active="no"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="no"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="127" name="Mods" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="silk_top" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="silk_bottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="silktop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="silkbottom" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="DrillLegend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="145" name="DrillLegend_01-16" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="DrillLegend_01-20" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="Document_mirrored" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="References" color="4" fill="1" visible="no" active="no"/>
<layer number="151" name="FaceHelp" color="5" fill="1" visible="no" active="no"/>
<layer number="152" name="FaceFrame" color="6" fill="1" visible="no" active="no"/>
<layer number="153" name="FacePlate" color="2" fill="1" visible="no" active="no"/>
<layer number="154" name="FaceLabel" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="156" name="gesam-Maß" color="1" fill="7" visible="no" active="no"/>
<layer number="157" name="FaceMchng" color="3" fill="1" visible="no" active="no"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="Topologie" color="9" fill="1" visible="no" active="no"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Bauteile" color="7" fill="1" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="bBauteile" color="1" fill="7" visible="no" active="no"/>
<layer number="202" name="value" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="no" color="7" fill="1" visible="no" active="no"/>
<layer number="204" name="204bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="205" name="205bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="206" name="206bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="207" name="207bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="208" name="208bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="210" name="tPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="211" name="bPNR" color="7" fill="1" visible="no" active="no"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="220" name="ausheb_u" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="ausheb_o" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="frontpla" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="248" name="PLCDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="249" name="MillDoku" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="13" fill="1" visible="no" active="no"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="AggieMarineRobotics">
<packages>
<package name="AMPHENOL_CNN_164PCI">
<hole x="0" y="0" drill="1.2"/>
<hole x="73.15" y="0" drill="1.2"/>
<smd name="A1" x="-11.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A2" x="-10.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A3" x="-9.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A4" x="-8.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A5" x="-7.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A6" x="-6.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A7" x="-5.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A8" x="-4.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A9" x="-3.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A10" x="-2.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A11" x="-1.65" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B1" x="-11.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B2" x="-10.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B3" x="-9.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B4" x="-8.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B5" x="-7.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B6" x="-6.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B7" x="-5.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B8" x="-4.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B9" x="-3.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B10" x="-2.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B11" x="-1.65" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A12" x="1.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A13" x="2.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A14" x="3.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A15" x="4.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A16" x="5.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A17" x="6.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A18" x="7.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A19" x="8.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A20" x="9.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A21" x="10.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A22" x="11.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B12" x="1.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B13" x="2.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B14" x="3.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B15" x="4.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B16" x="5.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B17" x="6.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B18" x="7.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B19" x="8.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B20" x="9.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B21" x="10.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B22" x="11.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A23" x="12.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A24" x="13.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A25" x="14.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A26" x="15.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A27" x="16.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A28" x="17.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A29" x="18.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A30" x="19.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A31" x="20.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A32" x="21.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A33" x="22.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B23" x="12.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B24" x="13.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B25" x="14.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B26" x="15.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B27" x="16.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B28" x="17.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B29" x="18.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B30" x="19.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B31" x="20.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B32" x="21.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B33" x="22.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A34" x="23.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A35" x="24.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A36" x="25.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A37" x="26.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A38" x="27.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A39" x="28.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A40" x="29.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A41" x="30.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A42" x="31.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A43" x="32.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A44" x="33.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B34" x="23.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B35" x="24.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B36" x="25.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B37" x="26.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B38" x="27.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B39" x="28.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B40" x="29.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B41" x="30.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B42" x="31.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B43" x="32.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B44" x="33.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A45" x="34.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A46" x="35.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A47" x="36.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A48" x="37.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A49" x="38.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A50" x="39.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A51" x="40.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A52" x="41.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A53" x="42.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A54" x="43.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A55" x="44.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B45" x="34.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B46" x="35.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B47" x="36.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B48" x="37.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B49" x="38.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B50" x="39.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B51" x="40.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B52" x="41.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B53" x="42.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B54" x="43.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B55" x="44.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A56" x="45.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A57" x="46.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A58" x="47.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A59" x="48.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A60" x="49.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A61" x="50.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A62" x="51.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A63" x="52.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A64" x="53.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A65" x="54.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A66" x="55.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B56" x="45.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B57" x="46.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B58" x="47.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B59" x="48.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B60" x="49.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B61" x="50.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B62" x="51.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B63" x="52.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B64" x="53.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B65" x="54.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B66" x="55.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A67" x="56.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A68" x="57.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A69" x="58.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A70" x="59.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A71" x="60.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A72" x="61.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A73" x="62.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A74" x="63.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A75" x="64.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A76" x="65.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A77" x="66.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B67" x="56.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B68" x="57.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B69" x="58.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B70" x="59.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B71" x="60.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B72" x="61.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B73" x="62.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B74" x="63.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B75" x="64.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B76" x="65.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B77" x="66.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A78" x="67.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A79" x="68.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A80" x="69.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A81" x="70.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="A82" x="71.35" y="3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B78" x="67.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B79" x="68.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B80" x="69.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B81" x="70.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="B82" x="71.35" y="-3.6" dx="2" dy="0.7" layer="1" rot="R90"/>
<smd name="P$1" x="-14.65" y="0" dx="5.2" dy="3" layer="1" rot="R90"/>
<smd name="P$2" x="74.35" y="0" dx="5.2" dy="3" layer="1" rot="R90"/>
<text x="-11.43" y="6.35" size="1.27" layer="21" rot="R180" align="center-right">&gt;VALUE</text>
<text x="-13.97" y="-3.81" size="1.27" layer="21" rot="R90" align="bottom-right">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="164PIN_EDGE">
<pin name="P1" x="-10.16" y="27.94" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P2" x="-10.16" y="25.4" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G1" x="10.16" y="27.94" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G2" x="10.16" y="25.4" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="A12" x="10.16" y="-2.54" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B12" x="-10.16" y="-2.54" visible="pin" length="short" swaplevel="1"/>
<pin name="P3" x="-10.16" y="22.86" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P4" x="-10.16" y="20.32" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G3" x="10.16" y="22.86" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G4" x="10.16" y="20.32" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P5" x="-10.16" y="17.78" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P6" x="-10.16" y="15.24" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G5" x="10.16" y="17.78" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G6" x="10.16" y="15.24" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P7" x="-10.16" y="12.7" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P8" x="-10.16" y="10.16" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G7" x="10.16" y="12.7" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G8" x="10.16" y="10.16" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P9" x="-10.16" y="7.62" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G9" x="10.16" y="7.62" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="P10" x="-10.16" y="5.08" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="P11" x="-10.16" y="2.54" visible="pin" length="short" direction="pwr" swaplevel="1"/>
<pin name="G10" x="10.16" y="5.08" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="G11" x="10.16" y="2.54" visible="pin" length="short" direction="pwr" swaplevel="1" rot="R180"/>
<pin name="A13" x="10.16" y="-5.08" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B13" x="-10.16" y="-5.08" visible="pin" length="short" swaplevel="1"/>
<pin name="A14" x="10.16" y="-7.62" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B14" x="-10.16" y="-7.62" visible="pin" length="short" swaplevel="1"/>
<pin name="A15" x="10.16" y="-10.16" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B15" x="-10.16" y="-10.16" visible="pin" length="short" swaplevel="1"/>
<pin name="A16" x="10.16" y="-12.7" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B16" x="-10.16" y="-12.7" visible="pin" length="short" swaplevel="1"/>
<pin name="A17" x="10.16" y="-15.24" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B17" x="-10.16" y="-15.24" visible="pin" length="short" swaplevel="1"/>
<pin name="A18" x="10.16" y="-17.78" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B18" x="-10.16" y="-17.78" visible="pin" length="short" swaplevel="1"/>
<pin name="A19" x="10.16" y="-20.32" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B19" x="-10.16" y="-20.32" visible="pin" length="short" swaplevel="1"/>
<pin name="A20" x="10.16" y="-22.86" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B20" x="-10.16" y="-22.86" visible="pin" length="short" swaplevel="1"/>
<pin name="A21" x="10.16" y="-25.4" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B21" x="-10.16" y="-25.4" visible="pin" length="short" swaplevel="1"/>
<pin name="A22" x="10.16" y="-27.94" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B22" x="-10.16" y="-27.94" visible="pin" length="short" swaplevel="1"/>
<pin name="A23" x="10.16" y="-30.48" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B23" x="-10.16" y="-30.48" visible="pin" length="short" swaplevel="1"/>
<pin name="A24" x="10.16" y="-33.02" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B24" x="-10.16" y="-33.02" visible="pin" length="short" swaplevel="1"/>
<pin name="A25" x="10.16" y="-35.56" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B25" x="-10.16" y="-35.56" visible="pin" length="short" swaplevel="1"/>
<pin name="A26" x="10.16" y="-38.1" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B26" x="-10.16" y="-38.1" visible="pin" length="short" swaplevel="1"/>
<pin name="A27" x="10.16" y="-40.64" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B27" x="-10.16" y="-40.64" visible="pin" length="short" swaplevel="1"/>
<pin name="A28" x="10.16" y="-43.18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B28" x="-10.16" y="-43.18" visible="pin" length="short" swaplevel="1"/>
<pin name="A29" x="10.16" y="-45.72" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B29" x="-10.16" y="-45.72" visible="pin" length="short" swaplevel="1"/>
<pin name="A30" x="10.16" y="-48.26" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B30" x="-10.16" y="-48.26" visible="pin" length="short" swaplevel="1"/>
<pin name="A31" x="10.16" y="-50.8" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B31" x="-10.16" y="-50.8" visible="pin" length="short" swaplevel="1"/>
<pin name="A32" x="10.16" y="-53.34" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B32" x="-10.16" y="-53.34" visible="pin" length="short" swaplevel="1"/>
<pin name="A33" x="10.16" y="-55.88" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B33" x="-10.16" y="-55.88" visible="pin" length="short" swaplevel="1"/>
<pin name="A34" x="10.16" y="-58.42" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B34" x="-10.16" y="-58.42" visible="pin" length="short" swaplevel="1"/>
<pin name="A35" x="10.16" y="-60.96" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B35" x="-10.16" y="-60.96" visible="pin" length="short" swaplevel="1"/>
<pin name="A36" x="10.16" y="-63.5" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B36" x="-10.16" y="-63.5" visible="pin" length="short" swaplevel="1"/>
<pin name="A37" x="10.16" y="-66.04" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B37" x="-10.16" y="-66.04" visible="pin" length="short" swaplevel="1"/>
<pin name="A38" x="10.16" y="-68.58" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B38" x="-10.16" y="-68.58" visible="pin" length="short" swaplevel="1"/>
<pin name="A39" x="10.16" y="-71.12" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B39" x="-10.16" y="-71.12" visible="pin" length="short" swaplevel="1"/>
<pin name="A40" x="10.16" y="-73.66" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B40" x="-10.16" y="-73.66" visible="pin" length="short" swaplevel="1"/>
<pin name="A41" x="10.16" y="-76.2" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B41" x="-10.16" y="-76.2" visible="pin" length="short" swaplevel="1"/>
<pin name="A42" x="10.16" y="-78.74" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B42" x="-10.16" y="-78.74" visible="pin" length="short" swaplevel="1"/>
<pin name="A43" x="10.16" y="-81.28" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B43" x="-10.16" y="-81.28" visible="pin" length="short" swaplevel="1"/>
<pin name="A44" x="10.16" y="-83.82" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B44" x="-10.16" y="-83.82" visible="pin" length="short" swaplevel="1"/>
<pin name="A45" x="10.16" y="-86.36" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B45" x="-10.16" y="-86.36" visible="pin" length="short" swaplevel="1"/>
<pin name="A46" x="10.16" y="-88.9" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B46" x="-10.16" y="-88.9" visible="pin" length="short" swaplevel="1"/>
<pin name="A47" x="10.16" y="-91.44" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B47" x="-10.16" y="-91.44" visible="pin" length="short" swaplevel="1"/>
<pin name="A48" x="10.16" y="-93.98" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B48" x="-10.16" y="-93.98" visible="pin" length="short" swaplevel="1"/>
<pin name="A49" x="10.16" y="-96.52" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B49" x="-10.16" y="-96.52" visible="pin" length="short" swaplevel="1"/>
<pin name="A50" x="10.16" y="-99.06" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B50" x="-10.16" y="-99.06" visible="pin" length="short" swaplevel="1"/>
<pin name="A51" x="10.16" y="-101.6" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B51" x="-10.16" y="-101.6" visible="pin" length="short" swaplevel="1"/>
<pin name="A52" x="10.16" y="-104.14" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B52" x="-10.16" y="-104.14" visible="pin" length="short" swaplevel="1"/>
<pin name="A53" x="10.16" y="-106.68" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B53" x="-10.16" y="-106.68" visible="pin" length="short" swaplevel="1"/>
<pin name="A54" x="10.16" y="-109.22" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B54" x="-10.16" y="-109.22" visible="pin" length="short" swaplevel="1"/>
<pin name="A55" x="10.16" y="-111.76" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B55" x="-10.16" y="-111.76" visible="pin" length="short" swaplevel="1"/>
<pin name="A56" x="10.16" y="-114.3" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B56" x="-10.16" y="-114.3" visible="pin" length="short" swaplevel="1"/>
<pin name="A57" x="10.16" y="-116.84" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B57" x="-10.16" y="-116.84" visible="pin" length="short" swaplevel="1"/>
<pin name="A58" x="10.16" y="-119.38" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B58" x="-10.16" y="-119.38" visible="pin" length="short" swaplevel="1"/>
<pin name="A59" x="10.16" y="-121.92" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B59" x="-10.16" y="-121.92" visible="pin" length="short" swaplevel="1"/>
<pin name="A60" x="10.16" y="-124.46" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B60" x="-10.16" y="-124.46" visible="pin" length="short" swaplevel="1"/>
<pin name="A61" x="10.16" y="-127" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B61" x="-10.16" y="-127" visible="pin" length="short" swaplevel="1"/>
<pin name="A62" x="10.16" y="-129.54" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B62" x="-10.16" y="-129.54" visible="pin" length="short" swaplevel="1"/>
<pin name="A63" x="10.16" y="-132.08" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B63" x="-10.16" y="-132.08" visible="pin" length="short" swaplevel="1"/>
<pin name="A64" x="10.16" y="-134.62" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B64" x="-10.16" y="-134.62" visible="pin" length="short" swaplevel="1"/>
<pin name="A65" x="10.16" y="-137.16" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B65" x="-10.16" y="-137.16" visible="pin" length="short" swaplevel="1"/>
<pin name="A66" x="10.16" y="-139.7" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B66" x="-10.16" y="-139.7" visible="pin" length="short" swaplevel="1"/>
<pin name="A67" x="10.16" y="-142.24" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B67" x="-10.16" y="-142.24" visible="pin" length="short" swaplevel="1"/>
<pin name="A68" x="10.16" y="-144.78" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B68" x="-10.16" y="-144.78" visible="pin" length="short" swaplevel="1"/>
<pin name="A69" x="10.16" y="-147.32" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B69" x="-10.16" y="-147.32" visible="pin" length="short" swaplevel="1"/>
<pin name="A70" x="10.16" y="-149.86" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B70" x="-10.16" y="-149.86" visible="pin" length="short" swaplevel="1"/>
<pin name="A71" x="10.16" y="-152.4" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B71" x="-10.16" y="-152.4" visible="pin" length="short" swaplevel="1"/>
<pin name="A72" x="10.16" y="-154.94" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B72" x="-10.16" y="-154.94" visible="pin" length="short" swaplevel="1"/>
<pin name="A73" x="10.16" y="-157.48" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B73" x="-10.16" y="-157.48" visible="pin" length="short" swaplevel="1"/>
<pin name="A74" x="10.16" y="-160.02" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B74" x="-10.16" y="-160.02" visible="pin" length="short" swaplevel="1"/>
<pin name="A75" x="10.16" y="-162.56" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B75" x="-10.16" y="-162.56" visible="pin" length="short" swaplevel="1"/>
<pin name="A76" x="10.16" y="-165.1" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B76" x="-10.16" y="-165.1" visible="pin" length="short" swaplevel="1"/>
<pin name="A77" x="10.16" y="-167.64" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B77" x="-10.16" y="-167.64" visible="pin" length="short" swaplevel="1"/>
<pin name="A78" x="10.16" y="-170.18" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B78" x="-10.16" y="-170.18" visible="pin" length="short" swaplevel="1"/>
<pin name="A79" x="10.16" y="-172.72" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B79" x="-10.16" y="-172.72" visible="pin" length="short" swaplevel="1"/>
<pin name="A80" x="10.16" y="-175.26" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B80" x="-10.16" y="-175.26" visible="pin" length="short" swaplevel="1"/>
<pin name="A81" x="10.16" y="-177.8" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B81" x="-10.16" y="-177.8" visible="pin" length="short" swaplevel="1"/>
<pin name="A82" x="10.16" y="-180.34" visible="pin" length="short" swaplevel="1" rot="R180"/>
<pin name="B82" x="-10.16" y="-180.34" visible="pin" length="short" swaplevel="1"/>
<wire x1="-7.62" y1="30.48" x2="7.62" y2="30.48" width="0.254" layer="94"/>
<wire x1="7.62" y1="30.48" x2="7.62" y2="-182.88" width="0.254" layer="94"/>
<wire x1="7.62" y1="-182.88" x2="-7.62" y2="-182.88" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-182.88" x2="-7.62" y2="30.48" width="0.254" layer="94"/>
<text x="-7.62" y="33.02" size="1.778" layer="94">&gt;NAME</text>
<text x="-7.62" y="-185.42" size="1.778" layer="94">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AMPHENOL_CNN_164PCI" prefix="CNN">
<gates>
<gate name="G$1" symbol="164PIN_EDGE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="AMPHENOL_CNN_164PCI">
<connects>
<connect gate="G$1" pin="A12" pad="A12"/>
<connect gate="G$1" pin="A13" pad="A13"/>
<connect gate="G$1" pin="A14" pad="A14"/>
<connect gate="G$1" pin="A15" pad="A15"/>
<connect gate="G$1" pin="A16" pad="A16"/>
<connect gate="G$1" pin="A17" pad="A17"/>
<connect gate="G$1" pin="A18" pad="A18"/>
<connect gate="G$1" pin="A19" pad="A19"/>
<connect gate="G$1" pin="A20" pad="A20"/>
<connect gate="G$1" pin="A21" pad="A21"/>
<connect gate="G$1" pin="A22" pad="A22"/>
<connect gate="G$1" pin="A23" pad="A23"/>
<connect gate="G$1" pin="A24" pad="A24"/>
<connect gate="G$1" pin="A25" pad="A25"/>
<connect gate="G$1" pin="A26" pad="A26"/>
<connect gate="G$1" pin="A27" pad="A27"/>
<connect gate="G$1" pin="A28" pad="A28"/>
<connect gate="G$1" pin="A29" pad="A29"/>
<connect gate="G$1" pin="A30" pad="A30"/>
<connect gate="G$1" pin="A31" pad="A31"/>
<connect gate="G$1" pin="A32" pad="A32"/>
<connect gate="G$1" pin="A33" pad="A33"/>
<connect gate="G$1" pin="A34" pad="A34"/>
<connect gate="G$1" pin="A35" pad="A35"/>
<connect gate="G$1" pin="A36" pad="A36"/>
<connect gate="G$1" pin="A37" pad="A37"/>
<connect gate="G$1" pin="A38" pad="A38"/>
<connect gate="G$1" pin="A39" pad="A39"/>
<connect gate="G$1" pin="A40" pad="A40"/>
<connect gate="G$1" pin="A41" pad="A41"/>
<connect gate="G$1" pin="A42" pad="A42"/>
<connect gate="G$1" pin="A43" pad="A43"/>
<connect gate="G$1" pin="A44" pad="A44"/>
<connect gate="G$1" pin="A45" pad="A45"/>
<connect gate="G$1" pin="A46" pad="A46"/>
<connect gate="G$1" pin="A47" pad="A47"/>
<connect gate="G$1" pin="A48" pad="A48"/>
<connect gate="G$1" pin="A49" pad="A49"/>
<connect gate="G$1" pin="A50" pad="A50"/>
<connect gate="G$1" pin="A51" pad="A51"/>
<connect gate="G$1" pin="A52" pad="A52"/>
<connect gate="G$1" pin="A53" pad="A53"/>
<connect gate="G$1" pin="A54" pad="A54"/>
<connect gate="G$1" pin="A55" pad="A55"/>
<connect gate="G$1" pin="A56" pad="A56"/>
<connect gate="G$1" pin="A57" pad="A57"/>
<connect gate="G$1" pin="A58" pad="A58"/>
<connect gate="G$1" pin="A59" pad="A59"/>
<connect gate="G$1" pin="A60" pad="A60"/>
<connect gate="G$1" pin="A61" pad="A61"/>
<connect gate="G$1" pin="A62" pad="A62"/>
<connect gate="G$1" pin="A63" pad="A63"/>
<connect gate="G$1" pin="A64" pad="A64"/>
<connect gate="G$1" pin="A65" pad="A65"/>
<connect gate="G$1" pin="A66" pad="A66"/>
<connect gate="G$1" pin="A67" pad="A67"/>
<connect gate="G$1" pin="A68" pad="A68"/>
<connect gate="G$1" pin="A69" pad="A69"/>
<connect gate="G$1" pin="A70" pad="A70"/>
<connect gate="G$1" pin="A71" pad="A71"/>
<connect gate="G$1" pin="A72" pad="A72"/>
<connect gate="G$1" pin="A73" pad="A73"/>
<connect gate="G$1" pin="A74" pad="A74"/>
<connect gate="G$1" pin="A75" pad="A75"/>
<connect gate="G$1" pin="A76" pad="A76"/>
<connect gate="G$1" pin="A77" pad="A77"/>
<connect gate="G$1" pin="A78" pad="A78"/>
<connect gate="G$1" pin="A79" pad="A79"/>
<connect gate="G$1" pin="A80" pad="A80"/>
<connect gate="G$1" pin="A81" pad="A81"/>
<connect gate="G$1" pin="A82" pad="A82"/>
<connect gate="G$1" pin="B12" pad="B12"/>
<connect gate="G$1" pin="B13" pad="B13"/>
<connect gate="G$1" pin="B14" pad="B14"/>
<connect gate="G$1" pin="B15" pad="B15"/>
<connect gate="G$1" pin="B16" pad="B16"/>
<connect gate="G$1" pin="B17" pad="B17"/>
<connect gate="G$1" pin="B18" pad="B18"/>
<connect gate="G$1" pin="B19" pad="B19"/>
<connect gate="G$1" pin="B20" pad="B20"/>
<connect gate="G$1" pin="B21" pad="B21"/>
<connect gate="G$1" pin="B22" pad="B22"/>
<connect gate="G$1" pin="B23" pad="B23"/>
<connect gate="G$1" pin="B24" pad="B24"/>
<connect gate="G$1" pin="B25" pad="B25"/>
<connect gate="G$1" pin="B26" pad="B26"/>
<connect gate="G$1" pin="B27" pad="B27"/>
<connect gate="G$1" pin="B28" pad="B28"/>
<connect gate="G$1" pin="B29" pad="B29"/>
<connect gate="G$1" pin="B30" pad="B30"/>
<connect gate="G$1" pin="B31" pad="B31"/>
<connect gate="G$1" pin="B32" pad="B32"/>
<connect gate="G$1" pin="B33" pad="B33"/>
<connect gate="G$1" pin="B34" pad="B34"/>
<connect gate="G$1" pin="B35" pad="B35"/>
<connect gate="G$1" pin="B36" pad="B36"/>
<connect gate="G$1" pin="B37" pad="B37"/>
<connect gate="G$1" pin="B38" pad="B38"/>
<connect gate="G$1" pin="B39" pad="B39"/>
<connect gate="G$1" pin="B40" pad="B40"/>
<connect gate="G$1" pin="B41" pad="B41"/>
<connect gate="G$1" pin="B42" pad="B42"/>
<connect gate="G$1" pin="B43" pad="B43"/>
<connect gate="G$1" pin="B44" pad="B44"/>
<connect gate="G$1" pin="B45" pad="B45"/>
<connect gate="G$1" pin="B46" pad="B46"/>
<connect gate="G$1" pin="B47" pad="B47"/>
<connect gate="G$1" pin="B48" pad="B48"/>
<connect gate="G$1" pin="B49" pad="B49"/>
<connect gate="G$1" pin="B50" pad="B50"/>
<connect gate="G$1" pin="B51" pad="B51"/>
<connect gate="G$1" pin="B52" pad="B52"/>
<connect gate="G$1" pin="B53" pad="B53"/>
<connect gate="G$1" pin="B54" pad="B54"/>
<connect gate="G$1" pin="B55" pad="B55"/>
<connect gate="G$1" pin="B56" pad="B56"/>
<connect gate="G$1" pin="B57" pad="B57"/>
<connect gate="G$1" pin="B58" pad="B58"/>
<connect gate="G$1" pin="B59" pad="B59"/>
<connect gate="G$1" pin="B60" pad="B60"/>
<connect gate="G$1" pin="B61" pad="B61"/>
<connect gate="G$1" pin="B62" pad="B62"/>
<connect gate="G$1" pin="B63" pad="B63"/>
<connect gate="G$1" pin="B64" pad="B64"/>
<connect gate="G$1" pin="B65" pad="B65"/>
<connect gate="G$1" pin="B66" pad="B66"/>
<connect gate="G$1" pin="B67" pad="B67"/>
<connect gate="G$1" pin="B68" pad="B68"/>
<connect gate="G$1" pin="B69" pad="B69"/>
<connect gate="G$1" pin="B70" pad="B70"/>
<connect gate="G$1" pin="B71" pad="B71"/>
<connect gate="G$1" pin="B72" pad="B72"/>
<connect gate="G$1" pin="B73" pad="B73"/>
<connect gate="G$1" pin="B74" pad="B74"/>
<connect gate="G$1" pin="B75" pad="B75"/>
<connect gate="G$1" pin="B76" pad="B76"/>
<connect gate="G$1" pin="B77" pad="B77"/>
<connect gate="G$1" pin="B78" pad="B78"/>
<connect gate="G$1" pin="B79" pad="B79"/>
<connect gate="G$1" pin="B80" pad="B80"/>
<connect gate="G$1" pin="B81" pad="B81"/>
<connect gate="G$1" pin="B82" pad="B82"/>
<connect gate="G$1" pin="G1" pad="A1"/>
<connect gate="G$1" pin="G10" pad="A10"/>
<connect gate="G$1" pin="G11" pad="A11"/>
<connect gate="G$1" pin="G2" pad="A2"/>
<connect gate="G$1" pin="G3" pad="A3"/>
<connect gate="G$1" pin="G4" pad="A4"/>
<connect gate="G$1" pin="G5" pad="A5"/>
<connect gate="G$1" pin="G6" pad="A6"/>
<connect gate="G$1" pin="G7" pad="A7"/>
<connect gate="G$1" pin="G8" pad="A8"/>
<connect gate="G$1" pin="G9" pad="A9"/>
<connect gate="G$1" pin="P1" pad="B1"/>
<connect gate="G$1" pin="P10" pad="B10"/>
<connect gate="G$1" pin="P11" pad="B11"/>
<connect gate="G$1" pin="P2" pad="B2"/>
<connect gate="G$1" pin="P3" pad="B3"/>
<connect gate="G$1" pin="P4" pad="B4"/>
<connect gate="G$1" pin="P5" pad="B5"/>
<connect gate="G$1" pin="P6" pad="B6"/>
<connect gate="G$1" pin="P7" pad="B7"/>
<connect gate="G$1" pin="P8" pad="B8"/>
<connect gate="G$1" pin="P9" pad="B9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="audio amplifier example" urn="urn:adsk.eagle:library:970892">
<description>
</description>
<packages>
<package name="R1206" urn="urn:adsk.eagle:footprint:970917/1" library_version="4">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.397" y="1.143" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.413" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0402" urn="urn:adsk.eagle:footprint:970914/1" library_version="4">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.762" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0603" urn="urn:adsk.eagle:footprint:970915/1" library_version="4">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R0805" urn="urn:adsk.eagle:footprint:970916/1" library_version="4">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="21"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="21"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
<smd name="1" x="-1" y="0" dx="1.1" dy="1.4" layer="1"/>
<smd name="2" x="1" y="0" dx="1.1" dy="1.4" layer="1"/>
<text x="-0.762" y="1.016" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.762" y="-2.286" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:970937/2" type="model" library_version="4">
<description>RESISTOR
chip</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
<package3d name="R0402" urn="urn:adsk.eagle:package:970935/2" type="model" library_version="4">
<description>RESISTOR
chip</description>
<packageinstances>
<packageinstance name="R0402"/>
</packageinstances>
</package3d>
<package3d name="R0603" urn="urn:adsk.eagle:package:970938/2" type="model" library_version="4">
<description>RESISTOR
chip</description>
<packageinstances>
<packageinstance name="R0603"/>
</packageinstances>
</package3d>
<package3d name="R0805" urn="urn:adsk.eagle:package:970936/2" type="model" library_version="4">
<description>RESISTOR
chip</description>
<packageinstances>
<packageinstance name="R0805"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="R-US" urn="urn:adsk.eagle:symbol:970913/1" library_version="4">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-US_" urn="urn:adsk.eagle:component:970948/2" prefix="R" library_version="4">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:970935/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:970938/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:970936/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:970937/2"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SparkFun-LED" urn="urn:adsk.eagle:library:529">
<description>&lt;h3&gt;SparkFun LEDs&lt;/h3&gt;
This library contains discrete LEDs for illumination or indication, but no displays.
&lt;br&gt;
&lt;br&gt;
We've spent an enormous amount of time creating and checking these footprints and parts, but it is &lt;b&gt; the end user's responsibility&lt;/b&gt; to ensure correctness and suitablity for a given componet or application. 
&lt;br&gt;
&lt;br&gt;If you enjoy using this library, please buy one of our products at &lt;a href=" www.sparkfun.com"&gt;SparkFun.com&lt;/a&gt;.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Licensing:&lt;/b&gt; Creative Commons ShareAlike 4.0 International - https://creativecommons.org/licenses/by-sa/4.0/ 
&lt;br&gt;
&lt;br&gt;
You are welcome to use this library for commercial purposes. For attribution, we ask that when you begin to sell your device using our footprint, you email us with a link to the product being sold. We want bragging rights that we helped (in a very small part) to create your 8th world wonder. We would like the opportunity to feature your device on our homepage.</description>
<packages>
<package name="LED-1206" urn="urn:adsk.eagle:footprint:39304/1" library_version="1">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch: &lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="2.4" y1="0.6825" x2="2.4" y2="-0.6825" width="0.2032" layer="21"/>
<wire x1="0.65375" y1="0.6825" x2="0.65375" y2="-0.6825" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="0.47625" width="0.2032" layer="51"/>
<wire x1="0.635" y1="0" x2="0.15875" y2="-0.47625" width="0.2032" layer="51"/>
<smd name="A" x="-1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<smd name="C" x="1.5" y="0" dx="1.2" dy="1.4" layer="1"/>
<text x="0" y="0.9525" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.9525" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
<package name="LED-0603" urn="urn:adsk.eagle:footprint:39307/1" library_version="1">
<description>&lt;B&gt;LED 0603 SMT&lt;/B&gt;&lt;p&gt;
0603, surface mount.
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Pin pitch:0.075inch &lt;/li&gt;
&lt;li&gt;Area: 0.06" x 0.03"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED - BLUE&lt;/li&gt;</description>
<smd name="C" x="0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<smd name="A" x="-0.877" y="0" dx="1" dy="1" layer="1" roundness="30" rot="R270"/>
<text x="0" y="0.635" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-0.635" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="1.5875" y1="0.47625" x2="1.5875" y2="-0.47625" width="0.127" layer="21"/>
<wire x1="0.15875" y1="0.47625" x2="0.15875" y2="0" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="0.15875" y2="-0.47625" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="0.3175" width="0.127" layer="51"/>
<wire x1="0.15875" y1="0" x2="-0.15875" y2="-0.3175" width="0.127" layer="51"/>
</package>
<package name="LED-1206-BOTTOM" urn="urn:adsk.eagle:footprint:39312/1" library_version="1">
<description>&lt;h3&gt;LED 1206 SMT&lt;/h3&gt;

1206, surface mount. 

&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count: 2&lt;/li&gt;
&lt;li&gt;Area: 0.125" x 0.06"&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;LED&lt;/li&gt;&lt;/ul&gt;</description>
<wire x1="-2" y1="0.4" x2="-2" y2="-0.4" width="0.127" layer="49"/>
<wire x1="-2.4" y1="0" x2="-1.6" y2="0" width="0.127" layer="49"/>
<wire x1="1.6" y1="0" x2="2.4" y2="0" width="0.127" layer="49"/>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="0.635" x2="0.254" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="-0.381" y2="-0.635" width="0.127" layer="49"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.635" width="0.127" layer="49"/>
<wire x1="0.254" y1="0" x2="1.27" y2="0" width="0.127" layer="49"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.7686" y1="1.016" x2="2.7686" y2="-1.016" width="0.127" layer="22"/>
<rectangle x1="-0.75" y1="-0.75" x2="0.75" y2="0.75" layer="51"/>
<smd name="A" x="-1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<smd name="C" x="1.8" y="0" dx="1.5" dy="1.6" layer="1"/>
<hole x="0" y="0" drill="2.3"/>
<polygon width="0" layer="51">
<vertex x="1.1" y="-0.5"/>
<vertex x="1.1" y="0.5"/>
<vertex x="1.6" y="0.5"/>
<vertex x="1.6" y="0.25" curve="90"/>
<vertex x="1.4" y="0.05"/>
<vertex x="1.4" y="-0.05" curve="90"/>
<vertex x="1.6" y="-0.25"/>
<vertex x="1.6" y="-0.5"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="-1.1" y="0.5"/>
<vertex x="-1.1" y="-0.5"/>
<vertex x="-1.6" y="-0.5"/>
<vertex x="-1.6" y="-0.25" curve="90"/>
<vertex x="-1.4" y="-0.05"/>
<vertex x="-1.4" y="0.05" curve="90"/>
<vertex x="-1.6" y="0.25"/>
<vertex x="-1.6" y="0.5"/>
</polygon>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
</package>
</packages>
<packages3d>
<package3d name="LED-1206" urn="urn:adsk.eagle:package:39352/1" type="box" library_version="1">
<description>LED 1206 SMT

1206, surface mount. 

Specifications:
Pin count: 2
Pin pitch: 
Area: 0.125" x 0.06"

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED-1206"/>
</packageinstances>
</package3d>
<package3d name="LED-0603" urn="urn:adsk.eagle:package:39354/1" type="box" library_version="1">
<description>LED 0603 SMT
0603, surface mount.
Specifications:
Pin count: 2
Pin pitch:0.075inch 
Area: 0.06" x 0.03"

Example device(s):
LED - BLUE</description>
<packageinstances>
<packageinstance name="LED-0603"/>
</packageinstances>
</package3d>
<package3d name="LED-1206-BOTTOM" urn="urn:adsk.eagle:package:39358/1" type="box" library_version="1">
<description>LED 1206 SMT

1206, surface mount. 

Specifications:
Pin count: 2
Area: 0.125" x 0.06"

Example device(s):
LED</description>
<packageinstances>
<packageinstance name="LED-1206-BOTTOM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="LED" urn="urn:adsk.eagle:symbol:39303/1" library_version="1">
<description>&lt;h3&gt;LED&lt;/h3&gt;
&lt;p&gt;&lt;/p&gt;</description>
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="-3.429" y="-4.572" size="1.778" layer="95" font="vector" rot="R90">&gt;NAME</text>
<text x="1.905" y="-4.572" size="1.778" layer="96" font="vector" rot="R90" align="top-left">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-GREEN" urn="urn:adsk.eagle:component:39390/1" prefix="D" uservalue="yes" library_version="1">
<description>&lt;h3&gt;Green SMD LED&lt;/h3&gt;
&lt;p&gt;Used in manufacturing of various products at SparkFun&lt;/p&gt;

&lt;p&gt;&lt;b&gt;Packages:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;b&gt;0603&lt;/b&gt; - SMD 0603 package &lt;a href="http://www.futureelectronics.com/en/technologies/semiconductors/optoelectronics/leds/Pages/9894312-LTST-C190GKT.aspx?CrossPart=HSMG-C190&amp;IM=0"&gt;[Product Link]&lt;/li&gt;
&lt;li&gt;&lt;b&gt;LilyPad 1206&lt;/b&gt; - SMD1206 LilyPad package &lt;a href="http://www.futureelectronics.com/en/Technologies/Product.aspx?ProductID=IN150NGHARVATEKCORPORATION2049943&amp;IM=0"&gt;[Product Link]&lt;/li&gt;
&lt;li&gt;&lt;b&gt; 1206&lt;/b&gt; - SMD1206  package &lt;a href=http://www.futureelectronics.com/en/technologies/semiconductors/optoelectronics/leds/Pages/8315665-LTST-C150KGKT.aspx?IM=0"&gt;[Product Link]&lt;/li&gt;
&lt;li&gt;&lt;b&gt; 1206 Bottom Mount&lt;/b&gt; -SMD 1206 Bottom Mount &lt;a href=http://www.digikey.com/product-detail/en/lite-on-inc/LTST-C230KGKT/160-1456-1-ND/386854"&gt;[Product Link]&lt;/li&gt;
&lt;ul&gt;&lt;/p&gt;

&lt;p&gt;&lt;b&gt;SparkFun Products:&lt;/b&gt;
&lt;ul&gt;&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/821”&gt;Pro Mini 328 -5V&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/12757”&gt;RedBoard&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/13720”&gt;MP3 Trigger&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=”https://www.sparkfun.com/products/11076”&gt;Makey Makey&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="LED-0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39354/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-00821" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="LILYPAD" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39352/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-09910"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39352/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-00862" constant="no"/>
<attribute name="VALUE" value="GREEN" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206-BOTTOM" package="LED-1206-BOTTOM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:39358/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="DIO-11076" constant="no"/>
<attribute name="VALUE" value="Green" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="CNN2" library="AggieMarineRobotics" deviceset="AMPHENOL_CNN_164PCI" device=""/>
<part name="CNN1" library="AggieMarineRobotics" deviceset="AMPHENOL_CNN_164PCI" device=""/>
<part name="CNN4" library="AggieMarineRobotics" deviceset="AMPHENOL_CNN_164PCI" device=""/>
<part name="CNN3" library="AggieMarineRobotics" deviceset="AMPHENOL_CNN_164PCI" device=""/>
<part name="CNN5" library="AggieMarineRobotics" deviceset="AMPHENOL_CNN_164PCI" device=""/>
<part name="R1" library="audio amplifier example" library_urn="urn:adsk.eagle:library:970892" deviceset="R-US_" device="R1206" package3d_urn="urn:adsk.eagle:package:970937/2" value="143"/>
<part name="LED1" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED-GREEN" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1" value="GREEN"/>
<part name="R2" library="audio amplifier example" library_urn="urn:adsk.eagle:library:970892" deviceset="R-US_" device="R1206" package3d_urn="urn:adsk.eagle:package:970937/2" value="143"/>
<part name="LED2" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED-GREEN" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1" value="GREEN"/>
<part name="R3" library="audio amplifier example" library_urn="urn:adsk.eagle:library:970892" deviceset="R-US_" device="R1206" package3d_urn="urn:adsk.eagle:package:970937/2" value="143"/>
<part name="LED3" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED-GREEN" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1" value="GREEN"/>
<part name="R4" library="audio amplifier example" library_urn="urn:adsk.eagle:library:970892" deviceset="R-US_" device="R1206" package3d_urn="urn:adsk.eagle:package:970937/2" value="143"/>
<part name="LED4" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED-GREEN" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1" value="GREEN"/>
<part name="R6" library="audio amplifier example" library_urn="urn:adsk.eagle:library:970892" deviceset="R-US_" device="R1206" package3d_urn="urn:adsk.eagle:package:970937/2" value="143"/>
<part name="LED6" library="SparkFun-LED" library_urn="urn:adsk.eagle:library:529" deviceset="LED-GREEN" device="1206" package3d_urn="urn:adsk.eagle:package:39352/1" value="GREEN"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="CNN2" gate="G$1" x="347.98" y="71.12"/>
<instance part="CNN1" gate="G$1" x="304.8" y="71.12"/>
<instance part="CNN4" gate="G$1" x="434.34" y="71.12"/>
<instance part="CNN3" gate="G$1" x="391.16" y="71.12"/>
<instance part="CNN5" gate="G$1" x="477.52" y="71.12"/>
<instance part="R1" gate="G$1" x="332.74" y="-124.46" rot="R90"/>
<instance part="LED1" gate="G$1" x="332.74" y="-134.62"/>
<instance part="R2" gate="G$1" x="289.56" y="-124.46" rot="R90"/>
<instance part="LED2" gate="G$1" x="289.56" y="-134.62"/>
<instance part="R3" gate="G$1" x="419.1" y="-124.46" rot="R90"/>
<instance part="LED3" gate="G$1" x="419.1" y="-134.62"/>
<instance part="R4" gate="G$1" x="375.92" y="-124.46" rot="R90"/>
<instance part="LED4" gate="G$1" x="375.92" y="-134.62"/>
<instance part="R6" gate="G$1" x="462.28" y="-124.46" rot="R90"/>
<instance part="LED6" gate="G$1" x="462.28" y="-134.62"/>
</instances>
<busses>
</busses>
<nets>
<net name="P1" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P1"/>
<wire x1="337.82" y1="99.06" x2="335.28" y2="99.06" width="0.1524" layer="91"/>
<label x="335.28" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P1"/>
<wire x1="294.64" y1="99.06" x2="292.1" y2="99.06" width="0.1524" layer="91"/>
<label x="292.1" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P1"/>
<wire x1="424.18" y1="99.06" x2="421.64" y2="99.06" width="0.1524" layer="91"/>
<label x="421.64" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P1"/>
<wire x1="381" y1="99.06" x2="378.46" y2="99.06" width="0.1524" layer="91"/>
<label x="378.46" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P1"/>
<wire x1="467.36" y1="99.06" x2="464.82" y2="99.06" width="0.1524" layer="91"/>
<label x="464.82" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P2" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P2"/>
<wire x1="337.82" y1="96.52" x2="335.28" y2="96.52" width="0.1524" layer="91"/>
<label x="335.28" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P2"/>
<wire x1="294.64" y1="96.52" x2="292.1" y2="96.52" width="0.1524" layer="91"/>
<label x="292.1" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P2"/>
<wire x1="424.18" y1="96.52" x2="421.64" y2="96.52" width="0.1524" layer="91"/>
<label x="421.64" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P2"/>
<wire x1="381" y1="96.52" x2="378.46" y2="96.52" width="0.1524" layer="91"/>
<label x="378.46" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P2"/>
<wire x1="467.36" y1="96.52" x2="464.82" y2="96.52" width="0.1524" layer="91"/>
<label x="464.82" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P3" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P3"/>
<wire x1="337.82" y1="93.98" x2="335.28" y2="93.98" width="0.1524" layer="91"/>
<label x="335.28" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P3"/>
<wire x1="294.64" y1="93.98" x2="292.1" y2="93.98" width="0.1524" layer="91"/>
<label x="292.1" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P3"/>
<wire x1="424.18" y1="93.98" x2="421.64" y2="93.98" width="0.1524" layer="91"/>
<label x="421.64" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P3"/>
<wire x1="381" y1="93.98" x2="378.46" y2="93.98" width="0.1524" layer="91"/>
<label x="378.46" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P3"/>
<wire x1="467.36" y1="93.98" x2="464.82" y2="93.98" width="0.1524" layer="91"/>
<label x="464.82" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P4" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P4"/>
<wire x1="337.82" y1="91.44" x2="335.28" y2="91.44" width="0.1524" layer="91"/>
<label x="335.28" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P4"/>
<wire x1="294.64" y1="91.44" x2="292.1" y2="91.44" width="0.1524" layer="91"/>
<label x="292.1" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P4"/>
<wire x1="424.18" y1="91.44" x2="421.64" y2="91.44" width="0.1524" layer="91"/>
<label x="421.64" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P4"/>
<wire x1="381" y1="91.44" x2="378.46" y2="91.44" width="0.1524" layer="91"/>
<label x="378.46" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P4"/>
<wire x1="467.36" y1="91.44" x2="464.82" y2="91.44" width="0.1524" layer="91"/>
<label x="464.82" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P5" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P5"/>
<wire x1="337.82" y1="88.9" x2="335.28" y2="88.9" width="0.1524" layer="91"/>
<label x="335.28" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P5"/>
<wire x1="294.64" y1="88.9" x2="292.1" y2="88.9" width="0.1524" layer="91"/>
<label x="292.1" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P5"/>
<wire x1="424.18" y1="88.9" x2="421.64" y2="88.9" width="0.1524" layer="91"/>
<label x="421.64" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P5"/>
<wire x1="381" y1="88.9" x2="378.46" y2="88.9" width="0.1524" layer="91"/>
<label x="378.46" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P5"/>
<wire x1="467.36" y1="88.9" x2="464.82" y2="88.9" width="0.1524" layer="91"/>
<label x="464.82" y="88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P6" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P6"/>
<wire x1="337.82" y1="86.36" x2="335.28" y2="86.36" width="0.1524" layer="91"/>
<label x="335.28" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P6"/>
<wire x1="294.64" y1="86.36" x2="292.1" y2="86.36" width="0.1524" layer="91"/>
<label x="292.1" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P6"/>
<wire x1="424.18" y1="86.36" x2="421.64" y2="86.36" width="0.1524" layer="91"/>
<label x="421.64" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P6"/>
<wire x1="381" y1="86.36" x2="378.46" y2="86.36" width="0.1524" layer="91"/>
<label x="378.46" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P6"/>
<wire x1="467.36" y1="86.36" x2="464.82" y2="86.36" width="0.1524" layer="91"/>
<label x="464.82" y="86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P7" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P7"/>
<wire x1="337.82" y1="83.82" x2="335.28" y2="83.82" width="0.1524" layer="91"/>
<label x="335.28" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P7"/>
<wire x1="294.64" y1="83.82" x2="292.1" y2="83.82" width="0.1524" layer="91"/>
<label x="292.1" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P7"/>
<wire x1="424.18" y1="83.82" x2="421.64" y2="83.82" width="0.1524" layer="91"/>
<label x="421.64" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P7"/>
<wire x1="381" y1="83.82" x2="378.46" y2="83.82" width="0.1524" layer="91"/>
<label x="378.46" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P7"/>
<wire x1="467.36" y1="83.82" x2="464.82" y2="83.82" width="0.1524" layer="91"/>
<label x="464.82" y="83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P8" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P8"/>
<wire x1="337.82" y1="81.28" x2="335.28" y2="81.28" width="0.1524" layer="91"/>
<label x="335.28" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P8"/>
<wire x1="294.64" y1="81.28" x2="292.1" y2="81.28" width="0.1524" layer="91"/>
<label x="292.1" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P8"/>
<wire x1="424.18" y1="81.28" x2="421.64" y2="81.28" width="0.1524" layer="91"/>
<label x="421.64" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P8"/>
<wire x1="381" y1="81.28" x2="378.46" y2="81.28" width="0.1524" layer="91"/>
<label x="378.46" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P8"/>
<wire x1="467.36" y1="81.28" x2="464.82" y2="81.28" width="0.1524" layer="91"/>
<label x="464.82" y="81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P9" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P9"/>
<wire x1="337.82" y1="78.74" x2="335.28" y2="78.74" width="0.1524" layer="91"/>
<label x="335.28" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P9"/>
<wire x1="294.64" y1="78.74" x2="292.1" y2="78.74" width="0.1524" layer="91"/>
<label x="292.1" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P9"/>
<wire x1="424.18" y1="78.74" x2="421.64" y2="78.74" width="0.1524" layer="91"/>
<label x="421.64" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P9"/>
<wire x1="381" y1="78.74" x2="378.46" y2="78.74" width="0.1524" layer="91"/>
<label x="378.46" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P9"/>
<wire x1="467.36" y1="78.74" x2="464.82" y2="78.74" width="0.1524" layer="91"/>
<label x="464.82" y="78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P10" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P10"/>
<wire x1="337.82" y1="76.2" x2="335.28" y2="76.2" width="0.1524" layer="91"/>
<label x="335.28" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P10"/>
<wire x1="294.64" y1="76.2" x2="292.1" y2="76.2" width="0.1524" layer="91"/>
<label x="292.1" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P10"/>
<wire x1="424.18" y1="76.2" x2="421.64" y2="76.2" width="0.1524" layer="91"/>
<label x="421.64" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P10"/>
<wire x1="381" y1="76.2" x2="378.46" y2="76.2" width="0.1524" layer="91"/>
<label x="378.46" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P10"/>
<wire x1="467.36" y1="76.2" x2="464.82" y2="76.2" width="0.1524" layer="91"/>
<label x="464.82" y="76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="P11" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="P11"/>
<wire x1="337.82" y1="73.66" x2="335.28" y2="73.66" width="0.1524" layer="91"/>
<label x="335.28" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="P11"/>
<wire x1="294.64" y1="73.66" x2="292.1" y2="73.66" width="0.1524" layer="91"/>
<label x="292.1" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="P11"/>
<wire x1="424.18" y1="73.66" x2="421.64" y2="73.66" width="0.1524" layer="91"/>
<label x="421.64" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="P11"/>
<wire x1="381" y1="73.66" x2="378.46" y2="73.66" width="0.1524" layer="91"/>
<label x="378.46" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="P11"/>
<wire x1="467.36" y1="73.66" x2="464.82" y2="73.66" width="0.1524" layer="91"/>
<label x="464.82" y="73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B12" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B12"/>
<wire x1="337.82" y1="68.58" x2="335.28" y2="68.58" width="0.1524" layer="91"/>
<label x="335.28" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B12"/>
<wire x1="294.64" y1="68.58" x2="292.1" y2="68.58" width="0.1524" layer="91"/>
<label x="292.1" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B12"/>
<wire x1="424.18" y1="68.58" x2="421.64" y2="68.58" width="0.1524" layer="91"/>
<label x="421.64" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B12"/>
<wire x1="381" y1="68.58" x2="378.46" y2="68.58" width="0.1524" layer="91"/>
<label x="378.46" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B12"/>
<wire x1="467.36" y1="68.58" x2="464.82" y2="68.58" width="0.1524" layer="91"/>
<label x="464.82" y="68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B13" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B13"/>
<wire x1="337.82" y1="66.04" x2="335.28" y2="66.04" width="0.1524" layer="91"/>
<label x="335.28" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B13"/>
<wire x1="294.64" y1="66.04" x2="292.1" y2="66.04" width="0.1524" layer="91"/>
<label x="292.1" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B13"/>
<wire x1="424.18" y1="66.04" x2="421.64" y2="66.04" width="0.1524" layer="91"/>
<label x="421.64" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B13"/>
<wire x1="381" y1="66.04" x2="378.46" y2="66.04" width="0.1524" layer="91"/>
<label x="378.46" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B13"/>
<wire x1="467.36" y1="66.04" x2="464.82" y2="66.04" width="0.1524" layer="91"/>
<label x="464.82" y="66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B14" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B14"/>
<wire x1="337.82" y1="63.5" x2="335.28" y2="63.5" width="0.1524" layer="91"/>
<label x="335.28" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B14"/>
<wire x1="294.64" y1="63.5" x2="292.1" y2="63.5" width="0.1524" layer="91"/>
<label x="292.1" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B14"/>
<wire x1="424.18" y1="63.5" x2="421.64" y2="63.5" width="0.1524" layer="91"/>
<label x="421.64" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B14"/>
<wire x1="381" y1="63.5" x2="378.46" y2="63.5" width="0.1524" layer="91"/>
<label x="378.46" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B14"/>
<wire x1="467.36" y1="63.5" x2="464.82" y2="63.5" width="0.1524" layer="91"/>
<label x="464.82" y="63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B15" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B15"/>
<wire x1="337.82" y1="60.96" x2="335.28" y2="60.96" width="0.1524" layer="91"/>
<label x="335.28" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B15"/>
<wire x1="294.64" y1="60.96" x2="292.1" y2="60.96" width="0.1524" layer="91"/>
<label x="292.1" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B15"/>
<wire x1="424.18" y1="60.96" x2="421.64" y2="60.96" width="0.1524" layer="91"/>
<label x="421.64" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B15"/>
<wire x1="381" y1="60.96" x2="378.46" y2="60.96" width="0.1524" layer="91"/>
<label x="378.46" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B15"/>
<wire x1="467.36" y1="60.96" x2="464.82" y2="60.96" width="0.1524" layer="91"/>
<label x="464.82" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B16" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B16"/>
<wire x1="337.82" y1="58.42" x2="335.28" y2="58.42" width="0.1524" layer="91"/>
<label x="335.28" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B16"/>
<wire x1="294.64" y1="58.42" x2="292.1" y2="58.42" width="0.1524" layer="91"/>
<label x="292.1" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B16"/>
<wire x1="424.18" y1="58.42" x2="421.64" y2="58.42" width="0.1524" layer="91"/>
<label x="421.64" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B16"/>
<wire x1="381" y1="58.42" x2="378.46" y2="58.42" width="0.1524" layer="91"/>
<label x="378.46" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B16"/>
<wire x1="467.36" y1="58.42" x2="464.82" y2="58.42" width="0.1524" layer="91"/>
<label x="464.82" y="58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B17" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B17"/>
<wire x1="337.82" y1="55.88" x2="335.28" y2="55.88" width="0.1524" layer="91"/>
<label x="335.28" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B17"/>
<wire x1="294.64" y1="55.88" x2="292.1" y2="55.88" width="0.1524" layer="91"/>
<label x="292.1" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B17"/>
<wire x1="424.18" y1="55.88" x2="421.64" y2="55.88" width="0.1524" layer="91"/>
<label x="421.64" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B17"/>
<wire x1="381" y1="55.88" x2="378.46" y2="55.88" width="0.1524" layer="91"/>
<label x="378.46" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B17"/>
<wire x1="467.36" y1="55.88" x2="464.82" y2="55.88" width="0.1524" layer="91"/>
<label x="464.82" y="55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B18" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B18"/>
<wire x1="337.82" y1="53.34" x2="335.28" y2="53.34" width="0.1524" layer="91"/>
<label x="335.28" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B18"/>
<wire x1="294.64" y1="53.34" x2="292.1" y2="53.34" width="0.1524" layer="91"/>
<label x="292.1" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B18"/>
<wire x1="424.18" y1="53.34" x2="421.64" y2="53.34" width="0.1524" layer="91"/>
<label x="421.64" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B18"/>
<wire x1="381" y1="53.34" x2="378.46" y2="53.34" width="0.1524" layer="91"/>
<label x="378.46" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B18"/>
<wire x1="467.36" y1="53.34" x2="464.82" y2="53.34" width="0.1524" layer="91"/>
<label x="464.82" y="53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B19" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B19"/>
<wire x1="337.82" y1="50.8" x2="335.28" y2="50.8" width="0.1524" layer="91"/>
<label x="335.28" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B19"/>
<wire x1="294.64" y1="50.8" x2="292.1" y2="50.8" width="0.1524" layer="91"/>
<label x="292.1" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B19"/>
<wire x1="424.18" y1="50.8" x2="421.64" y2="50.8" width="0.1524" layer="91"/>
<label x="421.64" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B19"/>
<wire x1="381" y1="50.8" x2="378.46" y2="50.8" width="0.1524" layer="91"/>
<label x="378.46" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B19"/>
<wire x1="467.36" y1="50.8" x2="464.82" y2="50.8" width="0.1524" layer="91"/>
<label x="464.82" y="50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B20" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B20"/>
<wire x1="337.82" y1="48.26" x2="335.28" y2="48.26" width="0.1524" layer="91"/>
<label x="335.28" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B20"/>
<wire x1="294.64" y1="48.26" x2="292.1" y2="48.26" width="0.1524" layer="91"/>
<label x="292.1" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B20"/>
<wire x1="424.18" y1="48.26" x2="421.64" y2="48.26" width="0.1524" layer="91"/>
<label x="421.64" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B20"/>
<wire x1="381" y1="48.26" x2="378.46" y2="48.26" width="0.1524" layer="91"/>
<label x="378.46" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B20"/>
<wire x1="467.36" y1="48.26" x2="464.82" y2="48.26" width="0.1524" layer="91"/>
<label x="464.82" y="48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B21" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B21"/>
<wire x1="337.82" y1="45.72" x2="335.28" y2="45.72" width="0.1524" layer="91"/>
<label x="335.28" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B21"/>
<wire x1="294.64" y1="45.72" x2="292.1" y2="45.72" width="0.1524" layer="91"/>
<label x="292.1" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B21"/>
<wire x1="424.18" y1="45.72" x2="421.64" y2="45.72" width="0.1524" layer="91"/>
<label x="421.64" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B21"/>
<wire x1="381" y1="45.72" x2="378.46" y2="45.72" width="0.1524" layer="91"/>
<label x="378.46" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B21"/>
<wire x1="467.36" y1="45.72" x2="464.82" y2="45.72" width="0.1524" layer="91"/>
<label x="464.82" y="45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B22" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B22"/>
<wire x1="337.82" y1="43.18" x2="335.28" y2="43.18" width="0.1524" layer="91"/>
<label x="335.28" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B22"/>
<wire x1="294.64" y1="43.18" x2="292.1" y2="43.18" width="0.1524" layer="91"/>
<label x="292.1" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B22"/>
<wire x1="424.18" y1="43.18" x2="421.64" y2="43.18" width="0.1524" layer="91"/>
<label x="421.64" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B22"/>
<wire x1="381" y1="43.18" x2="378.46" y2="43.18" width="0.1524" layer="91"/>
<label x="378.46" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B22"/>
<wire x1="467.36" y1="43.18" x2="464.82" y2="43.18" width="0.1524" layer="91"/>
<label x="464.82" y="43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B23" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B23"/>
<wire x1="337.82" y1="40.64" x2="335.28" y2="40.64" width="0.1524" layer="91"/>
<label x="335.28" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B23"/>
<wire x1="294.64" y1="40.64" x2="292.1" y2="40.64" width="0.1524" layer="91"/>
<label x="292.1" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B23"/>
<wire x1="424.18" y1="40.64" x2="421.64" y2="40.64" width="0.1524" layer="91"/>
<label x="421.64" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B23"/>
<wire x1="381" y1="40.64" x2="378.46" y2="40.64" width="0.1524" layer="91"/>
<label x="378.46" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B23"/>
<wire x1="467.36" y1="40.64" x2="464.82" y2="40.64" width="0.1524" layer="91"/>
<label x="464.82" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B24" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B24"/>
<wire x1="337.82" y1="38.1" x2="335.28" y2="38.1" width="0.1524" layer="91"/>
<label x="335.28" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B24"/>
<wire x1="294.64" y1="38.1" x2="292.1" y2="38.1" width="0.1524" layer="91"/>
<label x="292.1" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B24"/>
<wire x1="424.18" y1="38.1" x2="421.64" y2="38.1" width="0.1524" layer="91"/>
<label x="421.64" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B24"/>
<wire x1="381" y1="38.1" x2="378.46" y2="38.1" width="0.1524" layer="91"/>
<label x="378.46" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B24"/>
<wire x1="467.36" y1="38.1" x2="464.82" y2="38.1" width="0.1524" layer="91"/>
<label x="464.82" y="38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B25" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B25"/>
<wire x1="337.82" y1="35.56" x2="335.28" y2="35.56" width="0.1524" layer="91"/>
<label x="335.28" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B25"/>
<wire x1="294.64" y1="35.56" x2="292.1" y2="35.56" width="0.1524" layer="91"/>
<label x="292.1" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B25"/>
<wire x1="424.18" y1="35.56" x2="421.64" y2="35.56" width="0.1524" layer="91"/>
<label x="421.64" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B25"/>
<wire x1="381" y1="35.56" x2="378.46" y2="35.56" width="0.1524" layer="91"/>
<label x="378.46" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B25"/>
<wire x1="467.36" y1="35.56" x2="464.82" y2="35.56" width="0.1524" layer="91"/>
<label x="464.82" y="35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B26" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B26"/>
<wire x1="337.82" y1="33.02" x2="335.28" y2="33.02" width="0.1524" layer="91"/>
<label x="335.28" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B26"/>
<wire x1="294.64" y1="33.02" x2="292.1" y2="33.02" width="0.1524" layer="91"/>
<label x="292.1" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B26"/>
<wire x1="424.18" y1="33.02" x2="421.64" y2="33.02" width="0.1524" layer="91"/>
<label x="421.64" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B26"/>
<wire x1="381" y1="33.02" x2="378.46" y2="33.02" width="0.1524" layer="91"/>
<label x="378.46" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B26"/>
<wire x1="467.36" y1="33.02" x2="464.82" y2="33.02" width="0.1524" layer="91"/>
<label x="464.82" y="33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B27" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B27"/>
<wire x1="337.82" y1="30.48" x2="335.28" y2="30.48" width="0.1524" layer="91"/>
<label x="335.28" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B27"/>
<wire x1="294.64" y1="30.48" x2="292.1" y2="30.48" width="0.1524" layer="91"/>
<label x="292.1" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B27"/>
<wire x1="424.18" y1="30.48" x2="421.64" y2="30.48" width="0.1524" layer="91"/>
<label x="421.64" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B27"/>
<wire x1="381" y1="30.48" x2="378.46" y2="30.48" width="0.1524" layer="91"/>
<label x="378.46" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B27"/>
<wire x1="467.36" y1="30.48" x2="464.82" y2="30.48" width="0.1524" layer="91"/>
<label x="464.82" y="30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B28" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B28"/>
<wire x1="337.82" y1="27.94" x2="335.28" y2="27.94" width="0.1524" layer="91"/>
<label x="335.28" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B28"/>
<wire x1="294.64" y1="27.94" x2="292.1" y2="27.94" width="0.1524" layer="91"/>
<label x="292.1" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B28"/>
<wire x1="424.18" y1="27.94" x2="421.64" y2="27.94" width="0.1524" layer="91"/>
<label x="421.64" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B28"/>
<wire x1="381" y1="27.94" x2="378.46" y2="27.94" width="0.1524" layer="91"/>
<label x="378.46" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B28"/>
<wire x1="467.36" y1="27.94" x2="464.82" y2="27.94" width="0.1524" layer="91"/>
<label x="464.82" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B29" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B29"/>
<wire x1="337.82" y1="25.4" x2="335.28" y2="25.4" width="0.1524" layer="91"/>
<label x="335.28" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B29"/>
<wire x1="294.64" y1="25.4" x2="292.1" y2="25.4" width="0.1524" layer="91"/>
<label x="292.1" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B29"/>
<wire x1="424.18" y1="25.4" x2="421.64" y2="25.4" width="0.1524" layer="91"/>
<label x="421.64" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B29"/>
<wire x1="381" y1="25.4" x2="378.46" y2="25.4" width="0.1524" layer="91"/>
<label x="378.46" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B29"/>
<wire x1="467.36" y1="25.4" x2="464.82" y2="25.4" width="0.1524" layer="91"/>
<label x="464.82" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B30" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B30"/>
<wire x1="337.82" y1="22.86" x2="335.28" y2="22.86" width="0.1524" layer="91"/>
<label x="335.28" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B30"/>
<wire x1="294.64" y1="22.86" x2="292.1" y2="22.86" width="0.1524" layer="91"/>
<label x="292.1" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B30"/>
<wire x1="424.18" y1="22.86" x2="421.64" y2="22.86" width="0.1524" layer="91"/>
<label x="421.64" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B30"/>
<wire x1="381" y1="22.86" x2="378.46" y2="22.86" width="0.1524" layer="91"/>
<label x="378.46" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B30"/>
<wire x1="467.36" y1="22.86" x2="464.82" y2="22.86" width="0.1524" layer="91"/>
<label x="464.82" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B31" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B31"/>
<wire x1="337.82" y1="20.32" x2="335.28" y2="20.32" width="0.1524" layer="91"/>
<label x="335.28" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B31"/>
<wire x1="294.64" y1="20.32" x2="292.1" y2="20.32" width="0.1524" layer="91"/>
<label x="292.1" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B31"/>
<wire x1="424.18" y1="20.32" x2="421.64" y2="20.32" width="0.1524" layer="91"/>
<label x="421.64" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B31"/>
<wire x1="381" y1="20.32" x2="378.46" y2="20.32" width="0.1524" layer="91"/>
<label x="378.46" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B31"/>
<wire x1="467.36" y1="20.32" x2="464.82" y2="20.32" width="0.1524" layer="91"/>
<label x="464.82" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B32" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B32"/>
<wire x1="337.82" y1="17.78" x2="335.28" y2="17.78" width="0.1524" layer="91"/>
<label x="335.28" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B32"/>
<wire x1="294.64" y1="17.78" x2="292.1" y2="17.78" width="0.1524" layer="91"/>
<label x="292.1" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B32"/>
<wire x1="424.18" y1="17.78" x2="421.64" y2="17.78" width="0.1524" layer="91"/>
<label x="421.64" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B32"/>
<wire x1="381" y1="17.78" x2="378.46" y2="17.78" width="0.1524" layer="91"/>
<label x="378.46" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B32"/>
<wire x1="467.36" y1="17.78" x2="464.82" y2="17.78" width="0.1524" layer="91"/>
<label x="464.82" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B33" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B33"/>
<wire x1="337.82" y1="15.24" x2="335.28" y2="15.24" width="0.1524" layer="91"/>
<label x="335.28" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B33"/>
<wire x1="294.64" y1="15.24" x2="292.1" y2="15.24" width="0.1524" layer="91"/>
<label x="292.1" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B33"/>
<wire x1="424.18" y1="15.24" x2="421.64" y2="15.24" width="0.1524" layer="91"/>
<label x="421.64" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B33"/>
<wire x1="381" y1="15.24" x2="378.46" y2="15.24" width="0.1524" layer="91"/>
<label x="378.46" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B33"/>
<wire x1="467.36" y1="15.24" x2="464.82" y2="15.24" width="0.1524" layer="91"/>
<label x="464.82" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B34" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B34"/>
<wire x1="337.82" y1="12.7" x2="335.28" y2="12.7" width="0.1524" layer="91"/>
<label x="335.28" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B34"/>
<wire x1="294.64" y1="12.7" x2="292.1" y2="12.7" width="0.1524" layer="91"/>
<label x="292.1" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B34"/>
<wire x1="424.18" y1="12.7" x2="421.64" y2="12.7" width="0.1524" layer="91"/>
<label x="421.64" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B34"/>
<wire x1="381" y1="12.7" x2="378.46" y2="12.7" width="0.1524" layer="91"/>
<label x="378.46" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B34"/>
<wire x1="467.36" y1="12.7" x2="464.82" y2="12.7" width="0.1524" layer="91"/>
<label x="464.82" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B35" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B35"/>
<wire x1="337.82" y1="10.16" x2="335.28" y2="10.16" width="0.1524" layer="91"/>
<label x="335.28" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B35"/>
<wire x1="294.64" y1="10.16" x2="292.1" y2="10.16" width="0.1524" layer="91"/>
<label x="292.1" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B35"/>
<wire x1="424.18" y1="10.16" x2="421.64" y2="10.16" width="0.1524" layer="91"/>
<label x="421.64" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B35"/>
<wire x1="381" y1="10.16" x2="378.46" y2="10.16" width="0.1524" layer="91"/>
<label x="378.46" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B35"/>
<wire x1="467.36" y1="10.16" x2="464.82" y2="10.16" width="0.1524" layer="91"/>
<label x="464.82" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B36" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B36"/>
<wire x1="337.82" y1="7.62" x2="335.28" y2="7.62" width="0.1524" layer="91"/>
<label x="335.28" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B36"/>
<wire x1="294.64" y1="7.62" x2="292.1" y2="7.62" width="0.1524" layer="91"/>
<label x="292.1" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B36"/>
<wire x1="424.18" y1="7.62" x2="421.64" y2="7.62" width="0.1524" layer="91"/>
<label x="421.64" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B36"/>
<wire x1="381" y1="7.62" x2="378.46" y2="7.62" width="0.1524" layer="91"/>
<label x="378.46" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B36"/>
<wire x1="467.36" y1="7.62" x2="464.82" y2="7.62" width="0.1524" layer="91"/>
<label x="464.82" y="7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B37" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B37"/>
<wire x1="337.82" y1="5.08" x2="335.28" y2="5.08" width="0.1524" layer="91"/>
<label x="335.28" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B37"/>
<wire x1="294.64" y1="5.08" x2="292.1" y2="5.08" width="0.1524" layer="91"/>
<label x="292.1" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B37"/>
<wire x1="424.18" y1="5.08" x2="421.64" y2="5.08" width="0.1524" layer="91"/>
<label x="421.64" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B37"/>
<wire x1="381" y1="5.08" x2="378.46" y2="5.08" width="0.1524" layer="91"/>
<label x="378.46" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B37"/>
<wire x1="467.36" y1="5.08" x2="464.82" y2="5.08" width="0.1524" layer="91"/>
<label x="464.82" y="5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B38" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B38"/>
<wire x1="337.82" y1="2.54" x2="335.28" y2="2.54" width="0.1524" layer="91"/>
<label x="335.28" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B38"/>
<wire x1="294.64" y1="2.54" x2="292.1" y2="2.54" width="0.1524" layer="91"/>
<label x="292.1" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B38"/>
<wire x1="424.18" y1="2.54" x2="421.64" y2="2.54" width="0.1524" layer="91"/>
<label x="421.64" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B38"/>
<wire x1="381" y1="2.54" x2="378.46" y2="2.54" width="0.1524" layer="91"/>
<label x="378.46" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B38"/>
<wire x1="467.36" y1="2.54" x2="464.82" y2="2.54" width="0.1524" layer="91"/>
<label x="464.82" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B39" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B39"/>
<wire x1="337.82" y1="0" x2="335.28" y2="0" width="0.1524" layer="91"/>
<label x="335.28" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B39"/>
<wire x1="294.64" y1="0" x2="292.1" y2="0" width="0.1524" layer="91"/>
<label x="292.1" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B39"/>
<wire x1="424.18" y1="0" x2="421.64" y2="0" width="0.1524" layer="91"/>
<label x="421.64" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B39"/>
<wire x1="381" y1="0" x2="378.46" y2="0" width="0.1524" layer="91"/>
<label x="378.46" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B39"/>
<wire x1="467.36" y1="0" x2="464.82" y2="0" width="0.1524" layer="91"/>
<label x="464.82" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B40" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B40"/>
<wire x1="337.82" y1="-2.54" x2="335.28" y2="-2.54" width="0.1524" layer="91"/>
<label x="335.28" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B40"/>
<wire x1="294.64" y1="-2.54" x2="292.1" y2="-2.54" width="0.1524" layer="91"/>
<label x="292.1" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B40"/>
<wire x1="424.18" y1="-2.54" x2="421.64" y2="-2.54" width="0.1524" layer="91"/>
<label x="421.64" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B40"/>
<wire x1="381" y1="-2.54" x2="378.46" y2="-2.54" width="0.1524" layer="91"/>
<label x="378.46" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B40"/>
<wire x1="467.36" y1="-2.54" x2="464.82" y2="-2.54" width="0.1524" layer="91"/>
<label x="464.82" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B41" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B41"/>
<wire x1="337.82" y1="-5.08" x2="335.28" y2="-5.08" width="0.1524" layer="91"/>
<label x="335.28" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B41"/>
<wire x1="294.64" y1="-5.08" x2="292.1" y2="-5.08" width="0.1524" layer="91"/>
<label x="292.1" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B41"/>
<wire x1="424.18" y1="-5.08" x2="421.64" y2="-5.08" width="0.1524" layer="91"/>
<label x="421.64" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B41"/>
<wire x1="381" y1="-5.08" x2="378.46" y2="-5.08" width="0.1524" layer="91"/>
<label x="378.46" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B41"/>
<wire x1="467.36" y1="-5.08" x2="464.82" y2="-5.08" width="0.1524" layer="91"/>
<label x="464.82" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B42" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B42"/>
<wire x1="337.82" y1="-7.62" x2="335.28" y2="-7.62" width="0.1524" layer="91"/>
<label x="335.28" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B42"/>
<wire x1="294.64" y1="-7.62" x2="292.1" y2="-7.62" width="0.1524" layer="91"/>
<label x="292.1" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B42"/>
<wire x1="424.18" y1="-7.62" x2="421.64" y2="-7.62" width="0.1524" layer="91"/>
<label x="421.64" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B42"/>
<wire x1="381" y1="-7.62" x2="378.46" y2="-7.62" width="0.1524" layer="91"/>
<label x="378.46" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B42"/>
<wire x1="467.36" y1="-7.62" x2="464.82" y2="-7.62" width="0.1524" layer="91"/>
<label x="464.82" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B43" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B43"/>
<wire x1="337.82" y1="-10.16" x2="335.28" y2="-10.16" width="0.1524" layer="91"/>
<label x="335.28" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B43"/>
<wire x1="294.64" y1="-10.16" x2="292.1" y2="-10.16" width="0.1524" layer="91"/>
<label x="292.1" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B43"/>
<wire x1="424.18" y1="-10.16" x2="421.64" y2="-10.16" width="0.1524" layer="91"/>
<label x="421.64" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B43"/>
<wire x1="381" y1="-10.16" x2="378.46" y2="-10.16" width="0.1524" layer="91"/>
<label x="378.46" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B43"/>
<wire x1="467.36" y1="-10.16" x2="464.82" y2="-10.16" width="0.1524" layer="91"/>
<label x="464.82" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B44" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B44"/>
<wire x1="337.82" y1="-12.7" x2="335.28" y2="-12.7" width="0.1524" layer="91"/>
<label x="335.28" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B44"/>
<wire x1="294.64" y1="-12.7" x2="292.1" y2="-12.7" width="0.1524" layer="91"/>
<label x="292.1" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B44"/>
<wire x1="424.18" y1="-12.7" x2="421.64" y2="-12.7" width="0.1524" layer="91"/>
<label x="421.64" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B44"/>
<wire x1="381" y1="-12.7" x2="378.46" y2="-12.7" width="0.1524" layer="91"/>
<label x="378.46" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B44"/>
<wire x1="467.36" y1="-12.7" x2="464.82" y2="-12.7" width="0.1524" layer="91"/>
<label x="464.82" y="-12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B45" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B45"/>
<wire x1="337.82" y1="-15.24" x2="335.28" y2="-15.24" width="0.1524" layer="91"/>
<label x="335.28" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B45"/>
<wire x1="294.64" y1="-15.24" x2="292.1" y2="-15.24" width="0.1524" layer="91"/>
<label x="292.1" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B45"/>
<wire x1="424.18" y1="-15.24" x2="421.64" y2="-15.24" width="0.1524" layer="91"/>
<label x="421.64" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B45"/>
<wire x1="381" y1="-15.24" x2="378.46" y2="-15.24" width="0.1524" layer="91"/>
<label x="378.46" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B45"/>
<wire x1="467.36" y1="-15.24" x2="464.82" y2="-15.24" width="0.1524" layer="91"/>
<label x="464.82" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B46" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B46"/>
<wire x1="337.82" y1="-17.78" x2="335.28" y2="-17.78" width="0.1524" layer="91"/>
<label x="335.28" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B46"/>
<wire x1="294.64" y1="-17.78" x2="292.1" y2="-17.78" width="0.1524" layer="91"/>
<label x="292.1" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B46"/>
<wire x1="424.18" y1="-17.78" x2="421.64" y2="-17.78" width="0.1524" layer="91"/>
<label x="421.64" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B46"/>
<wire x1="381" y1="-17.78" x2="378.46" y2="-17.78" width="0.1524" layer="91"/>
<label x="378.46" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B46"/>
<wire x1="467.36" y1="-17.78" x2="464.82" y2="-17.78" width="0.1524" layer="91"/>
<label x="464.82" y="-17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B47" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B47"/>
<wire x1="337.82" y1="-20.32" x2="335.28" y2="-20.32" width="0.1524" layer="91"/>
<label x="335.28" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B47"/>
<wire x1="294.64" y1="-20.32" x2="292.1" y2="-20.32" width="0.1524" layer="91"/>
<label x="292.1" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B47"/>
<wire x1="424.18" y1="-20.32" x2="421.64" y2="-20.32" width="0.1524" layer="91"/>
<label x="421.64" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B47"/>
<wire x1="381" y1="-20.32" x2="378.46" y2="-20.32" width="0.1524" layer="91"/>
<label x="378.46" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B47"/>
<wire x1="467.36" y1="-20.32" x2="464.82" y2="-20.32" width="0.1524" layer="91"/>
<label x="464.82" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B48" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B48"/>
<wire x1="337.82" y1="-22.86" x2="335.28" y2="-22.86" width="0.1524" layer="91"/>
<label x="335.28" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B48"/>
<wire x1="294.64" y1="-22.86" x2="292.1" y2="-22.86" width="0.1524" layer="91"/>
<label x="292.1" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B48"/>
<wire x1="424.18" y1="-22.86" x2="421.64" y2="-22.86" width="0.1524" layer="91"/>
<label x="421.64" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B48"/>
<wire x1="381" y1="-22.86" x2="378.46" y2="-22.86" width="0.1524" layer="91"/>
<label x="378.46" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B48"/>
<wire x1="467.36" y1="-22.86" x2="464.82" y2="-22.86" width="0.1524" layer="91"/>
<label x="464.82" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B49" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B49"/>
<wire x1="337.82" y1="-25.4" x2="335.28" y2="-25.4" width="0.1524" layer="91"/>
<label x="335.28" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B49"/>
<wire x1="294.64" y1="-25.4" x2="292.1" y2="-25.4" width="0.1524" layer="91"/>
<label x="292.1" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B49"/>
<wire x1="424.18" y1="-25.4" x2="421.64" y2="-25.4" width="0.1524" layer="91"/>
<label x="421.64" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B49"/>
<wire x1="381" y1="-25.4" x2="378.46" y2="-25.4" width="0.1524" layer="91"/>
<label x="378.46" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B49"/>
<wire x1="467.36" y1="-25.4" x2="464.82" y2="-25.4" width="0.1524" layer="91"/>
<label x="464.82" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B50" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B50"/>
<wire x1="337.82" y1="-27.94" x2="335.28" y2="-27.94" width="0.1524" layer="91"/>
<label x="335.28" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B50"/>
<wire x1="294.64" y1="-27.94" x2="292.1" y2="-27.94" width="0.1524" layer="91"/>
<label x="292.1" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B50"/>
<wire x1="424.18" y1="-27.94" x2="421.64" y2="-27.94" width="0.1524" layer="91"/>
<label x="421.64" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B50"/>
<wire x1="381" y1="-27.94" x2="378.46" y2="-27.94" width="0.1524" layer="91"/>
<label x="378.46" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B50"/>
<wire x1="467.36" y1="-27.94" x2="464.82" y2="-27.94" width="0.1524" layer="91"/>
<label x="464.82" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B51" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B51"/>
<wire x1="337.82" y1="-30.48" x2="335.28" y2="-30.48" width="0.1524" layer="91"/>
<label x="335.28" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B51"/>
<wire x1="294.64" y1="-30.48" x2="292.1" y2="-30.48" width="0.1524" layer="91"/>
<label x="292.1" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B51"/>
<wire x1="424.18" y1="-30.48" x2="421.64" y2="-30.48" width="0.1524" layer="91"/>
<label x="421.64" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B51"/>
<wire x1="381" y1="-30.48" x2="378.46" y2="-30.48" width="0.1524" layer="91"/>
<label x="378.46" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B51"/>
<wire x1="467.36" y1="-30.48" x2="464.82" y2="-30.48" width="0.1524" layer="91"/>
<label x="464.82" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B52" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B52"/>
<wire x1="337.82" y1="-33.02" x2="335.28" y2="-33.02" width="0.1524" layer="91"/>
<label x="335.28" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B52"/>
<wire x1="294.64" y1="-33.02" x2="292.1" y2="-33.02" width="0.1524" layer="91"/>
<label x="292.1" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B52"/>
<wire x1="424.18" y1="-33.02" x2="421.64" y2="-33.02" width="0.1524" layer="91"/>
<label x="421.64" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B52"/>
<wire x1="381" y1="-33.02" x2="378.46" y2="-33.02" width="0.1524" layer="91"/>
<label x="378.46" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B52"/>
<wire x1="467.36" y1="-33.02" x2="464.82" y2="-33.02" width="0.1524" layer="91"/>
<label x="464.82" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B53" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B53"/>
<wire x1="337.82" y1="-35.56" x2="335.28" y2="-35.56" width="0.1524" layer="91"/>
<label x="335.28" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B53"/>
<wire x1="294.64" y1="-35.56" x2="292.1" y2="-35.56" width="0.1524" layer="91"/>
<label x="292.1" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B53"/>
<wire x1="424.18" y1="-35.56" x2="421.64" y2="-35.56" width="0.1524" layer="91"/>
<label x="421.64" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B53"/>
<wire x1="381" y1="-35.56" x2="378.46" y2="-35.56" width="0.1524" layer="91"/>
<label x="378.46" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B53"/>
<wire x1="467.36" y1="-35.56" x2="464.82" y2="-35.56" width="0.1524" layer="91"/>
<label x="464.82" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B54" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B54"/>
<wire x1="337.82" y1="-38.1" x2="335.28" y2="-38.1" width="0.1524" layer="91"/>
<label x="335.28" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B54"/>
<wire x1="294.64" y1="-38.1" x2="292.1" y2="-38.1" width="0.1524" layer="91"/>
<label x="292.1" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B54"/>
<wire x1="424.18" y1="-38.1" x2="421.64" y2="-38.1" width="0.1524" layer="91"/>
<label x="421.64" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B54"/>
<wire x1="381" y1="-38.1" x2="378.46" y2="-38.1" width="0.1524" layer="91"/>
<label x="378.46" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B54"/>
<wire x1="467.36" y1="-38.1" x2="464.82" y2="-38.1" width="0.1524" layer="91"/>
<label x="464.82" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B55" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B55"/>
<wire x1="337.82" y1="-40.64" x2="335.28" y2="-40.64" width="0.1524" layer="91"/>
<label x="335.28" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B55"/>
<wire x1="294.64" y1="-40.64" x2="292.1" y2="-40.64" width="0.1524" layer="91"/>
<label x="292.1" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B55"/>
<wire x1="424.18" y1="-40.64" x2="421.64" y2="-40.64" width="0.1524" layer="91"/>
<label x="421.64" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B55"/>
<wire x1="381" y1="-40.64" x2="378.46" y2="-40.64" width="0.1524" layer="91"/>
<label x="378.46" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B55"/>
<wire x1="467.36" y1="-40.64" x2="464.82" y2="-40.64" width="0.1524" layer="91"/>
<label x="464.82" y="-40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B56" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B56"/>
<wire x1="337.82" y1="-43.18" x2="335.28" y2="-43.18" width="0.1524" layer="91"/>
<label x="335.28" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B56"/>
<wire x1="294.64" y1="-43.18" x2="292.1" y2="-43.18" width="0.1524" layer="91"/>
<label x="292.1" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B56"/>
<wire x1="424.18" y1="-43.18" x2="421.64" y2="-43.18" width="0.1524" layer="91"/>
<label x="421.64" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B56"/>
<wire x1="381" y1="-43.18" x2="378.46" y2="-43.18" width="0.1524" layer="91"/>
<label x="378.46" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B56"/>
<wire x1="467.36" y1="-43.18" x2="464.82" y2="-43.18" width="0.1524" layer="91"/>
<label x="464.82" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B57" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B57"/>
<wire x1="337.82" y1="-45.72" x2="335.28" y2="-45.72" width="0.1524" layer="91"/>
<label x="335.28" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B57"/>
<wire x1="294.64" y1="-45.72" x2="292.1" y2="-45.72" width="0.1524" layer="91"/>
<label x="292.1" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B57"/>
<wire x1="424.18" y1="-45.72" x2="421.64" y2="-45.72" width="0.1524" layer="91"/>
<label x="421.64" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B57"/>
<wire x1="381" y1="-45.72" x2="378.46" y2="-45.72" width="0.1524" layer="91"/>
<label x="378.46" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B57"/>
<wire x1="467.36" y1="-45.72" x2="464.82" y2="-45.72" width="0.1524" layer="91"/>
<label x="464.82" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B58" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B58"/>
<wire x1="337.82" y1="-48.26" x2="335.28" y2="-48.26" width="0.1524" layer="91"/>
<label x="335.28" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B58"/>
<wire x1="294.64" y1="-48.26" x2="292.1" y2="-48.26" width="0.1524" layer="91"/>
<label x="292.1" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B58"/>
<wire x1="424.18" y1="-48.26" x2="421.64" y2="-48.26" width="0.1524" layer="91"/>
<label x="421.64" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B58"/>
<wire x1="381" y1="-48.26" x2="378.46" y2="-48.26" width="0.1524" layer="91"/>
<label x="378.46" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B58"/>
<wire x1="467.36" y1="-48.26" x2="464.82" y2="-48.26" width="0.1524" layer="91"/>
<label x="464.82" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B59" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B59"/>
<wire x1="337.82" y1="-50.8" x2="335.28" y2="-50.8" width="0.1524" layer="91"/>
<label x="335.28" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B59"/>
<wire x1="294.64" y1="-50.8" x2="292.1" y2="-50.8" width="0.1524" layer="91"/>
<label x="292.1" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B59"/>
<wire x1="424.18" y1="-50.8" x2="421.64" y2="-50.8" width="0.1524" layer="91"/>
<label x="421.64" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B59"/>
<wire x1="381" y1="-50.8" x2="378.46" y2="-50.8" width="0.1524" layer="91"/>
<label x="378.46" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B59"/>
<wire x1="467.36" y1="-50.8" x2="464.82" y2="-50.8" width="0.1524" layer="91"/>
<label x="464.82" y="-50.8" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B60" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B60"/>
<wire x1="337.82" y1="-53.34" x2="335.28" y2="-53.34" width="0.1524" layer="91"/>
<label x="335.28" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B60"/>
<wire x1="294.64" y1="-53.34" x2="292.1" y2="-53.34" width="0.1524" layer="91"/>
<label x="292.1" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B60"/>
<wire x1="424.18" y1="-53.34" x2="421.64" y2="-53.34" width="0.1524" layer="91"/>
<label x="421.64" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B60"/>
<wire x1="381" y1="-53.34" x2="378.46" y2="-53.34" width="0.1524" layer="91"/>
<label x="378.46" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B60"/>
<wire x1="467.36" y1="-53.34" x2="464.82" y2="-53.34" width="0.1524" layer="91"/>
<label x="464.82" y="-53.34" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B61" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B61"/>
<wire x1="337.82" y1="-55.88" x2="335.28" y2="-55.88" width="0.1524" layer="91"/>
<label x="335.28" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B61"/>
<wire x1="294.64" y1="-55.88" x2="292.1" y2="-55.88" width="0.1524" layer="91"/>
<label x="292.1" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B61"/>
<wire x1="424.18" y1="-55.88" x2="421.64" y2="-55.88" width="0.1524" layer="91"/>
<label x="421.64" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B61"/>
<wire x1="381" y1="-55.88" x2="378.46" y2="-55.88" width="0.1524" layer="91"/>
<label x="378.46" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B61"/>
<wire x1="467.36" y1="-55.88" x2="464.82" y2="-55.88" width="0.1524" layer="91"/>
<label x="464.82" y="-55.88" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B62" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B62"/>
<wire x1="337.82" y1="-58.42" x2="335.28" y2="-58.42" width="0.1524" layer="91"/>
<label x="335.28" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B62"/>
<wire x1="294.64" y1="-58.42" x2="292.1" y2="-58.42" width="0.1524" layer="91"/>
<label x="292.1" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B62"/>
<wire x1="424.18" y1="-58.42" x2="421.64" y2="-58.42" width="0.1524" layer="91"/>
<label x="421.64" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B62"/>
<wire x1="381" y1="-58.42" x2="378.46" y2="-58.42" width="0.1524" layer="91"/>
<label x="378.46" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B62"/>
<wire x1="467.36" y1="-58.42" x2="464.82" y2="-58.42" width="0.1524" layer="91"/>
<label x="464.82" y="-58.42" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B63" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B63"/>
<wire x1="337.82" y1="-60.96" x2="335.28" y2="-60.96" width="0.1524" layer="91"/>
<label x="335.28" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B63"/>
<wire x1="294.64" y1="-60.96" x2="292.1" y2="-60.96" width="0.1524" layer="91"/>
<label x="292.1" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B63"/>
<wire x1="424.18" y1="-60.96" x2="421.64" y2="-60.96" width="0.1524" layer="91"/>
<label x="421.64" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B63"/>
<wire x1="381" y1="-60.96" x2="378.46" y2="-60.96" width="0.1524" layer="91"/>
<label x="378.46" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B63"/>
<wire x1="467.36" y1="-60.96" x2="464.82" y2="-60.96" width="0.1524" layer="91"/>
<label x="464.82" y="-60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B64" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B64"/>
<wire x1="337.82" y1="-63.5" x2="335.28" y2="-63.5" width="0.1524" layer="91"/>
<label x="335.28" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B64"/>
<wire x1="294.64" y1="-63.5" x2="292.1" y2="-63.5" width="0.1524" layer="91"/>
<label x="292.1" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B64"/>
<wire x1="424.18" y1="-63.5" x2="421.64" y2="-63.5" width="0.1524" layer="91"/>
<label x="421.64" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B64"/>
<wire x1="381" y1="-63.5" x2="378.46" y2="-63.5" width="0.1524" layer="91"/>
<label x="378.46" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B64"/>
<wire x1="467.36" y1="-63.5" x2="464.82" y2="-63.5" width="0.1524" layer="91"/>
<label x="464.82" y="-63.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B65" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B65"/>
<wire x1="337.82" y1="-66.04" x2="335.28" y2="-66.04" width="0.1524" layer="91"/>
<label x="335.28" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B65"/>
<wire x1="294.64" y1="-66.04" x2="292.1" y2="-66.04" width="0.1524" layer="91"/>
<label x="292.1" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B65"/>
<wire x1="424.18" y1="-66.04" x2="421.64" y2="-66.04" width="0.1524" layer="91"/>
<label x="421.64" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B65"/>
<wire x1="381" y1="-66.04" x2="378.46" y2="-66.04" width="0.1524" layer="91"/>
<label x="378.46" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B65"/>
<wire x1="467.36" y1="-66.04" x2="464.82" y2="-66.04" width="0.1524" layer="91"/>
<label x="464.82" y="-66.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B66" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B66"/>
<wire x1="337.82" y1="-68.58" x2="335.28" y2="-68.58" width="0.1524" layer="91"/>
<label x="335.28" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B66"/>
<wire x1="294.64" y1="-68.58" x2="292.1" y2="-68.58" width="0.1524" layer="91"/>
<label x="292.1" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B66"/>
<wire x1="424.18" y1="-68.58" x2="421.64" y2="-68.58" width="0.1524" layer="91"/>
<label x="421.64" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B66"/>
<wire x1="381" y1="-68.58" x2="378.46" y2="-68.58" width="0.1524" layer="91"/>
<label x="378.46" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B66"/>
<wire x1="467.36" y1="-68.58" x2="464.82" y2="-68.58" width="0.1524" layer="91"/>
<label x="464.82" y="-68.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B67" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B67"/>
<wire x1="337.82" y1="-71.12" x2="335.28" y2="-71.12" width="0.1524" layer="91"/>
<label x="335.28" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B67"/>
<wire x1="294.64" y1="-71.12" x2="292.1" y2="-71.12" width="0.1524" layer="91"/>
<label x="292.1" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B67"/>
<wire x1="424.18" y1="-71.12" x2="421.64" y2="-71.12" width="0.1524" layer="91"/>
<label x="421.64" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B67"/>
<wire x1="381" y1="-71.12" x2="378.46" y2="-71.12" width="0.1524" layer="91"/>
<label x="378.46" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B67"/>
<wire x1="467.36" y1="-71.12" x2="464.82" y2="-71.12" width="0.1524" layer="91"/>
<label x="464.82" y="-71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B68" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B68"/>
<wire x1="337.82" y1="-73.66" x2="335.28" y2="-73.66" width="0.1524" layer="91"/>
<label x="335.28" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B68"/>
<wire x1="294.64" y1="-73.66" x2="292.1" y2="-73.66" width="0.1524" layer="91"/>
<label x="292.1" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B68"/>
<wire x1="424.18" y1="-73.66" x2="421.64" y2="-73.66" width="0.1524" layer="91"/>
<label x="421.64" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B68"/>
<wire x1="381" y1="-73.66" x2="378.46" y2="-73.66" width="0.1524" layer="91"/>
<label x="378.46" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B68"/>
<wire x1="467.36" y1="-73.66" x2="464.82" y2="-73.66" width="0.1524" layer="91"/>
<label x="464.82" y="-73.66" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B69" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B69"/>
<wire x1="337.82" y1="-76.2" x2="335.28" y2="-76.2" width="0.1524" layer="91"/>
<label x="335.28" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B69"/>
<wire x1="294.64" y1="-76.2" x2="292.1" y2="-76.2" width="0.1524" layer="91"/>
<label x="292.1" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B69"/>
<wire x1="424.18" y1="-76.2" x2="421.64" y2="-76.2" width="0.1524" layer="91"/>
<label x="421.64" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B69"/>
<wire x1="381" y1="-76.2" x2="378.46" y2="-76.2" width="0.1524" layer="91"/>
<label x="378.46" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B69"/>
<wire x1="467.36" y1="-76.2" x2="464.82" y2="-76.2" width="0.1524" layer="91"/>
<label x="464.82" y="-76.2" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B70" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B70"/>
<wire x1="337.82" y1="-78.74" x2="335.28" y2="-78.74" width="0.1524" layer="91"/>
<label x="335.28" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B70"/>
<wire x1="294.64" y1="-78.74" x2="292.1" y2="-78.74" width="0.1524" layer="91"/>
<label x="292.1" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B70"/>
<wire x1="424.18" y1="-78.74" x2="421.64" y2="-78.74" width="0.1524" layer="91"/>
<label x="421.64" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B70"/>
<wire x1="381" y1="-78.74" x2="378.46" y2="-78.74" width="0.1524" layer="91"/>
<label x="378.46" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B70"/>
<wire x1="467.36" y1="-78.74" x2="464.82" y2="-78.74" width="0.1524" layer="91"/>
<label x="464.82" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B71" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B71"/>
<wire x1="337.82" y1="-81.28" x2="335.28" y2="-81.28" width="0.1524" layer="91"/>
<label x="335.28" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B71"/>
<wire x1="294.64" y1="-81.28" x2="292.1" y2="-81.28" width="0.1524" layer="91"/>
<label x="292.1" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B71"/>
<wire x1="424.18" y1="-81.28" x2="421.64" y2="-81.28" width="0.1524" layer="91"/>
<label x="421.64" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B71"/>
<wire x1="381" y1="-81.28" x2="378.46" y2="-81.28" width="0.1524" layer="91"/>
<label x="378.46" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B71"/>
<wire x1="467.36" y1="-81.28" x2="464.82" y2="-81.28" width="0.1524" layer="91"/>
<label x="464.82" y="-81.28" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B72" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B72"/>
<wire x1="337.82" y1="-83.82" x2="335.28" y2="-83.82" width="0.1524" layer="91"/>
<label x="335.28" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B72"/>
<wire x1="294.64" y1="-83.82" x2="292.1" y2="-83.82" width="0.1524" layer="91"/>
<label x="292.1" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B72"/>
<wire x1="424.18" y1="-83.82" x2="421.64" y2="-83.82" width="0.1524" layer="91"/>
<label x="421.64" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B72"/>
<wire x1="381" y1="-83.82" x2="378.46" y2="-83.82" width="0.1524" layer="91"/>
<label x="378.46" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B72"/>
<wire x1="467.36" y1="-83.82" x2="464.82" y2="-83.82" width="0.1524" layer="91"/>
<label x="464.82" y="-83.82" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B73" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B73"/>
<wire x1="337.82" y1="-86.36" x2="335.28" y2="-86.36" width="0.1524" layer="91"/>
<label x="335.28" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B73"/>
<wire x1="294.64" y1="-86.36" x2="292.1" y2="-86.36" width="0.1524" layer="91"/>
<label x="292.1" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B73"/>
<wire x1="424.18" y1="-86.36" x2="421.64" y2="-86.36" width="0.1524" layer="91"/>
<label x="421.64" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B73"/>
<wire x1="381" y1="-86.36" x2="378.46" y2="-86.36" width="0.1524" layer="91"/>
<label x="378.46" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B73"/>
<wire x1="467.36" y1="-86.36" x2="464.82" y2="-86.36" width="0.1524" layer="91"/>
<label x="464.82" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B74" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B74"/>
<wire x1="337.82" y1="-88.9" x2="335.28" y2="-88.9" width="0.1524" layer="91"/>
<label x="335.28" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B74"/>
<wire x1="294.64" y1="-88.9" x2="292.1" y2="-88.9" width="0.1524" layer="91"/>
<label x="292.1" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B74"/>
<wire x1="424.18" y1="-88.9" x2="421.64" y2="-88.9" width="0.1524" layer="91"/>
<label x="421.64" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B74"/>
<wire x1="381" y1="-88.9" x2="378.46" y2="-88.9" width="0.1524" layer="91"/>
<label x="378.46" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B74"/>
<wire x1="467.36" y1="-88.9" x2="464.82" y2="-88.9" width="0.1524" layer="91"/>
<label x="464.82" y="-88.9" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B75" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B75"/>
<wire x1="337.82" y1="-91.44" x2="335.28" y2="-91.44" width="0.1524" layer="91"/>
<label x="335.28" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B75"/>
<wire x1="294.64" y1="-91.44" x2="292.1" y2="-91.44" width="0.1524" layer="91"/>
<label x="292.1" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B75"/>
<wire x1="424.18" y1="-91.44" x2="421.64" y2="-91.44" width="0.1524" layer="91"/>
<label x="421.64" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B75"/>
<wire x1="381" y1="-91.44" x2="378.46" y2="-91.44" width="0.1524" layer="91"/>
<label x="378.46" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B75"/>
<wire x1="467.36" y1="-91.44" x2="464.82" y2="-91.44" width="0.1524" layer="91"/>
<label x="464.82" y="-91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B76" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B76"/>
<wire x1="337.82" y1="-93.98" x2="335.28" y2="-93.98" width="0.1524" layer="91"/>
<label x="335.28" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B76"/>
<wire x1="294.64" y1="-93.98" x2="292.1" y2="-93.98" width="0.1524" layer="91"/>
<label x="292.1" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B76"/>
<wire x1="424.18" y1="-93.98" x2="421.64" y2="-93.98" width="0.1524" layer="91"/>
<label x="421.64" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B76"/>
<wire x1="381" y1="-93.98" x2="378.46" y2="-93.98" width="0.1524" layer="91"/>
<label x="378.46" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B76"/>
<wire x1="467.36" y1="-93.98" x2="464.82" y2="-93.98" width="0.1524" layer="91"/>
<label x="464.82" y="-93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B77" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B77"/>
<wire x1="337.82" y1="-96.52" x2="335.28" y2="-96.52" width="0.1524" layer="91"/>
<label x="335.28" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B77"/>
<wire x1="294.64" y1="-96.52" x2="292.1" y2="-96.52" width="0.1524" layer="91"/>
<label x="292.1" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B77"/>
<wire x1="424.18" y1="-96.52" x2="421.64" y2="-96.52" width="0.1524" layer="91"/>
<label x="421.64" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B77"/>
<wire x1="381" y1="-96.52" x2="378.46" y2="-96.52" width="0.1524" layer="91"/>
<label x="378.46" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B77"/>
<wire x1="467.36" y1="-96.52" x2="464.82" y2="-96.52" width="0.1524" layer="91"/>
<label x="464.82" y="-96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B78" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B78"/>
<wire x1="337.82" y1="-99.06" x2="335.28" y2="-99.06" width="0.1524" layer="91"/>
<label x="335.28" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B78"/>
<wire x1="294.64" y1="-99.06" x2="292.1" y2="-99.06" width="0.1524" layer="91"/>
<label x="292.1" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B78"/>
<wire x1="424.18" y1="-99.06" x2="421.64" y2="-99.06" width="0.1524" layer="91"/>
<label x="421.64" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B78"/>
<wire x1="381" y1="-99.06" x2="378.46" y2="-99.06" width="0.1524" layer="91"/>
<label x="378.46" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B78"/>
<wire x1="467.36" y1="-99.06" x2="464.82" y2="-99.06" width="0.1524" layer="91"/>
<label x="464.82" y="-99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B79" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B79"/>
<wire x1="337.82" y1="-101.6" x2="335.28" y2="-101.6" width="0.1524" layer="91"/>
<label x="335.28" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B79"/>
<wire x1="294.64" y1="-101.6" x2="292.1" y2="-101.6" width="0.1524" layer="91"/>
<label x="292.1" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B79"/>
<wire x1="424.18" y1="-101.6" x2="421.64" y2="-101.6" width="0.1524" layer="91"/>
<label x="421.64" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B79"/>
<wire x1="381" y1="-101.6" x2="378.46" y2="-101.6" width="0.1524" layer="91"/>
<label x="378.46" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B79"/>
<wire x1="467.36" y1="-101.6" x2="464.82" y2="-101.6" width="0.1524" layer="91"/>
<label x="464.82" y="-101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B80" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B80"/>
<wire x1="337.82" y1="-104.14" x2="335.28" y2="-104.14" width="0.1524" layer="91"/>
<label x="335.28" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B80"/>
<wire x1="294.64" y1="-104.14" x2="292.1" y2="-104.14" width="0.1524" layer="91"/>
<label x="292.1" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B80"/>
<wire x1="424.18" y1="-104.14" x2="421.64" y2="-104.14" width="0.1524" layer="91"/>
<label x="421.64" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B80"/>
<wire x1="381" y1="-104.14" x2="378.46" y2="-104.14" width="0.1524" layer="91"/>
<label x="378.46" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B80"/>
<wire x1="467.36" y1="-104.14" x2="464.82" y2="-104.14" width="0.1524" layer="91"/>
<label x="464.82" y="-104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B81" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="B81"/>
<wire x1="337.82" y1="-106.68" x2="335.28" y2="-106.68" width="0.1524" layer="91"/>
<label x="335.28" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="B81"/>
<wire x1="294.64" y1="-106.68" x2="292.1" y2="-106.68" width="0.1524" layer="91"/>
<label x="292.1" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="B81"/>
<wire x1="424.18" y1="-106.68" x2="421.64" y2="-106.68" width="0.1524" layer="91"/>
<label x="421.64" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="B81"/>
<wire x1="381" y1="-106.68" x2="378.46" y2="-106.68" width="0.1524" layer="91"/>
<label x="378.46" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="B81"/>
<wire x1="467.36" y1="-106.68" x2="464.82" y2="-106.68" width="0.1524" layer="91"/>
<label x="464.82" y="-106.68" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="A82" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A82"/>
<wire x1="358.14" y1="-109.22" x2="360.68" y2="-109.22" width="0.1524" layer="91"/>
<label x="360.68" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A82"/>
<wire x1="314.96" y1="-109.22" x2="317.5" y2="-109.22" width="0.1524" layer="91"/>
<label x="317.5" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A82"/>
<wire x1="444.5" y1="-109.22" x2="447.04" y2="-109.22" width="0.1524" layer="91"/>
<label x="447.04" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A82"/>
<wire x1="401.32" y1="-109.22" x2="403.86" y2="-109.22" width="0.1524" layer="91"/>
<label x="403.86" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A82"/>
<wire x1="487.68" y1="-109.22" x2="490.22" y2="-109.22" width="0.1524" layer="91"/>
<label x="490.22" y="-109.22" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A81" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A81"/>
<wire x1="358.14" y1="-106.68" x2="360.68" y2="-106.68" width="0.1524" layer="91"/>
<label x="360.68" y="-106.68" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A81"/>
<wire x1="314.96" y1="-106.68" x2="317.5" y2="-106.68" width="0.1524" layer="91"/>
<label x="317.5" y="-106.68" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A81"/>
<wire x1="444.5" y1="-106.68" x2="447.04" y2="-106.68" width="0.1524" layer="91"/>
<label x="447.04" y="-106.68" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A81"/>
<wire x1="401.32" y1="-106.68" x2="403.86" y2="-106.68" width="0.1524" layer="91"/>
<label x="403.86" y="-106.68" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A81"/>
<wire x1="487.68" y1="-106.68" x2="490.22" y2="-106.68" width="0.1524" layer="91"/>
<label x="490.22" y="-106.68" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A80" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A80"/>
<wire x1="358.14" y1="-104.14" x2="360.68" y2="-104.14" width="0.1524" layer="91"/>
<label x="360.68" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A80"/>
<wire x1="314.96" y1="-104.14" x2="317.5" y2="-104.14" width="0.1524" layer="91"/>
<label x="317.5" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A80"/>
<wire x1="444.5" y1="-104.14" x2="447.04" y2="-104.14" width="0.1524" layer="91"/>
<label x="447.04" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A80"/>
<wire x1="401.32" y1="-104.14" x2="403.86" y2="-104.14" width="0.1524" layer="91"/>
<label x="403.86" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A80"/>
<wire x1="487.68" y1="-104.14" x2="490.22" y2="-104.14" width="0.1524" layer="91"/>
<label x="490.22" y="-104.14" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A79" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A79"/>
<wire x1="358.14" y1="-101.6" x2="360.68" y2="-101.6" width="0.1524" layer="91"/>
<label x="360.68" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A79"/>
<wire x1="314.96" y1="-101.6" x2="317.5" y2="-101.6" width="0.1524" layer="91"/>
<label x="317.5" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A79"/>
<wire x1="444.5" y1="-101.6" x2="447.04" y2="-101.6" width="0.1524" layer="91"/>
<label x="447.04" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A79"/>
<wire x1="401.32" y1="-101.6" x2="403.86" y2="-101.6" width="0.1524" layer="91"/>
<label x="403.86" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A79"/>
<wire x1="487.68" y1="-101.6" x2="490.22" y2="-101.6" width="0.1524" layer="91"/>
<label x="490.22" y="-101.6" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A78" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A78"/>
<wire x1="358.14" y1="-99.06" x2="360.68" y2="-99.06" width="0.1524" layer="91"/>
<label x="360.68" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A78"/>
<wire x1="314.96" y1="-99.06" x2="317.5" y2="-99.06" width="0.1524" layer="91"/>
<label x="317.5" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A78"/>
<wire x1="444.5" y1="-99.06" x2="447.04" y2="-99.06" width="0.1524" layer="91"/>
<label x="447.04" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A78"/>
<wire x1="401.32" y1="-99.06" x2="403.86" y2="-99.06" width="0.1524" layer="91"/>
<label x="403.86" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A78"/>
<wire x1="487.68" y1="-99.06" x2="490.22" y2="-99.06" width="0.1524" layer="91"/>
<label x="490.22" y="-99.06" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A77" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A77"/>
<wire x1="358.14" y1="-96.52" x2="360.68" y2="-96.52" width="0.1524" layer="91"/>
<label x="360.68" y="-96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A77"/>
<wire x1="314.96" y1="-96.52" x2="317.5" y2="-96.52" width="0.1524" layer="91"/>
<label x="317.5" y="-96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A77"/>
<wire x1="444.5" y1="-96.52" x2="447.04" y2="-96.52" width="0.1524" layer="91"/>
<label x="447.04" y="-96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A77"/>
<wire x1="401.32" y1="-96.52" x2="403.86" y2="-96.52" width="0.1524" layer="91"/>
<label x="403.86" y="-96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A77"/>
<wire x1="487.68" y1="-96.52" x2="490.22" y2="-96.52" width="0.1524" layer="91"/>
<label x="490.22" y="-96.52" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A76" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A76"/>
<wire x1="358.14" y1="-93.98" x2="360.68" y2="-93.98" width="0.1524" layer="91"/>
<label x="360.68" y="-93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A76"/>
<wire x1="314.96" y1="-93.98" x2="317.5" y2="-93.98" width="0.1524" layer="91"/>
<label x="317.5" y="-93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A76"/>
<wire x1="444.5" y1="-93.98" x2="447.04" y2="-93.98" width="0.1524" layer="91"/>
<label x="447.04" y="-93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A76"/>
<wire x1="401.32" y1="-93.98" x2="403.86" y2="-93.98" width="0.1524" layer="91"/>
<label x="403.86" y="-93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A76"/>
<wire x1="487.68" y1="-93.98" x2="490.22" y2="-93.98" width="0.1524" layer="91"/>
<label x="490.22" y="-93.98" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A75" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A75"/>
<wire x1="358.14" y1="-91.44" x2="360.68" y2="-91.44" width="0.1524" layer="91"/>
<label x="360.68" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A75"/>
<wire x1="314.96" y1="-91.44" x2="317.5" y2="-91.44" width="0.1524" layer="91"/>
<label x="317.5" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A75"/>
<wire x1="444.5" y1="-91.44" x2="447.04" y2="-91.44" width="0.1524" layer="91"/>
<label x="447.04" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A75"/>
<wire x1="401.32" y1="-91.44" x2="403.86" y2="-91.44" width="0.1524" layer="91"/>
<label x="403.86" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A75"/>
<wire x1="487.68" y1="-91.44" x2="490.22" y2="-91.44" width="0.1524" layer="91"/>
<label x="490.22" y="-91.44" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A74" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A74"/>
<wire x1="358.14" y1="-88.9" x2="360.68" y2="-88.9" width="0.1524" layer="91"/>
<label x="360.68" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A74"/>
<wire x1="314.96" y1="-88.9" x2="317.5" y2="-88.9" width="0.1524" layer="91"/>
<label x="317.5" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A74"/>
<wire x1="444.5" y1="-88.9" x2="447.04" y2="-88.9" width="0.1524" layer="91"/>
<label x="447.04" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A74"/>
<wire x1="401.32" y1="-88.9" x2="403.86" y2="-88.9" width="0.1524" layer="91"/>
<label x="403.86" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A74"/>
<wire x1="487.68" y1="-88.9" x2="490.22" y2="-88.9" width="0.1524" layer="91"/>
<label x="490.22" y="-88.9" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A73" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A73"/>
<wire x1="358.14" y1="-86.36" x2="360.68" y2="-86.36" width="0.1524" layer="91"/>
<label x="360.68" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A73"/>
<wire x1="314.96" y1="-86.36" x2="317.5" y2="-86.36" width="0.1524" layer="91"/>
<label x="317.5" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A73"/>
<wire x1="444.5" y1="-86.36" x2="447.04" y2="-86.36" width="0.1524" layer="91"/>
<label x="447.04" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A73"/>
<wire x1="401.32" y1="-86.36" x2="403.86" y2="-86.36" width="0.1524" layer="91"/>
<label x="403.86" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A73"/>
<wire x1="487.68" y1="-86.36" x2="490.22" y2="-86.36" width="0.1524" layer="91"/>
<label x="490.22" y="-86.36" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A72" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A72"/>
<wire x1="358.14" y1="-83.82" x2="360.68" y2="-83.82" width="0.1524" layer="91"/>
<label x="360.68" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A72"/>
<wire x1="314.96" y1="-83.82" x2="317.5" y2="-83.82" width="0.1524" layer="91"/>
<label x="317.5" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A72"/>
<wire x1="444.5" y1="-83.82" x2="447.04" y2="-83.82" width="0.1524" layer="91"/>
<label x="447.04" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A72"/>
<wire x1="401.32" y1="-83.82" x2="403.86" y2="-83.82" width="0.1524" layer="91"/>
<label x="403.86" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A72"/>
<wire x1="487.68" y1="-83.82" x2="490.22" y2="-83.82" width="0.1524" layer="91"/>
<label x="490.22" y="-83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A71" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A71"/>
<wire x1="358.14" y1="-81.28" x2="360.68" y2="-81.28" width="0.1524" layer="91"/>
<label x="360.68" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A71"/>
<wire x1="314.96" y1="-81.28" x2="317.5" y2="-81.28" width="0.1524" layer="91"/>
<label x="317.5" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A71"/>
<wire x1="444.5" y1="-81.28" x2="447.04" y2="-81.28" width="0.1524" layer="91"/>
<label x="447.04" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A71"/>
<wire x1="401.32" y1="-81.28" x2="403.86" y2="-81.28" width="0.1524" layer="91"/>
<label x="403.86" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A71"/>
<wire x1="487.68" y1="-81.28" x2="490.22" y2="-81.28" width="0.1524" layer="91"/>
<label x="490.22" y="-81.28" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A70" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A70"/>
<wire x1="358.14" y1="-78.74" x2="360.68" y2="-78.74" width="0.1524" layer="91"/>
<label x="360.68" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A70"/>
<wire x1="314.96" y1="-78.74" x2="317.5" y2="-78.74" width="0.1524" layer="91"/>
<label x="317.5" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A70"/>
<wire x1="444.5" y1="-78.74" x2="447.04" y2="-78.74" width="0.1524" layer="91"/>
<label x="447.04" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A70"/>
<wire x1="401.32" y1="-78.74" x2="403.86" y2="-78.74" width="0.1524" layer="91"/>
<label x="403.86" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A70"/>
<wire x1="487.68" y1="-78.74" x2="490.22" y2="-78.74" width="0.1524" layer="91"/>
<label x="490.22" y="-78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A69" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A69"/>
<wire x1="358.14" y1="-76.2" x2="360.68" y2="-76.2" width="0.1524" layer="91"/>
<label x="360.68" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A69"/>
<wire x1="314.96" y1="-76.2" x2="317.5" y2="-76.2" width="0.1524" layer="91"/>
<label x="317.5" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A69"/>
<wire x1="444.5" y1="-76.2" x2="447.04" y2="-76.2" width="0.1524" layer="91"/>
<label x="447.04" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A69"/>
<wire x1="401.32" y1="-76.2" x2="403.86" y2="-76.2" width="0.1524" layer="91"/>
<label x="403.86" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A69"/>
<wire x1="487.68" y1="-76.2" x2="490.22" y2="-76.2" width="0.1524" layer="91"/>
<label x="490.22" y="-76.2" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A68" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A68"/>
<wire x1="358.14" y1="-73.66" x2="360.68" y2="-73.66" width="0.1524" layer="91"/>
<label x="360.68" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A68"/>
<wire x1="314.96" y1="-73.66" x2="317.5" y2="-73.66" width="0.1524" layer="91"/>
<label x="317.5" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A68"/>
<wire x1="444.5" y1="-73.66" x2="447.04" y2="-73.66" width="0.1524" layer="91"/>
<label x="447.04" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A68"/>
<wire x1="401.32" y1="-73.66" x2="403.86" y2="-73.66" width="0.1524" layer="91"/>
<label x="403.86" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A68"/>
<wire x1="487.68" y1="-73.66" x2="490.22" y2="-73.66" width="0.1524" layer="91"/>
<label x="490.22" y="-73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A67" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A67"/>
<wire x1="358.14" y1="-71.12" x2="360.68" y2="-71.12" width="0.1524" layer="91"/>
<label x="360.68" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A67"/>
<wire x1="314.96" y1="-71.12" x2="317.5" y2="-71.12" width="0.1524" layer="91"/>
<label x="317.5" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A67"/>
<wire x1="444.5" y1="-71.12" x2="447.04" y2="-71.12" width="0.1524" layer="91"/>
<label x="447.04" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A67"/>
<wire x1="401.32" y1="-71.12" x2="403.86" y2="-71.12" width="0.1524" layer="91"/>
<label x="403.86" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A67"/>
<wire x1="487.68" y1="-71.12" x2="490.22" y2="-71.12" width="0.1524" layer="91"/>
<label x="490.22" y="-71.12" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A66" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A66"/>
<wire x1="358.14" y1="-68.58" x2="360.68" y2="-68.58" width="0.1524" layer="91"/>
<label x="360.68" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A66"/>
<wire x1="314.96" y1="-68.58" x2="317.5" y2="-68.58" width="0.1524" layer="91"/>
<label x="317.5" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A66"/>
<wire x1="444.5" y1="-68.58" x2="447.04" y2="-68.58" width="0.1524" layer="91"/>
<label x="447.04" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A66"/>
<wire x1="401.32" y1="-68.58" x2="403.86" y2="-68.58" width="0.1524" layer="91"/>
<label x="403.86" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A66"/>
<wire x1="487.68" y1="-68.58" x2="490.22" y2="-68.58" width="0.1524" layer="91"/>
<label x="490.22" y="-68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A65" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A65"/>
<wire x1="358.14" y1="-66.04" x2="360.68" y2="-66.04" width="0.1524" layer="91"/>
<label x="360.68" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A65"/>
<wire x1="314.96" y1="-66.04" x2="317.5" y2="-66.04" width="0.1524" layer="91"/>
<label x="317.5" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A65"/>
<wire x1="444.5" y1="-66.04" x2="447.04" y2="-66.04" width="0.1524" layer="91"/>
<label x="447.04" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A65"/>
<wire x1="401.32" y1="-66.04" x2="403.86" y2="-66.04" width="0.1524" layer="91"/>
<label x="403.86" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A65"/>
<wire x1="487.68" y1="-66.04" x2="490.22" y2="-66.04" width="0.1524" layer="91"/>
<label x="490.22" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A64" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A64"/>
<wire x1="358.14" y1="-63.5" x2="360.68" y2="-63.5" width="0.1524" layer="91"/>
<label x="360.68" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A64"/>
<wire x1="314.96" y1="-63.5" x2="317.5" y2="-63.5" width="0.1524" layer="91"/>
<label x="317.5" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A64"/>
<wire x1="444.5" y1="-63.5" x2="447.04" y2="-63.5" width="0.1524" layer="91"/>
<label x="447.04" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A64"/>
<wire x1="401.32" y1="-63.5" x2="403.86" y2="-63.5" width="0.1524" layer="91"/>
<label x="403.86" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A64"/>
<wire x1="487.68" y1="-63.5" x2="490.22" y2="-63.5" width="0.1524" layer="91"/>
<label x="490.22" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A63" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A63"/>
<wire x1="358.14" y1="-60.96" x2="360.68" y2="-60.96" width="0.1524" layer="91"/>
<label x="360.68" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A63"/>
<wire x1="314.96" y1="-60.96" x2="317.5" y2="-60.96" width="0.1524" layer="91"/>
<label x="317.5" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A63"/>
<wire x1="444.5" y1="-60.96" x2="447.04" y2="-60.96" width="0.1524" layer="91"/>
<label x="447.04" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A63"/>
<wire x1="401.32" y1="-60.96" x2="403.86" y2="-60.96" width="0.1524" layer="91"/>
<label x="403.86" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A63"/>
<wire x1="487.68" y1="-60.96" x2="490.22" y2="-60.96" width="0.1524" layer="91"/>
<label x="490.22" y="-60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A62" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A62"/>
<wire x1="358.14" y1="-58.42" x2="360.68" y2="-58.42" width="0.1524" layer="91"/>
<label x="360.68" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A62"/>
<wire x1="314.96" y1="-58.42" x2="317.5" y2="-58.42" width="0.1524" layer="91"/>
<label x="317.5" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A62"/>
<wire x1="444.5" y1="-58.42" x2="447.04" y2="-58.42" width="0.1524" layer="91"/>
<label x="447.04" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A62"/>
<wire x1="401.32" y1="-58.42" x2="403.86" y2="-58.42" width="0.1524" layer="91"/>
<label x="403.86" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A62"/>
<wire x1="487.68" y1="-58.42" x2="490.22" y2="-58.42" width="0.1524" layer="91"/>
<label x="490.22" y="-58.42" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A61" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A61"/>
<wire x1="358.14" y1="-55.88" x2="360.68" y2="-55.88" width="0.1524" layer="91"/>
<label x="360.68" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A61"/>
<wire x1="314.96" y1="-55.88" x2="317.5" y2="-55.88" width="0.1524" layer="91"/>
<label x="317.5" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A61"/>
<wire x1="444.5" y1="-55.88" x2="447.04" y2="-55.88" width="0.1524" layer="91"/>
<label x="447.04" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A61"/>
<wire x1="401.32" y1="-55.88" x2="403.86" y2="-55.88" width="0.1524" layer="91"/>
<label x="403.86" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A61"/>
<wire x1="487.68" y1="-55.88" x2="490.22" y2="-55.88" width="0.1524" layer="91"/>
<label x="490.22" y="-55.88" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A60" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A60"/>
<wire x1="358.14" y1="-53.34" x2="360.68" y2="-53.34" width="0.1524" layer="91"/>
<label x="360.68" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A60"/>
<wire x1="314.96" y1="-53.34" x2="317.5" y2="-53.34" width="0.1524" layer="91"/>
<label x="317.5" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A60"/>
<wire x1="444.5" y1="-53.34" x2="447.04" y2="-53.34" width="0.1524" layer="91"/>
<label x="447.04" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A60"/>
<wire x1="401.32" y1="-53.34" x2="403.86" y2="-53.34" width="0.1524" layer="91"/>
<label x="403.86" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A60"/>
<wire x1="487.68" y1="-53.34" x2="490.22" y2="-53.34" width="0.1524" layer="91"/>
<label x="490.22" y="-53.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A59" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A59"/>
<wire x1="358.14" y1="-50.8" x2="360.68" y2="-50.8" width="0.1524" layer="91"/>
<label x="360.68" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A59"/>
<wire x1="314.96" y1="-50.8" x2="317.5" y2="-50.8" width="0.1524" layer="91"/>
<label x="317.5" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A59"/>
<wire x1="444.5" y1="-50.8" x2="447.04" y2="-50.8" width="0.1524" layer="91"/>
<label x="447.04" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A59"/>
<wire x1="401.32" y1="-50.8" x2="403.86" y2="-50.8" width="0.1524" layer="91"/>
<label x="403.86" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A59"/>
<wire x1="487.68" y1="-50.8" x2="490.22" y2="-50.8" width="0.1524" layer="91"/>
<label x="490.22" y="-50.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A58" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A58"/>
<wire x1="358.14" y1="-48.26" x2="360.68" y2="-48.26" width="0.1524" layer="91"/>
<label x="360.68" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A58"/>
<wire x1="314.96" y1="-48.26" x2="317.5" y2="-48.26" width="0.1524" layer="91"/>
<label x="317.5" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A58"/>
<wire x1="444.5" y1="-48.26" x2="447.04" y2="-48.26" width="0.1524" layer="91"/>
<label x="447.04" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A58"/>
<wire x1="401.32" y1="-48.26" x2="403.86" y2="-48.26" width="0.1524" layer="91"/>
<label x="403.86" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A58"/>
<wire x1="487.68" y1="-48.26" x2="490.22" y2="-48.26" width="0.1524" layer="91"/>
<label x="490.22" y="-48.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A57" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A57"/>
<wire x1="358.14" y1="-45.72" x2="360.68" y2="-45.72" width="0.1524" layer="91"/>
<label x="360.68" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A57"/>
<wire x1="314.96" y1="-45.72" x2="317.5" y2="-45.72" width="0.1524" layer="91"/>
<label x="317.5" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A57"/>
<wire x1="444.5" y1="-45.72" x2="447.04" y2="-45.72" width="0.1524" layer="91"/>
<label x="447.04" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A57"/>
<wire x1="401.32" y1="-45.72" x2="403.86" y2="-45.72" width="0.1524" layer="91"/>
<label x="403.86" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A57"/>
<wire x1="487.68" y1="-45.72" x2="490.22" y2="-45.72" width="0.1524" layer="91"/>
<label x="490.22" y="-45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A56" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A56"/>
<wire x1="358.14" y1="-43.18" x2="360.68" y2="-43.18" width="0.1524" layer="91"/>
<label x="360.68" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A56"/>
<wire x1="314.96" y1="-43.18" x2="317.5" y2="-43.18" width="0.1524" layer="91"/>
<label x="317.5" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A56"/>
<wire x1="444.5" y1="-43.18" x2="447.04" y2="-43.18" width="0.1524" layer="91"/>
<label x="447.04" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A56"/>
<wire x1="401.32" y1="-43.18" x2="403.86" y2="-43.18" width="0.1524" layer="91"/>
<label x="403.86" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A56"/>
<wire x1="487.68" y1="-43.18" x2="490.22" y2="-43.18" width="0.1524" layer="91"/>
<label x="490.22" y="-43.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A55" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A55"/>
<wire x1="358.14" y1="-40.64" x2="360.68" y2="-40.64" width="0.1524" layer="91"/>
<label x="360.68" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A55"/>
<wire x1="314.96" y1="-40.64" x2="317.5" y2="-40.64" width="0.1524" layer="91"/>
<label x="317.5" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A55"/>
<wire x1="444.5" y1="-40.64" x2="447.04" y2="-40.64" width="0.1524" layer="91"/>
<label x="447.04" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A55"/>
<wire x1="401.32" y1="-40.64" x2="403.86" y2="-40.64" width="0.1524" layer="91"/>
<label x="403.86" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A55"/>
<wire x1="487.68" y1="-40.64" x2="490.22" y2="-40.64" width="0.1524" layer="91"/>
<label x="490.22" y="-40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A54" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A54"/>
<wire x1="358.14" y1="-38.1" x2="360.68" y2="-38.1" width="0.1524" layer="91"/>
<label x="360.68" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A54"/>
<wire x1="314.96" y1="-38.1" x2="317.5" y2="-38.1" width="0.1524" layer="91"/>
<label x="317.5" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A54"/>
<wire x1="444.5" y1="-38.1" x2="447.04" y2="-38.1" width="0.1524" layer="91"/>
<label x="447.04" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A54"/>
<wire x1="401.32" y1="-38.1" x2="403.86" y2="-38.1" width="0.1524" layer="91"/>
<label x="403.86" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A54"/>
<wire x1="487.68" y1="-38.1" x2="490.22" y2="-38.1" width="0.1524" layer="91"/>
<label x="490.22" y="-38.1" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A53" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A53"/>
<wire x1="358.14" y1="-35.56" x2="360.68" y2="-35.56" width="0.1524" layer="91"/>
<label x="360.68" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A53"/>
<wire x1="314.96" y1="-35.56" x2="317.5" y2="-35.56" width="0.1524" layer="91"/>
<label x="317.5" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A53"/>
<wire x1="444.5" y1="-35.56" x2="447.04" y2="-35.56" width="0.1524" layer="91"/>
<label x="447.04" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A53"/>
<wire x1="401.32" y1="-35.56" x2="403.86" y2="-35.56" width="0.1524" layer="91"/>
<label x="403.86" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A53"/>
<wire x1="487.68" y1="-35.56" x2="490.22" y2="-35.56" width="0.1524" layer="91"/>
<label x="490.22" y="-35.56" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A52" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A52"/>
<wire x1="358.14" y1="-33.02" x2="360.68" y2="-33.02" width="0.1524" layer="91"/>
<label x="360.68" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A52"/>
<wire x1="314.96" y1="-33.02" x2="317.5" y2="-33.02" width="0.1524" layer="91"/>
<label x="317.5" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A52"/>
<wire x1="444.5" y1="-33.02" x2="447.04" y2="-33.02" width="0.1524" layer="91"/>
<label x="447.04" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A52"/>
<wire x1="401.32" y1="-33.02" x2="403.86" y2="-33.02" width="0.1524" layer="91"/>
<label x="403.86" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A52"/>
<wire x1="487.68" y1="-33.02" x2="490.22" y2="-33.02" width="0.1524" layer="91"/>
<label x="490.22" y="-33.02" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A51" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A51"/>
<wire x1="358.14" y1="-30.48" x2="360.68" y2="-30.48" width="0.1524" layer="91"/>
<label x="360.68" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A51"/>
<wire x1="314.96" y1="-30.48" x2="317.5" y2="-30.48" width="0.1524" layer="91"/>
<label x="317.5" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A51"/>
<wire x1="444.5" y1="-30.48" x2="447.04" y2="-30.48" width="0.1524" layer="91"/>
<label x="447.04" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A51"/>
<wire x1="401.32" y1="-30.48" x2="403.86" y2="-30.48" width="0.1524" layer="91"/>
<label x="403.86" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A51"/>
<wire x1="487.68" y1="-30.48" x2="490.22" y2="-30.48" width="0.1524" layer="91"/>
<label x="490.22" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A50" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A50"/>
<wire x1="358.14" y1="-27.94" x2="360.68" y2="-27.94" width="0.1524" layer="91"/>
<label x="360.68" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A50"/>
<wire x1="314.96" y1="-27.94" x2="317.5" y2="-27.94" width="0.1524" layer="91"/>
<label x="317.5" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A50"/>
<wire x1="444.5" y1="-27.94" x2="447.04" y2="-27.94" width="0.1524" layer="91"/>
<label x="447.04" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A50"/>
<wire x1="401.32" y1="-27.94" x2="403.86" y2="-27.94" width="0.1524" layer="91"/>
<label x="403.86" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A50"/>
<wire x1="487.68" y1="-27.94" x2="490.22" y2="-27.94" width="0.1524" layer="91"/>
<label x="490.22" y="-27.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A49" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A49"/>
<wire x1="358.14" y1="-25.4" x2="360.68" y2="-25.4" width="0.1524" layer="91"/>
<label x="360.68" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A49"/>
<wire x1="314.96" y1="-25.4" x2="317.5" y2="-25.4" width="0.1524" layer="91"/>
<label x="317.5" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A49"/>
<wire x1="444.5" y1="-25.4" x2="447.04" y2="-25.4" width="0.1524" layer="91"/>
<label x="447.04" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A49"/>
<wire x1="401.32" y1="-25.4" x2="403.86" y2="-25.4" width="0.1524" layer="91"/>
<label x="403.86" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A49"/>
<wire x1="487.68" y1="-25.4" x2="490.22" y2="-25.4" width="0.1524" layer="91"/>
<label x="490.22" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A48" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A48"/>
<wire x1="358.14" y1="-22.86" x2="360.68" y2="-22.86" width="0.1524" layer="91"/>
<label x="360.68" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A48"/>
<wire x1="314.96" y1="-22.86" x2="317.5" y2="-22.86" width="0.1524" layer="91"/>
<label x="317.5" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A48"/>
<wire x1="444.5" y1="-22.86" x2="447.04" y2="-22.86" width="0.1524" layer="91"/>
<label x="447.04" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A48"/>
<wire x1="401.32" y1="-22.86" x2="403.86" y2="-22.86" width="0.1524" layer="91"/>
<label x="403.86" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A48"/>
<wire x1="487.68" y1="-22.86" x2="490.22" y2="-22.86" width="0.1524" layer="91"/>
<label x="490.22" y="-22.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A47" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A47"/>
<wire x1="358.14" y1="-20.32" x2="360.68" y2="-20.32" width="0.1524" layer="91"/>
<label x="360.68" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A47"/>
<wire x1="314.96" y1="-20.32" x2="317.5" y2="-20.32" width="0.1524" layer="91"/>
<label x="317.5" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A47"/>
<wire x1="444.5" y1="-20.32" x2="447.04" y2="-20.32" width="0.1524" layer="91"/>
<label x="447.04" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A47"/>
<wire x1="401.32" y1="-20.32" x2="403.86" y2="-20.32" width="0.1524" layer="91"/>
<label x="403.86" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A47"/>
<wire x1="487.68" y1="-20.32" x2="490.22" y2="-20.32" width="0.1524" layer="91"/>
<label x="490.22" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A46" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A46"/>
<wire x1="358.14" y1="-17.78" x2="360.68" y2="-17.78" width="0.1524" layer="91"/>
<label x="360.68" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A46"/>
<wire x1="314.96" y1="-17.78" x2="317.5" y2="-17.78" width="0.1524" layer="91"/>
<label x="317.5" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A46"/>
<wire x1="444.5" y1="-17.78" x2="447.04" y2="-17.78" width="0.1524" layer="91"/>
<label x="447.04" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A46"/>
<wire x1="401.32" y1="-17.78" x2="403.86" y2="-17.78" width="0.1524" layer="91"/>
<label x="403.86" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A46"/>
<wire x1="487.68" y1="-17.78" x2="490.22" y2="-17.78" width="0.1524" layer="91"/>
<label x="490.22" y="-17.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A45" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A45"/>
<wire x1="358.14" y1="-15.24" x2="360.68" y2="-15.24" width="0.1524" layer="91"/>
<label x="360.68" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A45"/>
<wire x1="314.96" y1="-15.24" x2="317.5" y2="-15.24" width="0.1524" layer="91"/>
<label x="317.5" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A45"/>
<wire x1="444.5" y1="-15.24" x2="447.04" y2="-15.24" width="0.1524" layer="91"/>
<label x="447.04" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A45"/>
<wire x1="401.32" y1="-15.24" x2="403.86" y2="-15.24" width="0.1524" layer="91"/>
<label x="403.86" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A45"/>
<wire x1="487.68" y1="-15.24" x2="490.22" y2="-15.24" width="0.1524" layer="91"/>
<label x="490.22" y="-15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A44" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A44"/>
<wire x1="358.14" y1="-12.7" x2="360.68" y2="-12.7" width="0.1524" layer="91"/>
<label x="360.68" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A44"/>
<wire x1="314.96" y1="-12.7" x2="317.5" y2="-12.7" width="0.1524" layer="91"/>
<label x="317.5" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A44"/>
<wire x1="444.5" y1="-12.7" x2="447.04" y2="-12.7" width="0.1524" layer="91"/>
<label x="447.04" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A44"/>
<wire x1="401.32" y1="-12.7" x2="403.86" y2="-12.7" width="0.1524" layer="91"/>
<label x="403.86" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A44"/>
<wire x1="487.68" y1="-12.7" x2="490.22" y2="-12.7" width="0.1524" layer="91"/>
<label x="490.22" y="-12.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A43" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A43"/>
<wire x1="358.14" y1="-10.16" x2="360.68" y2="-10.16" width="0.1524" layer="91"/>
<label x="360.68" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A43"/>
<wire x1="314.96" y1="-10.16" x2="317.5" y2="-10.16" width="0.1524" layer="91"/>
<label x="317.5" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A43"/>
<wire x1="444.5" y1="-10.16" x2="447.04" y2="-10.16" width="0.1524" layer="91"/>
<label x="447.04" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A43"/>
<wire x1="401.32" y1="-10.16" x2="403.86" y2="-10.16" width="0.1524" layer="91"/>
<label x="403.86" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A43"/>
<wire x1="487.68" y1="-10.16" x2="490.22" y2="-10.16" width="0.1524" layer="91"/>
<label x="490.22" y="-10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A42" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A42"/>
<wire x1="358.14" y1="-7.62" x2="360.68" y2="-7.62" width="0.1524" layer="91"/>
<label x="360.68" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A42"/>
<wire x1="314.96" y1="-7.62" x2="317.5" y2="-7.62" width="0.1524" layer="91"/>
<label x="317.5" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A42"/>
<wire x1="444.5" y1="-7.62" x2="447.04" y2="-7.62" width="0.1524" layer="91"/>
<label x="447.04" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A42"/>
<wire x1="401.32" y1="-7.62" x2="403.86" y2="-7.62" width="0.1524" layer="91"/>
<label x="403.86" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A42"/>
<wire x1="487.68" y1="-7.62" x2="490.22" y2="-7.62" width="0.1524" layer="91"/>
<label x="490.22" y="-7.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A41" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A41"/>
<wire x1="358.14" y1="-5.08" x2="360.68" y2="-5.08" width="0.1524" layer="91"/>
<label x="360.68" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A41"/>
<wire x1="314.96" y1="-5.08" x2="317.5" y2="-5.08" width="0.1524" layer="91"/>
<label x="317.5" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A41"/>
<wire x1="444.5" y1="-5.08" x2="447.04" y2="-5.08" width="0.1524" layer="91"/>
<label x="447.04" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A41"/>
<wire x1="401.32" y1="-5.08" x2="403.86" y2="-5.08" width="0.1524" layer="91"/>
<label x="403.86" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A41"/>
<wire x1="487.68" y1="-5.08" x2="490.22" y2="-5.08" width="0.1524" layer="91"/>
<label x="490.22" y="-5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A40" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A40"/>
<wire x1="358.14" y1="-2.54" x2="360.68" y2="-2.54" width="0.1524" layer="91"/>
<label x="360.68" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A40"/>
<wire x1="314.96" y1="-2.54" x2="317.5" y2="-2.54" width="0.1524" layer="91"/>
<label x="317.5" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A40"/>
<wire x1="444.5" y1="-2.54" x2="447.04" y2="-2.54" width="0.1524" layer="91"/>
<label x="447.04" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A40"/>
<wire x1="401.32" y1="-2.54" x2="403.86" y2="-2.54" width="0.1524" layer="91"/>
<label x="403.86" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A40"/>
<wire x1="487.68" y1="-2.54" x2="490.22" y2="-2.54" width="0.1524" layer="91"/>
<label x="490.22" y="-2.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A39" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A39"/>
<wire x1="358.14" y1="0" x2="360.68" y2="0" width="0.1524" layer="91"/>
<label x="360.68" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A39"/>
<wire x1="314.96" y1="0" x2="317.5" y2="0" width="0.1524" layer="91"/>
<label x="317.5" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A39"/>
<wire x1="444.5" y1="0" x2="447.04" y2="0" width="0.1524" layer="91"/>
<label x="447.04" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A39"/>
<wire x1="401.32" y1="0" x2="403.86" y2="0" width="0.1524" layer="91"/>
<label x="403.86" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A39"/>
<wire x1="487.68" y1="0" x2="490.22" y2="0" width="0.1524" layer="91"/>
<label x="490.22" y="0" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A38" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A38"/>
<wire x1="358.14" y1="2.54" x2="360.68" y2="2.54" width="0.1524" layer="91"/>
<label x="360.68" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A38"/>
<wire x1="314.96" y1="2.54" x2="317.5" y2="2.54" width="0.1524" layer="91"/>
<label x="317.5" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A38"/>
<wire x1="444.5" y1="2.54" x2="447.04" y2="2.54" width="0.1524" layer="91"/>
<label x="447.04" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A38"/>
<wire x1="401.32" y1="2.54" x2="403.86" y2="2.54" width="0.1524" layer="91"/>
<label x="403.86" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A38"/>
<wire x1="487.68" y1="2.54" x2="490.22" y2="2.54" width="0.1524" layer="91"/>
<label x="490.22" y="2.54" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A37" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A37"/>
<wire x1="358.14" y1="5.08" x2="360.68" y2="5.08" width="0.1524" layer="91"/>
<label x="360.68" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A37"/>
<wire x1="314.96" y1="5.08" x2="317.5" y2="5.08" width="0.1524" layer="91"/>
<label x="317.5" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A37"/>
<wire x1="444.5" y1="5.08" x2="447.04" y2="5.08" width="0.1524" layer="91"/>
<label x="447.04" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A37"/>
<wire x1="401.32" y1="5.08" x2="403.86" y2="5.08" width="0.1524" layer="91"/>
<label x="403.86" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A37"/>
<wire x1="487.68" y1="5.08" x2="490.22" y2="5.08" width="0.1524" layer="91"/>
<label x="490.22" y="5.08" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A36" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A36"/>
<wire x1="358.14" y1="7.62" x2="360.68" y2="7.62" width="0.1524" layer="91"/>
<label x="360.68" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A36"/>
<wire x1="314.96" y1="7.62" x2="317.5" y2="7.62" width="0.1524" layer="91"/>
<label x="317.5" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A36"/>
<wire x1="444.5" y1="7.62" x2="447.04" y2="7.62" width="0.1524" layer="91"/>
<label x="447.04" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A36"/>
<wire x1="401.32" y1="7.62" x2="403.86" y2="7.62" width="0.1524" layer="91"/>
<label x="403.86" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A36"/>
<wire x1="487.68" y1="7.62" x2="490.22" y2="7.62" width="0.1524" layer="91"/>
<label x="490.22" y="7.62" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A35" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A35"/>
<wire x1="358.14" y1="10.16" x2="360.68" y2="10.16" width="0.1524" layer="91"/>
<label x="360.68" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A35"/>
<wire x1="314.96" y1="10.16" x2="317.5" y2="10.16" width="0.1524" layer="91"/>
<label x="317.5" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A35"/>
<wire x1="444.5" y1="10.16" x2="447.04" y2="10.16" width="0.1524" layer="91"/>
<label x="447.04" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A35"/>
<wire x1="401.32" y1="10.16" x2="403.86" y2="10.16" width="0.1524" layer="91"/>
<label x="403.86" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A35"/>
<wire x1="487.68" y1="10.16" x2="490.22" y2="10.16" width="0.1524" layer="91"/>
<label x="490.22" y="10.16" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A34" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A34"/>
<wire x1="358.14" y1="12.7" x2="360.68" y2="12.7" width="0.1524" layer="91"/>
<label x="360.68" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A34"/>
<wire x1="314.96" y1="12.7" x2="317.5" y2="12.7" width="0.1524" layer="91"/>
<label x="317.5" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A34"/>
<wire x1="444.5" y1="12.7" x2="447.04" y2="12.7" width="0.1524" layer="91"/>
<label x="447.04" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A34"/>
<wire x1="401.32" y1="12.7" x2="403.86" y2="12.7" width="0.1524" layer="91"/>
<label x="403.86" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A34"/>
<wire x1="487.68" y1="12.7" x2="490.22" y2="12.7" width="0.1524" layer="91"/>
<label x="490.22" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A33" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A33"/>
<wire x1="358.14" y1="15.24" x2="360.68" y2="15.24" width="0.1524" layer="91"/>
<label x="360.68" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A33"/>
<wire x1="314.96" y1="15.24" x2="317.5" y2="15.24" width="0.1524" layer="91"/>
<label x="317.5" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A33"/>
<wire x1="444.5" y1="15.24" x2="447.04" y2="15.24" width="0.1524" layer="91"/>
<label x="447.04" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A33"/>
<wire x1="401.32" y1="15.24" x2="403.86" y2="15.24" width="0.1524" layer="91"/>
<label x="403.86" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A33"/>
<wire x1="487.68" y1="15.24" x2="490.22" y2="15.24" width="0.1524" layer="91"/>
<label x="490.22" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A32" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A32"/>
<wire x1="358.14" y1="17.78" x2="360.68" y2="17.78" width="0.1524" layer="91"/>
<label x="360.68" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A32"/>
<wire x1="314.96" y1="17.78" x2="317.5" y2="17.78" width="0.1524" layer="91"/>
<label x="317.5" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A32"/>
<wire x1="444.5" y1="17.78" x2="447.04" y2="17.78" width="0.1524" layer="91"/>
<label x="447.04" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A32"/>
<wire x1="401.32" y1="17.78" x2="403.86" y2="17.78" width="0.1524" layer="91"/>
<label x="403.86" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A32"/>
<wire x1="487.68" y1="17.78" x2="490.22" y2="17.78" width="0.1524" layer="91"/>
<label x="490.22" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A31" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A31"/>
<wire x1="358.14" y1="20.32" x2="360.68" y2="20.32" width="0.1524" layer="91"/>
<label x="360.68" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A31"/>
<wire x1="314.96" y1="20.32" x2="317.5" y2="20.32" width="0.1524" layer="91"/>
<label x="317.5" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A31"/>
<wire x1="444.5" y1="20.32" x2="447.04" y2="20.32" width="0.1524" layer="91"/>
<label x="447.04" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A31"/>
<wire x1="401.32" y1="20.32" x2="403.86" y2="20.32" width="0.1524" layer="91"/>
<label x="403.86" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A31"/>
<wire x1="487.68" y1="20.32" x2="490.22" y2="20.32" width="0.1524" layer="91"/>
<label x="490.22" y="20.32" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A30" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A30"/>
<wire x1="358.14" y1="22.86" x2="360.68" y2="22.86" width="0.1524" layer="91"/>
<label x="360.68" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A30"/>
<wire x1="314.96" y1="22.86" x2="317.5" y2="22.86" width="0.1524" layer="91"/>
<label x="317.5" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A30"/>
<wire x1="444.5" y1="22.86" x2="447.04" y2="22.86" width="0.1524" layer="91"/>
<label x="447.04" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A30"/>
<wire x1="401.32" y1="22.86" x2="403.86" y2="22.86" width="0.1524" layer="91"/>
<label x="403.86" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A30"/>
<wire x1="487.68" y1="22.86" x2="490.22" y2="22.86" width="0.1524" layer="91"/>
<label x="490.22" y="22.86" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A29" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A29"/>
<wire x1="358.14" y1="25.4" x2="360.68" y2="25.4" width="0.1524" layer="91"/>
<label x="360.68" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A29"/>
<wire x1="314.96" y1="25.4" x2="317.5" y2="25.4" width="0.1524" layer="91"/>
<label x="317.5" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A29"/>
<wire x1="444.5" y1="25.4" x2="447.04" y2="25.4" width="0.1524" layer="91"/>
<label x="447.04" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A29"/>
<wire x1="401.32" y1="25.4" x2="403.86" y2="25.4" width="0.1524" layer="91"/>
<label x="403.86" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A29"/>
<wire x1="487.68" y1="25.4" x2="490.22" y2="25.4" width="0.1524" layer="91"/>
<label x="490.22" y="25.4" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A28" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A28"/>
<wire x1="358.14" y1="27.94" x2="360.68" y2="27.94" width="0.1524" layer="91"/>
<label x="360.68" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A28"/>
<wire x1="314.96" y1="27.94" x2="317.5" y2="27.94" width="0.1524" layer="91"/>
<label x="317.5" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A28"/>
<wire x1="444.5" y1="27.94" x2="447.04" y2="27.94" width="0.1524" layer="91"/>
<label x="447.04" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A28"/>
<wire x1="401.32" y1="27.94" x2="403.86" y2="27.94" width="0.1524" layer="91"/>
<label x="403.86" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A28"/>
<wire x1="487.68" y1="27.94" x2="490.22" y2="27.94" width="0.1524" layer="91"/>
<label x="490.22" y="27.94" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A27" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A27"/>
<wire x1="358.14" y1="30.48" x2="360.68" y2="30.48" width="0.1524" layer="91"/>
<label x="360.68" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A27"/>
<wire x1="314.96" y1="30.48" x2="317.5" y2="30.48" width="0.1524" layer="91"/>
<label x="317.5" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A27"/>
<wire x1="444.5" y1="30.48" x2="447.04" y2="30.48" width="0.1524" layer="91"/>
<label x="447.04" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A27"/>
<wire x1="401.32" y1="30.48" x2="403.86" y2="30.48" width="0.1524" layer="91"/>
<label x="403.86" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A27"/>
<wire x1="487.68" y1="30.48" x2="490.22" y2="30.48" width="0.1524" layer="91"/>
<label x="490.22" y="30.48" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A26" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A26"/>
<wire x1="358.14" y1="33.02" x2="360.68" y2="33.02" width="0.1524" layer="91"/>
<label x="360.68" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A26"/>
<wire x1="314.96" y1="33.02" x2="317.5" y2="33.02" width="0.1524" layer="91"/>
<label x="317.5" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A26"/>
<wire x1="444.5" y1="33.02" x2="447.04" y2="33.02" width="0.1524" layer="91"/>
<label x="447.04" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A26"/>
<wire x1="401.32" y1="33.02" x2="403.86" y2="33.02" width="0.1524" layer="91"/>
<label x="403.86" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A26"/>
<wire x1="487.68" y1="33.02" x2="490.22" y2="33.02" width="0.1524" layer="91"/>
<label x="490.22" y="33.02" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A25" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A25"/>
<wire x1="358.14" y1="35.56" x2="360.68" y2="35.56" width="0.1524" layer="91"/>
<label x="360.68" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A25"/>
<wire x1="314.96" y1="35.56" x2="317.5" y2="35.56" width="0.1524" layer="91"/>
<label x="317.5" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A25"/>
<wire x1="444.5" y1="35.56" x2="447.04" y2="35.56" width="0.1524" layer="91"/>
<label x="447.04" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A25"/>
<wire x1="401.32" y1="35.56" x2="403.86" y2="35.56" width="0.1524" layer="91"/>
<label x="403.86" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A25"/>
<wire x1="487.68" y1="35.56" x2="490.22" y2="35.56" width="0.1524" layer="91"/>
<label x="490.22" y="35.56" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A24" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A24"/>
<wire x1="358.14" y1="38.1" x2="360.68" y2="38.1" width="0.1524" layer="91"/>
<label x="360.68" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A24"/>
<wire x1="314.96" y1="38.1" x2="317.5" y2="38.1" width="0.1524" layer="91"/>
<label x="317.5" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A24"/>
<wire x1="444.5" y1="38.1" x2="447.04" y2="38.1" width="0.1524" layer="91"/>
<label x="447.04" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A24"/>
<wire x1="401.32" y1="38.1" x2="403.86" y2="38.1" width="0.1524" layer="91"/>
<label x="403.86" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A24"/>
<wire x1="487.68" y1="38.1" x2="490.22" y2="38.1" width="0.1524" layer="91"/>
<label x="490.22" y="38.1" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A23" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A23"/>
<wire x1="358.14" y1="40.64" x2="360.68" y2="40.64" width="0.1524" layer="91"/>
<label x="360.68" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A23"/>
<wire x1="314.96" y1="40.64" x2="317.5" y2="40.64" width="0.1524" layer="91"/>
<label x="317.5" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A23"/>
<wire x1="444.5" y1="40.64" x2="447.04" y2="40.64" width="0.1524" layer="91"/>
<label x="447.04" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A23"/>
<wire x1="401.32" y1="40.64" x2="403.86" y2="40.64" width="0.1524" layer="91"/>
<label x="403.86" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A23"/>
<wire x1="487.68" y1="40.64" x2="490.22" y2="40.64" width="0.1524" layer="91"/>
<label x="490.22" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A22" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A22"/>
<wire x1="358.14" y1="43.18" x2="360.68" y2="43.18" width="0.1524" layer="91"/>
<label x="360.68" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A22"/>
<wire x1="314.96" y1="43.18" x2="317.5" y2="43.18" width="0.1524" layer="91"/>
<label x="317.5" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A22"/>
<wire x1="444.5" y1="43.18" x2="447.04" y2="43.18" width="0.1524" layer="91"/>
<label x="447.04" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A22"/>
<wire x1="401.32" y1="43.18" x2="403.86" y2="43.18" width="0.1524" layer="91"/>
<label x="403.86" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A22"/>
<wire x1="487.68" y1="43.18" x2="490.22" y2="43.18" width="0.1524" layer="91"/>
<label x="490.22" y="43.18" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A21" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A21"/>
<wire x1="358.14" y1="45.72" x2="360.68" y2="45.72" width="0.1524" layer="91"/>
<label x="360.68" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A21"/>
<wire x1="314.96" y1="45.72" x2="317.5" y2="45.72" width="0.1524" layer="91"/>
<label x="317.5" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A21"/>
<wire x1="444.5" y1="45.72" x2="447.04" y2="45.72" width="0.1524" layer="91"/>
<label x="447.04" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A21"/>
<wire x1="401.32" y1="45.72" x2="403.86" y2="45.72" width="0.1524" layer="91"/>
<label x="403.86" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A21"/>
<wire x1="487.68" y1="45.72" x2="490.22" y2="45.72" width="0.1524" layer="91"/>
<label x="490.22" y="45.72" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A20" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A20"/>
<wire x1="358.14" y1="48.26" x2="360.68" y2="48.26" width="0.1524" layer="91"/>
<label x="360.68" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A20"/>
<wire x1="314.96" y1="48.26" x2="317.5" y2="48.26" width="0.1524" layer="91"/>
<label x="317.5" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A20"/>
<wire x1="444.5" y1="48.26" x2="447.04" y2="48.26" width="0.1524" layer="91"/>
<label x="447.04" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A20"/>
<wire x1="401.32" y1="48.26" x2="403.86" y2="48.26" width="0.1524" layer="91"/>
<label x="403.86" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A20"/>
<wire x1="487.68" y1="48.26" x2="490.22" y2="48.26" width="0.1524" layer="91"/>
<label x="490.22" y="48.26" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A19" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A19"/>
<wire x1="358.14" y1="50.8" x2="360.68" y2="50.8" width="0.1524" layer="91"/>
<label x="360.68" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A19"/>
<wire x1="314.96" y1="50.8" x2="317.5" y2="50.8" width="0.1524" layer="91"/>
<label x="317.5" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A19"/>
<wire x1="444.5" y1="50.8" x2="447.04" y2="50.8" width="0.1524" layer="91"/>
<label x="447.04" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A19"/>
<wire x1="401.32" y1="50.8" x2="403.86" y2="50.8" width="0.1524" layer="91"/>
<label x="403.86" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A19"/>
<wire x1="487.68" y1="50.8" x2="490.22" y2="50.8" width="0.1524" layer="91"/>
<label x="490.22" y="50.8" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A18" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A18"/>
<wire x1="358.14" y1="53.34" x2="360.68" y2="53.34" width="0.1524" layer="91"/>
<label x="360.68" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A18"/>
<wire x1="314.96" y1="53.34" x2="317.5" y2="53.34" width="0.1524" layer="91"/>
<label x="317.5" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A18"/>
<wire x1="444.5" y1="53.34" x2="447.04" y2="53.34" width="0.1524" layer="91"/>
<label x="447.04" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A18"/>
<wire x1="401.32" y1="53.34" x2="403.86" y2="53.34" width="0.1524" layer="91"/>
<label x="403.86" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A18"/>
<wire x1="487.68" y1="53.34" x2="490.22" y2="53.34" width="0.1524" layer="91"/>
<label x="490.22" y="53.34" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A17" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A17"/>
<wire x1="358.14" y1="55.88" x2="360.68" y2="55.88" width="0.1524" layer="91"/>
<label x="360.68" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A17"/>
<wire x1="314.96" y1="55.88" x2="317.5" y2="55.88" width="0.1524" layer="91"/>
<label x="317.5" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A17"/>
<wire x1="444.5" y1="55.88" x2="447.04" y2="55.88" width="0.1524" layer="91"/>
<label x="447.04" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A17"/>
<wire x1="401.32" y1="55.88" x2="403.86" y2="55.88" width="0.1524" layer="91"/>
<label x="403.86" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A17"/>
<wire x1="487.68" y1="55.88" x2="490.22" y2="55.88" width="0.1524" layer="91"/>
<label x="490.22" y="55.88" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A16" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A16"/>
<wire x1="358.14" y1="58.42" x2="360.68" y2="58.42" width="0.1524" layer="91"/>
<label x="360.68" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A16"/>
<wire x1="314.96" y1="58.42" x2="317.5" y2="58.42" width="0.1524" layer="91"/>
<label x="317.5" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A16"/>
<wire x1="444.5" y1="58.42" x2="447.04" y2="58.42" width="0.1524" layer="91"/>
<label x="447.04" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A16"/>
<wire x1="401.32" y1="58.42" x2="403.86" y2="58.42" width="0.1524" layer="91"/>
<label x="403.86" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A16"/>
<wire x1="487.68" y1="58.42" x2="490.22" y2="58.42" width="0.1524" layer="91"/>
<label x="490.22" y="58.42" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A15" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A15"/>
<wire x1="358.14" y1="60.96" x2="360.68" y2="60.96" width="0.1524" layer="91"/>
<label x="360.68" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A15"/>
<wire x1="314.96" y1="60.96" x2="317.5" y2="60.96" width="0.1524" layer="91"/>
<label x="317.5" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A15"/>
<wire x1="444.5" y1="60.96" x2="447.04" y2="60.96" width="0.1524" layer="91"/>
<label x="447.04" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A15"/>
<wire x1="401.32" y1="60.96" x2="403.86" y2="60.96" width="0.1524" layer="91"/>
<label x="403.86" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A15"/>
<wire x1="487.68" y1="60.96" x2="490.22" y2="60.96" width="0.1524" layer="91"/>
<label x="490.22" y="60.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A14" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A14"/>
<wire x1="358.14" y1="63.5" x2="360.68" y2="63.5" width="0.1524" layer="91"/>
<label x="360.68" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A14"/>
<wire x1="314.96" y1="63.5" x2="317.5" y2="63.5" width="0.1524" layer="91"/>
<label x="317.5" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A14"/>
<wire x1="444.5" y1="63.5" x2="447.04" y2="63.5" width="0.1524" layer="91"/>
<label x="447.04" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A14"/>
<wire x1="401.32" y1="63.5" x2="403.86" y2="63.5" width="0.1524" layer="91"/>
<label x="403.86" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A14"/>
<wire x1="487.68" y1="63.5" x2="490.22" y2="63.5" width="0.1524" layer="91"/>
<label x="490.22" y="63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A13" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A13"/>
<wire x1="358.14" y1="66.04" x2="360.68" y2="66.04" width="0.1524" layer="91"/>
<label x="360.68" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A13"/>
<wire x1="314.96" y1="66.04" x2="317.5" y2="66.04" width="0.1524" layer="91"/>
<label x="317.5" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A13"/>
<wire x1="444.5" y1="66.04" x2="447.04" y2="66.04" width="0.1524" layer="91"/>
<label x="447.04" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A13"/>
<wire x1="401.32" y1="66.04" x2="403.86" y2="66.04" width="0.1524" layer="91"/>
<label x="403.86" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A13"/>
<wire x1="487.68" y1="66.04" x2="490.22" y2="66.04" width="0.1524" layer="91"/>
<label x="490.22" y="66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="A12" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="A12"/>
<wire x1="358.14" y1="68.58" x2="360.68" y2="68.58" width="0.1524" layer="91"/>
<label x="360.68" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="A12"/>
<wire x1="314.96" y1="68.58" x2="317.5" y2="68.58" width="0.1524" layer="91"/>
<label x="317.5" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="A12"/>
<wire x1="444.5" y1="68.58" x2="447.04" y2="68.58" width="0.1524" layer="91"/>
<label x="447.04" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="A12"/>
<wire x1="401.32" y1="68.58" x2="403.86" y2="68.58" width="0.1524" layer="91"/>
<label x="403.86" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="A12"/>
<wire x1="487.68" y1="68.58" x2="490.22" y2="68.58" width="0.1524" layer="91"/>
<label x="490.22" y="68.58" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G11" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G11"/>
<wire x1="358.14" y1="73.66" x2="360.68" y2="73.66" width="0.1524" layer="91"/>
<label x="360.68" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G11"/>
<wire x1="314.96" y1="73.66" x2="317.5" y2="73.66" width="0.1524" layer="91"/>
<label x="317.5" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G11"/>
<wire x1="444.5" y1="73.66" x2="447.04" y2="73.66" width="0.1524" layer="91"/>
<label x="447.04" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G11"/>
<wire x1="401.32" y1="73.66" x2="403.86" y2="73.66" width="0.1524" layer="91"/>
<label x="403.86" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G11"/>
<wire x1="487.68" y1="73.66" x2="490.22" y2="73.66" width="0.1524" layer="91"/>
<label x="490.22" y="73.66" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G10" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G10"/>
<wire x1="358.14" y1="76.2" x2="360.68" y2="76.2" width="0.1524" layer="91"/>
<label x="360.68" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G10"/>
<wire x1="314.96" y1="76.2" x2="317.5" y2="76.2" width="0.1524" layer="91"/>
<label x="317.5" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G10"/>
<wire x1="444.5" y1="76.2" x2="447.04" y2="76.2" width="0.1524" layer="91"/>
<label x="447.04" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G10"/>
<wire x1="401.32" y1="76.2" x2="403.86" y2="76.2" width="0.1524" layer="91"/>
<label x="403.86" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G10"/>
<wire x1="487.68" y1="76.2" x2="490.22" y2="76.2" width="0.1524" layer="91"/>
<label x="490.22" y="76.2" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G9" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G9"/>
<wire x1="358.14" y1="78.74" x2="360.68" y2="78.74" width="0.1524" layer="91"/>
<label x="360.68" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G9"/>
<wire x1="314.96" y1="78.74" x2="317.5" y2="78.74" width="0.1524" layer="91"/>
<label x="317.5" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G9"/>
<wire x1="444.5" y1="78.74" x2="447.04" y2="78.74" width="0.1524" layer="91"/>
<label x="447.04" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G9"/>
<wire x1="401.32" y1="78.74" x2="403.86" y2="78.74" width="0.1524" layer="91"/>
<label x="403.86" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G9"/>
<wire x1="487.68" y1="78.74" x2="490.22" y2="78.74" width="0.1524" layer="91"/>
<label x="490.22" y="78.74" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G8" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G8"/>
<wire x1="358.14" y1="81.28" x2="360.68" y2="81.28" width="0.1524" layer="91"/>
<label x="360.68" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G8"/>
<wire x1="314.96" y1="81.28" x2="317.5" y2="81.28" width="0.1524" layer="91"/>
<label x="317.5" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G8"/>
<wire x1="444.5" y1="81.28" x2="447.04" y2="81.28" width="0.1524" layer="91"/>
<label x="447.04" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G8"/>
<wire x1="401.32" y1="81.28" x2="403.86" y2="81.28" width="0.1524" layer="91"/>
<label x="403.86" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G8"/>
<wire x1="487.68" y1="81.28" x2="490.22" y2="81.28" width="0.1524" layer="91"/>
<label x="490.22" y="81.28" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G7" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G7"/>
<wire x1="358.14" y1="83.82" x2="360.68" y2="83.82" width="0.1524" layer="91"/>
<label x="360.68" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G7"/>
<wire x1="314.96" y1="83.82" x2="317.5" y2="83.82" width="0.1524" layer="91"/>
<label x="317.5" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G7"/>
<wire x1="444.5" y1="83.82" x2="447.04" y2="83.82" width="0.1524" layer="91"/>
<label x="447.04" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G7"/>
<wire x1="401.32" y1="83.82" x2="403.86" y2="83.82" width="0.1524" layer="91"/>
<label x="403.86" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G7"/>
<wire x1="487.68" y1="83.82" x2="490.22" y2="83.82" width="0.1524" layer="91"/>
<label x="490.22" y="83.82" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G6" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G6"/>
<wire x1="358.14" y1="86.36" x2="360.68" y2="86.36" width="0.1524" layer="91"/>
<label x="360.68" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G6"/>
<wire x1="314.96" y1="86.36" x2="317.5" y2="86.36" width="0.1524" layer="91"/>
<label x="317.5" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G6"/>
<wire x1="444.5" y1="86.36" x2="447.04" y2="86.36" width="0.1524" layer="91"/>
<label x="447.04" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G6"/>
<wire x1="401.32" y1="86.36" x2="403.86" y2="86.36" width="0.1524" layer="91"/>
<label x="403.86" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G6"/>
<wire x1="487.68" y1="86.36" x2="490.22" y2="86.36" width="0.1524" layer="91"/>
<label x="490.22" y="86.36" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G5" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G5"/>
<wire x1="358.14" y1="88.9" x2="360.68" y2="88.9" width="0.1524" layer="91"/>
<label x="360.68" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G5"/>
<wire x1="314.96" y1="88.9" x2="317.5" y2="88.9" width="0.1524" layer="91"/>
<label x="317.5" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G5"/>
<wire x1="444.5" y1="88.9" x2="447.04" y2="88.9" width="0.1524" layer="91"/>
<label x="447.04" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G5"/>
<wire x1="401.32" y1="88.9" x2="403.86" y2="88.9" width="0.1524" layer="91"/>
<label x="403.86" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G5"/>
<wire x1="487.68" y1="88.9" x2="490.22" y2="88.9" width="0.1524" layer="91"/>
<label x="490.22" y="88.9" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G4" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G4"/>
<wire x1="358.14" y1="91.44" x2="360.68" y2="91.44" width="0.1524" layer="91"/>
<label x="360.68" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G4"/>
<wire x1="314.96" y1="91.44" x2="317.5" y2="91.44" width="0.1524" layer="91"/>
<label x="317.5" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G4"/>
<wire x1="444.5" y1="91.44" x2="447.04" y2="91.44" width="0.1524" layer="91"/>
<label x="447.04" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G4"/>
<wire x1="401.32" y1="91.44" x2="403.86" y2="91.44" width="0.1524" layer="91"/>
<label x="403.86" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G4"/>
<wire x1="487.68" y1="91.44" x2="490.22" y2="91.44" width="0.1524" layer="91"/>
<label x="490.22" y="91.44" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G3" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G3"/>
<wire x1="358.14" y1="93.98" x2="360.68" y2="93.98" width="0.1524" layer="91"/>
<label x="360.68" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G3"/>
<wire x1="314.96" y1="93.98" x2="317.5" y2="93.98" width="0.1524" layer="91"/>
<label x="317.5" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G3"/>
<wire x1="444.5" y1="93.98" x2="447.04" y2="93.98" width="0.1524" layer="91"/>
<label x="447.04" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G3"/>
<wire x1="401.32" y1="93.98" x2="403.86" y2="93.98" width="0.1524" layer="91"/>
<label x="403.86" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G3"/>
<wire x1="487.68" y1="93.98" x2="490.22" y2="93.98" width="0.1524" layer="91"/>
<label x="490.22" y="93.98" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G2" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G2"/>
<wire x1="358.14" y1="96.52" x2="360.68" y2="96.52" width="0.1524" layer="91"/>
<label x="360.68" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G2"/>
<wire x1="314.96" y1="96.52" x2="317.5" y2="96.52" width="0.1524" layer="91"/>
<label x="317.5" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G2"/>
<wire x1="444.5" y1="96.52" x2="447.04" y2="96.52" width="0.1524" layer="91"/>
<label x="447.04" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G2"/>
<wire x1="401.32" y1="96.52" x2="403.86" y2="96.52" width="0.1524" layer="91"/>
<label x="403.86" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G2"/>
<wire x1="487.68" y1="96.52" x2="490.22" y2="96.52" width="0.1524" layer="91"/>
<label x="490.22" y="96.52" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="G1" class="0">
<segment>
<pinref part="CNN2" gate="G$1" pin="G1"/>
<wire x1="358.14" y1="99.06" x2="360.68" y2="99.06" width="0.1524" layer="91"/>
<label x="360.68" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN1" gate="G$1" pin="G1"/>
<wire x1="314.96" y1="99.06" x2="317.5" y2="99.06" width="0.1524" layer="91"/>
<label x="317.5" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN4" gate="G$1" pin="G1"/>
<wire x1="444.5" y1="99.06" x2="447.04" y2="99.06" width="0.1524" layer="91"/>
<label x="447.04" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN3" gate="G$1" pin="G1"/>
<wire x1="401.32" y1="99.06" x2="403.86" y2="99.06" width="0.1524" layer="91"/>
<label x="403.86" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="CNN5" gate="G$1" pin="G1"/>
<wire x1="487.68" y1="99.06" x2="490.22" y2="99.06" width="0.1524" layer="91"/>
<label x="490.22" y="99.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<wire x1="332.74" y1="-139.7" x2="332.74" y2="-144.78" width="0.1524" layer="91"/>
<label x="332.74" y="-144.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<wire x1="289.56" y1="-139.7" x2="289.56" y2="-144.78" width="0.1524" layer="91"/>
<label x="289.56" y="-144.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<wire x1="419.1" y1="-139.7" x2="419.1" y2="-144.78" width="0.1524" layer="91"/>
<label x="419.1" y="-144.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED4" gate="G$1" pin="C"/>
<wire x1="375.92" y1="-139.7" x2="375.92" y2="-144.78" width="0.1524" layer="91"/>
<label x="375.92" y="-144.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="C"/>
<wire x1="462.28" y1="-139.7" x2="462.28" y2="-144.78" width="0.1524" layer="91"/>
<label x="462.28" y="-144.78" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="332.74" y1="-119.38" x2="332.74" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CNN2" gate="G$1" pin="B82"/>
<wire x1="332.74" y1="-109.22" x2="337.82" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="332.74" y1="-132.08" x2="332.74" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<wire x1="289.56" y1="-119.38" x2="289.56" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CNN1" gate="G$1" pin="B82"/>
<wire x1="294.64" y1="-109.22" x2="289.56" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="289.56" y1="-132.08" x2="289.56" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="419.1" y1="-119.38" x2="419.1" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CNN4" gate="G$1" pin="B82"/>
<wire x1="424.18" y1="-109.22" x2="419.1" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="A"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="419.1" y1="-132.08" x2="419.1" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<wire x1="375.92" y1="-119.38" x2="375.92" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CNN3" gate="G$1" pin="B82"/>
<wire x1="381" y1="-109.22" x2="375.92" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="A"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="375.92" y1="-132.08" x2="375.92" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="462.28" y1="-119.38" x2="462.28" y2="-109.22" width="0.1524" layer="91"/>
<pinref part="CNN5" gate="G$1" pin="B82"/>
<wire x1="467.36" y1="-109.22" x2="462.28" y2="-109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="A"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="462.28" y1="-132.08" x2="462.28" y2="-129.54" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
